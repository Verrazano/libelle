// timer.as

////ENTITY////

void resetTimer(Entity@ this, string timername) {
    this.setFloat("timersecs_" + timername, getGameTime());
    this.setUInt("timerticks_" + timername, getTicksElapsed());
}

float getSecondsPast(Entity@ this, string timername) {
    return getGameTime() - this.getFloat("timersecs_" + timername);
}

uint getTicksPast(Entity@ this, string timername) {
    return getTicksElapsed() - this.getUInt("timerticks_" + timername);
}

bool timerPastSeconds(Entity@ this, string timername, float time) {
    return getSecondsPast(this, timername) > time;
}

bool timerPastTicks(Entity@ this, string timername, uint ticks) {
    return getTicksPast(this, timername) > ticks;
}

void syncTimer(Entity@ this, string timername) {
    this.syncFloat("timersecs_" + timername);
    this.syncUInt("timerticks_" + timername);
}

void advanceTimer(Entity@ this, string timername, float timeToAdd) {
    this.setFloat("timersecs_" + timername, getSecondsPast(this, timername) + timeToAdd);
    this.setUInt("timerticks_" + timername, getTicksPast(this, timername) + timeToAdd);
}

////GAMEMODE////

void resetTimer(GameMode@ this, string timername) {
    this.setFloat("timersecs_" + timername, getGameTime());
    this.setUInt("timerticks_" + timername, getTicksElapsed());
}

float getSecondsPast(GameMode@ this, string timername) {
    return getGameTime() - this.getFloat("timersecs_" + timername);
}

uint getTicksPast(GameMode@ this, string timername) {
    return getTicksElapsed() - this.getUInt("timerticks_" + timername);
}

bool timerPastSeconds(GameMode@ this, string timername, float time) {
    return getSecondsPast(this, timername) > time;
}

bool timerPastTicks(GameMode@ this, string timername, uint ticks) {
    return getTicksPast(this, timername) > ticks;
}

void syncTimer(GameMode@ this, string timername) {
    this.syncFloat("timersecs_" + timername);
    this.syncUInt("timerticks_" + timername);
}

void advanceTimer(GameMode@ this, string timername, float timeToAdd) {
    this.setFloat("timersecs_" + timername, getSecondsPast(this, timername) + timeToAdd);
    this.setUInt("timerticks_" + timername, getTicksPast(this, timername) + timeToAdd);
}

////PLAYER////

void resetTimer(Player@ this, string timername) {
    this.setFloat("timersecs_" + timername, getGameTime());
    this.setUInt("timerticks_" + timername, getTicksElapsed());
}

float getSecondsPast(Player@ this, string timername) {
    return getGameTime() - this.getFloat("timersecs_" + timername);
}

uint getTicksPast(Player@ this, string timername) {
    return getTicksElapsed() - this.getUInt("timerticks_" + timername);
}

bool timerPastSeconds(Player@ this, string timername, float time) {
    return getSecondsPast(this, timername) > time;
}

bool timerPastTicks(Player@ this, string timername, uint ticks) {
    return getTicksPast(this, timername) > ticks;
}

void syncTimer(Player@ this, string timername) {
    this.syncFloat("timersecs_" + timername);
    this.syncUInt("timerticks_" + timername);
}

void advanceTimer(Player@ this, string timername, float timeToAdd) {
    this.setFloat("timersecs_" + timername, getSecondsPast(this, timername) + timeToAdd);
    this.setUInt("timerticks_" + timername, getTicksPast(this, timername) + timeToAdd);
}