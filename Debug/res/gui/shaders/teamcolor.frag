uniform sampler2D texture;

uniform vec4 r1;
uniform vec4 r2;
uniform vec4 r3;
uniform vec4 r4;

void main()
{
	vec4 red1; vec4 red2; vec4 red3; vec4 red4;
	red1.r = 175.f/255.f; red1.g = 37.f/255.f; red1.b = 57.f/255.f;
	red2.r = 101.f/255.f; red2.g = 22.f/255.f; red2.b = 45.f/255.f;
	red3.r = 54.f/255.f; red3.g = 9.f/255.f; red3.b = 34.f/255.f;	
	red4.r = 212.f/255.f; red4.g = 78.f/255.f; red4.b = 71.f/255.f;
	

	vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
	red1.a = pixel.a;
	red2.a = pixel.a;
	red3.a = pixel.a;
	red4.a = pixel.a;

	vec4 outpixel = pixel;

	if(pixel == red1){outpixel = r1;}
	else if(pixel == red2){outpixel = r2;}
	else if(pixel == red3){outpixel = r3;}
	else if(pixel == red4){outpixel = r4;}

	gl_FragColor = outpixel;

}
