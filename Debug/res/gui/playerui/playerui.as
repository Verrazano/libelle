//playerui.as

#include "../../entities/common/guns.as"

void onInit(GameMode@ this)
{
    if (isServer())
    {
        return;
    }

    Player@ p = getLocalPlayer();

    if (p is null)
    {
        return;
    }

    Entity@ e = p.getEntity();

    if (e is null)
    {
        return;
    }

    e.setBool("drawPlayerUI", true);
    //e.syncBool("drawPlayerUI");

}

void onDraw(GameMode@ this)
{
    if (isServer())
    {
        return;
    }

    Player@ p = getLocalPlayer();

    if (p is null)
    {
        return;
    }

    Entity@ e = p.getEntity();

    if (e is null)
    {
        return;
    }

    uint curweapon = e.getUInt("curweapon");

    string gun = getGunName(e, curweapon);

    float ammoLeft = getAmmoLeft(e, curweapon);
    float magsLeft = getMagsLeft(e, curweapon);

    string ammoText = "Ammo: " + ammoLeft + " / " + magsLeft;

    uint maxAmmo = getMaxAmmo(gun);
    float percentAmmoLeft = maxAmmo > 0 ? ammoLeft / maxAmmo : -1;
    if (percentAmmoLeft == -1)
    {
        e.setBool("drawPlayerUI", false);
        //e.syncBool("drawPlayerUI");

    }
    else
    {
        e.setBool("drawPlayerUI", true);
        //e.syncBool("drawPlayerUI");

    }

    if (e.getBool("drawPlayerUI"))
    {
        //actual drawing of ammo/guns
        if (ammoLeft > 0 && !isReloading(e, gun, curweapon))
        {
            drawRect(30 - 3, 600 - 30, percentAmmoLeft * (ammoText.length() * 8 + 6), 20, 25, 50, 100, 175);
            drawRect(30 - 3, 600 - 30, ammoText.length() * 8 + 6, 20, 0, 0, 0, 175);
        }
        if (isReloading(e, gun, curweapon))
        {
            float percentReloadTimer = getReloadTime(gun) > 0 ? getSecondsPast(e, "reloadTimer") / getReloadTime(gun) : 1;
            drawRect(30 - 3, 600 - 30, percentReloadTimer * (ammoText.length() * 8 + 6), 20, 25, 50, 100, 175);
            drawRect(30 - 3, 600 - 30, ammoText.length() * 8 + 6, 20, 50, 0, 0, 175);
        }
        if (ammoLeft == 0 && magsLeft == 0)
        {
            drawRect(30 - 3, 600 - 30, ammoText.length() * 8 + 6, 20, 50, 0, 0, 175);
        }

        drawText(ammoText, 30, 600 - 30, 255, 255, 255);

        string gunText = gun.substr(0, gun.findFirst("_") > 0 ? gun.findFirst("_") : gun.length()) != "" ? gun.substr(0, gun.findFirst("_") > 0 ? gun.findFirst("_") : gun.length()) : "gun";
        drawSprite("res/gui/playerui/weapons/" + gunText + ".png", 50, 600 - 100, 3, e.getTeam());

        //actual drawing of utility (jumpJet, etc.)
        float utilChargeTime = e.getFloat("util_charge_time");
        float jumpJetTimeLeft = getSecondsPast(e, "jumpJetTimer") > 0 ? getSecondsPast(e, "jumpJetTimer") : 0;
        jumpJetTimeLeft = jumpJetTimeLeft < utilChargeTime && jumpJetTimeLeft > 0 ? jumpJetTimeLeft : utilChargeTime;
        float percentJumpJetTime = utilChargeTime > 0 ? jumpJetTimeLeft / utilChargeTime : 0;

        string utilText = "Utility: " + e.getString("utility");

        if (percentJumpJetTime < 1)
        {
            drawRect(150 - 3, 600 - 30, percentJumpJetTime * (utilText.length() * 8 + 6), 20, 25, 50, 100, 175);
            drawRect(150 - 3, 600 - 30, utilText.length() * 8 + 6, 20, 50, 0, 0, 175);

        }
        if (percentJumpJetTime >= 1)
        {
            drawRect(150 - 3, 600 - 30, percentJumpJetTime * (utilText.length() * 8 + 6), 20, 25, 50, 100, 175);
            drawRect(150 - 3, 600 - 30, utilText.length() * 8 + 6, 20, 0, 0, 0, 175);
        }

        drawText(utilText, 150, 600 - 30, 255, 255, 255);
        //draw sprite for util here

        //draw equipment place area
        if (isDeployer(gun))
        {
            string gunText = gun.substr(0, gun.findFirst("_") > 0 ? gun.findFirst("_") : gun.length()) != "" ? gun.substr(0, gun.findFirst("_") > 0 ? gun.findFirst("_") : gun.length()) : "gun";
            drawSprite("res/gui/playerui/weapons/" + gunText + ".png", getMousePos().x - (3 * 25 / 2), getMousePos().y - (3 * 25 / 2), 3, e.getTeam());

            vec2f pos(e.getPosition());
            vec2f mouse(getMouseInView());
            uint distance = sqrt(pow(mouse.x - pos.x, 2) + pow(mouse.y - pos.y, 2));
            if (distance > e.getUInt("equipmentPlaceDist") || distance < 10 || getAmmoLeft(e, curweapon) == 0)
            {
                drawRect(getMousePos().x - (3 * 25 / 2) + 2, getMousePos().y - (3 * 25 / 2) + 2, 32 * 2 + 2, 32 * 2 + 2, 200, 0, 0, 100);
            
            }
            else
            {
                drawRect(getMousePos().x - (3 * 25 / 2) + 2, getMousePos().y - (3 * 25 / 2) + 2, 32 * 2 + 2, 32 * 2 + 2, 0, 200, 0, 100);
            
            }

        }

    }

}