    // deathmatch.as

#include "../../entities/common/guns.as"

void onInit(GameMode@ this)
{
    this.setFloat("respawntime", 1);

    //name, magCap, maxMags, fireVel, fireRate, rpb, semiAuto, spread, reloadTime, damageMod, ammo, fireSound, reloadSound, range, deployer (optional, default = false)
    //player guns
    addGun("submachinegun", 36, 6, 30, 0.09, 1, false, 2, 2.3, 1.5, "20cal", "res/sounds/minigun.wav", "res/sounds/reload1.wav", 50000);
    addGun("pistol", 12, 10, 30, 0.1, 1, true, 4, 1.7, 2, "20cal", "res/sounds/gunfire1.wav", "res/sounds/reload1.wav", 50000);
    addGun("dual_pistols", 12, 10, 30, 0.1, 2, true, 4, 1.7, 2, "20cal", "res/sounds/gunfire1.wav", "res/sounds/reload1.wav", 50000);
    addGun("minigun", 60, 3, 30, 0.1, 1, false, 10, 3, 1, "20cal", "res/sounds/minigun.wav", "res/sounds/reload1.wav", 30000);
    addGun("crossbow", 60, 1, 30, 0.2, 1, true, 3, 0.2, 3, "bolt", "res/sounds/bowshot.wav", "res/sounds/reload1.wav", 70000);
    //guns for equipment
    addGun("turret", 1500, 10, 40, 0.1, 1, true, 1, 10, 1, "80cal", "res/sounds/minigun.wav", "res/sounds/reload1.wav", 50000);
    //equipment deployment guns
    addGun("turret_deployer", 1, 0, 0, 1, 1, true, 0, 0.01, 0, "turret", "res/sounds/hollowhit1.wav", "res/sounds/reload1.wav", 50000, true);
    addGun("landmine_deployer", 1, 1, 10, 1, 1, true, 0, 0.01, 40, "landmine", "res/sounds/hollowhit1.wav", "res/sounds/reload1.wav", 50000, true);

    this.setUInt("team1count", 0);
    this.setUInt("team2count", 0);
}

void onReset(GameMode@ this)
{
    if (isClient())
    {
        return;
    }

    //createEntity("landmine", 400, 300).setTeam(1);
    //createEntity("landmine", 425, 300).setTeam(1);
    //createEntity("landmine", 400, 300).setTeam(1);
    //createEntity("landmine", 400, 200).setTeam(1);
    //createEntity("turret", 400, 400).setTeam(1);

    Entity@ d = createEntity("shippingcontainer", 700, 300);
    createEntity("crate", 800, 400);
    createEntity("crate", 700, 400);
    createEntity("crate", 700, 400);

    setupSpawns(this, 2, 300, 1000);
    //GameMode@, spawnCount, startX, distSeperatingX

    resetTimer(this, "timeSinceLastDeathTimer");

}

void onTick(GameMode@ this)
{
    if (isClient())
    {
        return;
    }

    uint count = playersCount();
    for (uint i = 0; i  < count; i++)
    {
        Player@ p = getPlayerByIndex(i);
        if (p !is null)
        {
            if (p.getBool("needsrespawn"))
            {
                if (timerPastSeconds(p, "respawn_time", this.getFloat("respawntime")))
                {
                    int t = p.getInt("team");
                    if (t > 0)
                    {
                        for (int i = 0; i < this.getInt("spawnCount"); i++)
                        {
                            if (i == p.getInt("team"))
                            {
                                uint curclass = p.getUInt("class");
                                Entity@ e = createEntity(curclass == 0 ? "drachen" : (curclass == 1 ? "libelle" : "geist"), this.getUInt("spawn_" + i + "_X"), this.getUInt("spawn_" + i + "_Y"));
                                //Entity@ e = createEntity(rand() % 2 == 0 ? "libelle" : rand() % 2 == 0 ? "drachen" : "geist", 300, 400);
                                //Entity@ e = createEntity("joe", 300, 400);
                                e.setTeam(p.getInt("team"));
                                p.setString("entityId", e.getID());
                                e.plug(p);
                            }

                        }

                    }
                    else
                    {
                        uint curclass = p.getUInt("class");
                        Entity@ e = createEntity(curclass == 0 ? "drachen" : curclass == 1 ? "libelle" : "geist", this.getUInt("spawn_" + i + "_X"), this.getUInt("spawn_" + i + "_Y"));
                        //Entity@ e = createEntity(rand() % 2 == 0 ? "libelle" : rand() % 2 == 0 ? "drachen" : "geist", 300, 400);
                        //Entity@ e = createEntity("joe", 300, 400);
                        e.setTeam(p.getInt("team"));
                        p.setString("entityId", e.getID());
                        e.plug(p);
                    }

                    p.setBool("needsrespawn", false);
                    p.syncBool("needsrespawn");

                }

            }

        }

    }

    /*if (getSecondsPast(this, "timeSinceLastDeathTimer") <=  6)
    {
        if (!this.getBool("drawKillString"))
        {
            this.setBool("drawKillString", true);
            this.syncBool("drawKillString");

        }

    }
    else if (getSecondsPast(this, "timeSinceLastDeathTimer") >=  6)
    {
        if (this.getBool("drawKillString"))
        {
            this.setBool("drawKillString", false);
            this.syncBool("drawKillString");

        }

    }*/

}

void onDraw(GameMode@ this)
{
    if (isServer())
    {
        return;
    }

    Player@ p = getLocalPlayer();
    if (p !is null)
    {
        if (p.getBool("needsrespawn"))
        {
            setView(0, 0, 1200, 900);
            drawSelect(this, p);

            int t = (this.getFloat("respawntime") - getSecondsPast(p, "respawn_time"));
            if (t <= 0)
            {
                t = 1;
            }
            drawText("respawning . . . " + t, 400, 0, 255, 0, 0);

        }
        else
        {
            drawText(this.getString("playerlist"), 700, 0);

        }

        //drawing of killstring
        string killString = this.getString("killstr");
        //if (this.getBool("drawKillString"))
        //{
        drawText(killString, 775 - killString.length() * 8, 560, 255, 0, 0);

        //}

    }

}

void drawSelect(GameMode@ this, Player@ p)
{
    if (p !is null)
    {
        uint curclass = p.getUInt("class");

        drawRect(10, 60, 250, 450, 80, 80, 80, 200);
        drawText("      Drachen (Heavy)\n\n\n\n\n\n             health: " + p.getFloat("max_health") + "\n             speed: 11\n     dash; minigun; rocketlauncher\n\n\n             minigun\n             dmg: 1\n             rof: 0.1 (x2)\n             ammo: 60/3\n             reload: 3\n             spread: 10\n             full-auto", -30, 60);
        drawSprite("res/sprites/selectdrachen.png", 10, 80, 3);

        drawRect(270, 60, 250, 450, 80, 80, 80, 200);
        drawText("      Libelle (Soldier)\n\n\n\n\n\n             health: " + p.getFloat("max_health") + "\n             speed: 15\n     jumpjet; assult rifle; shotgun\n\n\n             assult rifle\n             dmg: 2\n             rof: 0.1\n             ammo: 12/10\n             reload: 1.7\n             spread: 6\n             semi-auto", 230, 60);
        drawSprite("res/sprites/selectlibelle.png", 270, 80, 3);

        drawRect(530, 60, 250, 450, 80, 80, 80, 200);
        drawText("      Geist (Scout)\n\n\n\n\n\n             health: " + p.getFloat("max_health") + "\n             speed: 18\n     jumpjet; crossbow; daul pistols\n\n\n             crossbow\n             dmg: 3\n             rof: 0.2\n             ammo: 1/60\n             reload: 0.2\n             spread: 3\n             semi-auto", 490, 60);
        drawSprite("res/sprites/selectgeist.png", 530, 80, 3);

        drawRect(10 * (curclass + 1) + 250 * curclass, 60, 250, 450, 100, 100, 0, 80);

        if (p.fireKeyPressed())
        {
            vec2f mouse(getMousePos());
            if (mouse.x > 10 && mouse.x < 260)
            {
                curclass = 0;
            }
            else if (mouse.x > 270 && mouse.x < 520)
            {
                curclass = 1;
            }
            else if (mouse.x > 530 && mouse.x < 780)
            {
                curclass = 2;
            }

            p.setUInt("class", curclass);
            p.syncUInt("class");

        }

    }

}

void onPlayerJoin(GameMode@ this, Player@ p)
{
    print(p.name + " joined the server");
    p.setUInt("class", 2); //0 = drachen, 1 = libelle, 2 = geist
    uint team = 0;
    uint team1count = this.getUInt("team1count");
    uint team2count = this.getUInt("team2count");

    if (team1count > team2count)
    {
        team2count++;
        team = 1;

    }
    else
    {
        team1count++;
        team = 0;

    }

    this.setUInt("team1count", team1count);
    this.setUInt("team2count", team2count);

    p.setInt("team", team);
    addToRespawnQueue(p);

    string playerlist;
    for (uint i = 0; i < playersCount(); i++)
    {
        playerlist += getPlayerByIndex(i).name + "\n";

    }
    this.setString("playerlist", playerlist);
    this.syncString("playerlist");

    this.syncString("killstr");

}

void addToRespawnQueue(Player@ p)
{
    p.setBool("needsrespawn", true);
    resetTimer(p, "respawn_time");
    p.syncBool("needsrespawn");
    syncTimer(p, "respawn_time");

}

void onEntityDie(GameMode@ this, Entity@ e)
{
    if (isClient())
    {
        return;
    }

    Player@ p = e.getPlayer();
    if (p !is null)
    {
        string hurtname = e.getString("recenthurt");

        addToRespawnQueue(p);

        if (hurtname == "")
        {
            return;
        }
        this.setString("killstr", hurtname + " MURDERLIZED " + p.name + "!");
        print(this.getString("killstr"));
        this.syncString("killstr");

    }

    //resetTimer(this, "timeSinceLastDeathTimer");
    //syncTimer(this, "timeSinceLastDeathTimer");

}

void onPlayerLeave(GameMode@ this, Player@ p)
{
    print(p.name + " left the server");
    Entity@ e = getEntityByID(p.getString("entityId"));
    if (e is null)
    {
        return;
    }
    e.kill();

    string playerlist;
    bool found = false;
    for (uint i = 0; i < playersCount(); i++)
    {
        if (getPlayerByIndex(i).name != p.name && !found)
        {
            found = true;
            playerlist += getPlayerByIndex(i).name + "\n";
        }

    }

    this.setString("playerlist", playerlist);
    this.syncString("playerlist");

}

void setupSpawns(GameMode@ this, int spawnCount, int startX, uint distSeperatingX)
{
    this.setInt("spawnCount", spawnCount);

    for (int i = 0; i < spawnCount; i++)
    {
        createEntity("spawn", startX + (distSeperatingX * i), 400).setTeam(i);
        this.setUInt("spawn_" + i + "_X", startX + (distSeperatingX * i));
        this.setUInt("spawn_" + i + "_Y", 400);

    }

}
