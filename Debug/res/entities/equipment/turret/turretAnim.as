//turretAnim.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    SpriteLayer@ sprite = this.getLayer("turret_sprite");
    sprite.setTextureRect(0, 0, 13, 27);
    sprite.setSize(13, 27);
    sprite.setOrigin(6.5, 21);
    sprite.setZ(0.9);

    SpriteLayer@ stand = this.addLayer("stand", "res/entities/equipment/turret/turret.png", 0, 0, 0, 0, true);
    stand.setTextureRect(14, 0, 19, 27);
    stand.setSize(19, 27);
    stand.setOrigin(9.5, 10);

    this.setFloat("stand_initial", this.getAngle());

}

void onDraw(Entity@ this)
{
    SpriteLayer@ stand = this.getLayer("stand");
    stand.setAngle(this.getFloat("stand_initial") - this.getAngle());

    drawText("" + this.getUInt("ammoLeft"), this.getPosition().x - (("Ammo Left: " + this.getUInt("ammoLeft")).length() * 7), this.getPosition().y - 20, 0, 255, 0);

}

void onTick(Entity@ this)
{
    uint curweapon = this.getUInt("curweapon");
    if (isServer())
    {
        this.setUInt("ammoLeft", this.getUInt(gunTag2(curweapon) + "curAmmo"));
        this.syncUInt("ammoLeft");

    }

}