//turretInit.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    //needed for healthBar
    this.setFloat("max_health", this.health);
}