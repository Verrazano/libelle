//turretLogic.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    if (isClient())
    {
        return;
    }

    //stub right now
    this.addTag("damageable");

    //needed to shoot
    this.setBool("switchweaponpressed", false);
    this.setBool("reloadpressed", false);

    initForGuns(this);
    giveGun(this, "turret");
}

void onTick(Entity@ this)
{
    if (isClient())
    {
        return;
    }

    uint curweapon = this.getUInt("curweapon");
    string gun = getGunName(this, curweapon);

    //targeting
    uint count = playersCount();
    for (uint i = 0; i  < count; i++)
    {
        Player@ p = getPlayerByIndex(i);
        if (p !is null)
        {
            string id = this.getString("shooterid");
            Entity@ e = getEntityByID(p.getString("entityId"));

            if ((e.hasTag("player") || e.hasTag("damageable")) && !this.isOnTeam(e.getTeam()) && id != e.getID())
            {
                float playerX = e.getPosition().x;
                float playerY = e.getPosition().y;

                float distToPlayer = sqrt(((this.getPosition().y - playerY) * (this.getPosition().y - playerY)) + ((this.getPosition().x - playerX) * (this.getPosition().x - playerX)));

                if (distToPlayer < 300)
                {
                    //rotating
                    float dY = this.getPosition().y - playerY;
                    float dX = this.getPosition().x - playerX;
                    float angle = -(atan2(dY, dX) * 180 / 3.14159) + 180;
                    float curAngle = this.getAngle();

                    float lerpDeg = lerpDegrees(curAngle, angle, 0.05);

                    this.setAngle(lerpDeg);
                    //this.setAngle(angle);

                    //shooting
                    shoot(this, gun, curweapon);
                }

            }

        }

    }

    isReloading(this, gun, curweapon);

}

float lerpDegrees(float start, float end, float amount)
{
    float difference = abs(end - start);
    if (difference > 180)
    {
        // We need to add on to one of the values.
        if (end > start)
        {
            // We'll add it on to start...
            start += 360;

        }
        else
        {
            // Add it on to end.
            end += 360;

        }

    }

    // Interpolate it.
    float value = (start + ((end - start) * amount));

    // Wrap it..
    float rangeZero = 360;

    if (value >= 0 && value <= 360)
    {
        return value;

    }

    return (value % rangeZero);

}