//turretLogic.as

void onInit(Entity@ this)
{
    if (isClient())
    {
        return;
    }

    //stub right now
    //this.addTag("damageable");
}

void onCollide(Entity@ this, Entity@ o)
{
    string id = this.getString("shooterid");

    if ((o.hasTag("player") || o.hasTag("damageable")) && !this.isOnTeam(o.getTeam()) && id != o.getID())
    {
        o.health -= this.getFloat("damage");
        o.setString("recenthurt", "landmine");
        this.health = -1;

        if (isServer())
        {
            return;
        }

        if (isLocalPlayer(o.getPlayer()))
        {
            Player@ p = o.getPlayer();

            playSound(this.getString("explode_sound"), this.getPosition().x, this.getPosition().y);

        }

    }

}