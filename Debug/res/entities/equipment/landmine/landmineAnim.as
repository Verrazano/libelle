void onInit(Entity@ this)
{
    SpriteLayer@ sprite = this.getLayer("landmine_sprite");
    sprite.setTextureRect(0, 0, 20, 20);
    sprite.setSize(20, 20);
    sprite.setOrigin((20 / 2), (20 / 2));

    //for explosion animation
    resetTimer(this, "landmineExplosionAnim");
    this.setFloat("landmineExplosionFrame", 0);

}

void onDraw(Entity@ this)
{
    if (this.getBool("blowUpNow"))
    {
        if (getSecondsPast(this, "landmineExplosionAnim") % 0.5 == 0)
        {
            this.setFloat("landmineExplosionFrame", this.getFloat("landmineExplosionFrame") + 1);

        }

    }

}