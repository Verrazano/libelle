//turretInit.as

void onInit(Entity@ this)
{
    //for sound
    this.setString("explode_sound", "res/sounds/woodhit1.wav");

    //for expl damage
    this.setFloat("damage", 20); //not needed as it is deployed as a bullet

    //needed for placing
    this.setBool("no_rotate_on_place", true);

}