//overcover.as

void onInit(Entity@ this)
{
	this.addTag("overcover");
	SpriteLayer@ sprite = this.getLayer("sprite");
	sprite.setZ(2); //overcover level

	this.setTeam(-255); //this is done so the engine knows not use a shader on it so it can fade

	this.setBool("hidden", false);
	this.setUInt("transparency", 255);

}

void onTick(Entity@ this)
{
	if(isServer())
		return;

	uint transparency = this.getUInt("transparency");
	if(this.getBool("hidden"))
	{
		uint min_transparency = 80;
		if(transparency > min_transparency)
		{
			transparency -= 3;
			this.setUInt("transparency", transparency);
			this.getLayer("sprite").setAlpha(transparency);

		}

	}
	else
	{
		if(transparency != 0)
		{
			this.setUInt("transparency", 255);
			this.getLayer("sprite").setAlpha(255);

		}

	}

}

void onCollide(Entity@ this, Entity@ o)
{
	if(isServer())
		return;

	if(isLocalPlayer(o.getPlayer()))
		this.setBool("hidden", true);

}

void onEndCollide(Entity@ this, Entity@ o)
{
	if(isLocalPlayer(o.getPlayer()))
		this.setBool("hidden", false);

}