//guns.as

#include "../../common/timer.as"

void loadGun(string cfg)
{
    if (isClient())
    {
        return;
    }

    //load from cfg, wait on this until I have the other stuff done

}

void addGun(string name,
            uint magCap,
            uint maxMags,
            float fireVel,
            float fireRate,
            uint rpb,
            bool semiAuto,
            float spread,
            float reloadTime,
            float damageMod,
            string ammo,
            string fireSound,
            string reloadSound,
            float range,
            bool deployer = false)
{
    GameMode@ mode = getGameMode();
    //save to gamemode, update gun count

    //iterate the available guns
    uint weaponnum = mode.getUInt("weaponnum");
    weaponnum++;
    mode.setUInt("weaponnum", weaponnum);

    string tag = gunTag(name); //name_

    mode.setString(gunTag2(weaponnum) + "name", name);
    mode.setUInt(tag + "magCap", magCap);
    mode.setUInt(tag + "maxAmmo", magCap);
    mode.setUInt(tag + "maxMags", maxMags);
    mode.setFloat(tag + "fireVel", fireVel);
    mode.setFloat(tag + "fireRate", fireRate);
    mode.setUInt(tag + "rpb", rpb);
    mode.setBool(tag + "semiAuto", semiAuto);
    mode.setFloat(tag + "spread", spread);
    mode.setFloat(tag + "reloadTime", reloadTime);
    mode.setFloat(tag + "damageMod", damageMod);
    mode.setString(tag + "ammo", ammo);
    mode.setString(tag + "fireSound", fireSound);
    mode.setString(tag + "reloadSound", reloadSound);
    mode.setFloat(tag + "range", range);
    mode.setBool(tag + "deployer", deployer);

}


void initForGuns(Entity@ this)   //call this before giving a gun
{
    this.setUInt("weaponnum", 0);
    this.setUInt("curweapon", 0);

    this.setBool("fired", false);
    this.setBool("reloading", false);

    resetTimer(this, "fireTimer");
    resetTimer(this, "reloadTimer");
}

void giveGun(Entity@ this, string gun)
{

    uint weaponnum = this.getUInt("weaponnum");

    GameMode@ mode = getGameMode();

    string tag = gunTag2(weaponnum);
    this.setString(tag + "name", gun);

    uint magCap = mode.getUInt(gunTag(gun) + "magCap");
    uint maxMags = mode.getUInt(gunTag(gun) + "maxMags");

    this.setUInt(tag + "curAmmo", magCap);
    this.setUInt(tag + "curMag", maxMags);

    weaponnum++;
    this.setUInt("weaponnum", weaponnum);
}

string getGunName(Entity@ this, int weaponnum)
{
    return this.getString(gunTag2(weaponnum) + "name");

}

void shoot(Entity@ this, string gun, int weaponnum)
{
    this.setBool("fired", true);
    if (!canFire(this, gun, weaponnum))
    {
        return;
    }

    for (uint i = 0; i < getRPB(gun); i++)
    {
        Entity@ bullet = createEntity(getAmmo(gun));
        if (bullet is null)
        {
            return;
        }

        bullet.setTeam(this.getTeam());

        bullet.addTag("bullet");
        bullet.setFloat("range", getRange(gun));
        bullet.syncFloat("range");

        bullet.setFloat("damage", getDamageMod(gun));
        bullet.syncFloat("damage");

        this.getPlayer() is null ? bullet.setString("shooter", this.getName()) : bullet.setString("shooter", this.getPlayer().name);
        bullet.syncString("shooter");
        bullet.setString("shooterid", this.getID());
        bullet.syncString("shooterid");

        if (!bullet.getBool("no_rotate_on_place"))
        {
            bullet.setAngle(this.getAngle());

        }

        vec2f pos(this.getPosition());
        bullet.setPosition(pos.x, pos.y);
        bullet.setFloat("originX", pos.x);
        bullet.syncFloat("originX");
        bullet.setFloat("originY", pos.y);
        bullet.syncFloat("originY");

        if (isDeployer(gun))
        {
            vec2f mouse(getMouseInView());

            float offset = 25 / 2;//this.getLayer(this.getString("main_sprite")).getSize().x / 2;

            bullet.setPosition(mouse.x + offset, mouse.y - offset);
            bullet.setFloat("originX", bullet.getPosition().x);
            bullet.syncFloat("originX");
            bullet.setFloat("originY", bullet.getPosition().y);
            bullet.syncFloat("originY");

        }

        float spread = applySpread(this, gun);
        float x = cos(toRadians(this.getAngle()) + spread);
        float y = sin(toRadians(this.getAngle()) + spread);

        float speed = getFireVel(gun);
        bullet.setVelocity(x * speed, y * speed);
        resetTimer(this, "fireTimer");

        playFireSound(this, gun);

        //addParticle(pos.x, pos.y, "res/entities/common/weapons/casing.png", 3, false, ((rand()%20)-1)/1000, ((rand()%20)-1)/1000, 10);

        this.setUInt(gunTag2(weaponnum) + "curAmmo", this.getUInt(gunTag2(weaponnum) + "curAmmo") - 1);


        //auto reloading
        if (getMagsLeft(this, weaponnum) == 0)
        {
            return;
        }
        if (getAmmoLeft(this, weaponnum) == 0 || getAmmoLeft(this, weaponnum) > getMaxAmmo(gun))
        {
            reload(this, gun);
        }

    }
}

bool canFire(Entity@ this, string gun, int weaponnum)
{
    if (this.getBool("fired") && timerPastSeconds(this, "fireTimer", getFireRate(gun)))
    {
        this.setBool("fired", false);
    }
    else
    {
        return false;
    }

    if (isReloading(this, gun, weaponnum))
    {
        return false;
    }

    if (getMagsLeft(this, weaponnum) == 0 && getAmmoLeft(this, weaponnum) == 0)
    {
        return false;
    }

    if (getAmmoLeft(this, weaponnum) == 0 || getAmmoLeft(this, weaponnum) > getMagCap(gun))
    {
        reload(this, gun);
        return false;
    }

    if (isDeployer(gun))
    {
        vec2f pos(this.getPosition());
        vec2f mouse(getMouseInView());
        uint distance = sqrt(pow(mouse.x - pos.x, 2) + pow(mouse.y - pos.y, 2));
        if (distance > this.getUInt("equipmentPlaceDist") || distance < 10)
        {
            return false;
        }

    }

    return !this.getBool("fired");
}

string gunTag2(uint weaponnum)
{
    return "weapon" + toString(weaponnum) + "_";
}

string gunTag(string gun)
{
    return gun + "_";
}

void reload(Entity@ this, string gun)
{
    uint weaponnum = this.getUInt("curweapon");
    if (getMagsLeft(this, weaponnum) > 0 && getAmmoLeft(this, weaponnum) != getMaxAmmo(gun))
    {
        this.setBool("reloading", true);
        resetTimer(this, "reloadTimer");
        playReloadSound(this, gun);

    }

}

bool isReloading(Entity@ this, string gun, int weaponnum)
{
    //TODO: optimize by not calling all these get sets, just save bool once and set it
    if (this.getBool("reloading") && timerPastSeconds(this, "reloadTimer", getReloadTime(gun)))
    {
        this.setBool("reloading", false);
        if (getMagsLeft(this, weaponnum) > 0 && getAmmoLeft(this, weaponnum) != getMaxAmmo(gun))
        {
            this.setUInt(gunTag2(weaponnum) + "curMag", getMagsLeft(this, weaponnum) - 1);
            this.setUInt(gunTag2(weaponnum) + "curAmmo", getMagCap(gun));

        }

    }

    return this.getBool("reloading");
}

void cancelReloading(Entity@ this, int weaponnum)
{
    if (getAmmoLeft(this, weaponnum) <= 0)
    {
        return;
    }
    this.setBool("reloading", false);
    this.syncBool("reloading");
}

float applySpread(Entity@ this, string gun)
{
    if (getSpread(gun) == 0)
    {
        return 0;
    }

    float s = rand() % (getSpread(gun) * 1000);
    s /= 1000;
    rand() % 2 == 0 ? s *= -1 : s *= 1;
    return toRadians(s);
}

uint getAmmoLeft(Entity@ this, uint weaponnum)
{
    return this.getUInt(gunTag2(weaponnum) + "curAmmo");
}

uint getMaxAmmo(string gun)
{
    return getGameMode().getUInt(gunTag(gun) + "maxAmmo");
}

uint getMagsLeft(Entity@ this, uint weaponnum)
{
    return this.getUInt(gunTag2(weaponnum) + "curMag");
}

uint getMagCap(string gun)
{
    return getGameMode().getUInt(gunTag(gun) + "magCap");
}

uint getMaxMags(string gun)
{
    return getGameMode().getUInt(gunTag(gun) + "maxMags");
}

float getFireVel(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "fireVel");
}

float getFireRate(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "fireRate");
}

uint getRPB(string gun)
{
    return getGameMode().getUInt(gunTag(gun) + "rpb");
}

bool isSemiAuto(string gun)
{
    return getGameMode().getBool(gunTag(gun) + "semiAuto");
}

float getSpread(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "spread");
}

float getReloadTime(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "reloadTime");
}

float getDamageMod(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "damageMod");
}

string getAmmo(string gun)
{
    return getGameMode().getString(gunTag(gun) + "ammo");
}

float getRange(string gun)
{
    return getGameMode().getFloat(gunTag(gun) + "range");
}

bool isDeployer(string gun)
{
    return getGameMode().getBool(gunTag(gun) + "deployer");
}

void playFireSound(Entity@ this, string gun)
{
    playSound(getGameMode().getString(gunTag(gun) + "fireSound"), this.getPosition().x, this.getPosition().y);
}

void playReloadSound(Entity@ this, string gun)
{
    playSound(getGameMode().getString(gunTag(gun) + "reloadSound"), this.getPosition().x, this.getPosition().y);
}