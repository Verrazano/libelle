//ammo.as

void onInit(Entity@ this)
{
	SpriteLayer@ sprite = this.getLayer("bullet_sprite");
	//sprite.setSize(sprite.getSize().x*spriteScale, sprite.getSize().y*spriteScale);
	//sprite.setOrigin(sprite.getOrigin().x*spriteScale, sprite.getOrigin().y*spriteScale);

}

void onTick(Entity@ this)
{
	//print("wtf");
	if(isClient())
		return;

	float range = this.getFloat("range");
	//print("range " + range);
	if(range == 0)
		return;


	float originX = this.getFloat("originX");
	float originY = this.getFloat("originY");
	vec2f pos(this.getPosition());
	//print("pos.x " + pos.x);

	float dist = sqrt(pow(pos.x - originX, 2) + pow(pos.y - originY, 2));
	//print("dist " + dist);
	if(dist > range)
	{
		this.kill();
	}

}

void onCollide(Entity@ this, Entity@ o)
{
	if(isClient())
		return;

	float range = this.getFloat("range"); //i do this so we know we got all the info needed synced
	if(range == 0)
		return;


	string id = this.getString("shooterid");
	//uint oid = o.getID();

	if((o.hasTag("player") || o.hasTag("damageable")) && !this.isOnTeam(o.getTeam()) && id != o.getID())
	{
		o.health -= this.getFloat("damage");
		o.setString("recenthurt", this.getString("shooter"));
		this.health = -1;

	}
	else if(!this.isOnTeam(o.getTeam()) && id != o.getID() && !o.hasTag("overcover") && !o.hasTag("halfcover"))
	{
		//print("kill here");
		this.health = -1; //TODO: make kill safe to use in onCollide and similar functions
 
	}

}