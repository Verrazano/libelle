// smashSound.as

// requires these two variables
// string smash_sound = "res/sounds/woodhit1.wav"
// float smash_threshhold = 4.8;

// optional
// bool smash_debug = true;

void onInit(Entity@ this)
{
    this.addTag("smashSound");
    Config cfg;

    cfg.file = this.getConfig();
    cfg.load();

    this.setString("smash_sound", cfg.lookupString("smash_sound"));
    this.setFloat("smash_threshhold", cfg.lookupFloat("smash_threshhold"));
    this.setBool("smash_debug", cfg.lookupBool("smash_debug"));

}

void onCollide(Entity@ this, Entity@ o)
{
    if (o.hasTag("cover"))
    {
        return;
    }

    float length = sqrt(pow(this.getVelocity().x, 2) + pow(this.getVelocity().y, 2));
    if (length < this.getFloat("smash_threshhold"))
    {
        return;
    }

    if (this.getBool("smash_debug"))
    {
        print(this.getName() + " smashed into " + o.getName() + " with a velocity length of: " + length);
    }
    playSound(this.getString("smash_sound"), this.getPosition().x, this.getPosition().y);

}