//drachenAnim.as

void onInit(Entity@ this)
{
	if(isServer())
		return;

	SpriteLayer@ sprite = this.getLayer("drachen_sprite");
	sprite.setTextureRect(0, 0, 22, 43);
	sprite.setSize(22, 43);
	sprite.setOrigin(11, 28);

}