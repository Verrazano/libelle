//drachenInit.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    //needed for walkingAnim
    this.setString("main_sprite", "libelle_sprite");

	//needed for healthBar
	this.setFloat("max_health", this.health);

    //needed for playerMovement
    this.setFloat("speed", 900);
    this.setFloat("max_vel", 4.25);

    //needed for walkingAnim
    this.setFloat("frame_width", 19);
    this.setFloat("frame_height", 32);

    //needed for utility
    this.setString("utility", "Jump Jet");
    this.setFloat("util_charge_time", 4);

    //needed for shooting (duh)
    giveGun(this, "submachinegun");
    giveGun(this, "turret_deployer");

    //needed for equipment
    this.setUInt("equipmentPlaceDist", 60);

}