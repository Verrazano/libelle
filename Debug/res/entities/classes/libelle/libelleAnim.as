//libelleAnim.as

void onInit(Entity@ this)
{
	if(isServer())
		return;

	SpriteLayer@ sprite = this.getLayer("libelle_sprite");
	sprite.setTextureRect(0, 0, 19, 32);
	sprite.setSize(19, 32);
	sprite.setOrigin(9.5, 20);

}