//geistInit.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
	//needed for walkingAnim
	this.setString("main_sprite", "geist_sprite");

	//needed for healthBar
	this.setFloat("max_health", this.health);

	//needed for playerMovement
	this.setFloat("speed", 900);
	this.setFloat("max_vel", 6);

	//needed for walkingAnim
	this.setFloat("frame_width", 20);
	this.setFloat("frame_height", 33);

    //needed for utility
    this.setString("utility", "Jump Jet");
    this.setFloat("util_charge_time", 2);

    //needed for shooting (duh)
	giveGun(this, "crossbow");
	giveGun(this, "dual_pistols");

	//needed for equipment
    this.setUInt("equipmentPlaceDist", 70);

}