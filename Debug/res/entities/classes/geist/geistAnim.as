//geistAnim.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    SpriteLayer@ sprite = this.getLayer("geist_sprite");
    sprite.setTextureRect(0, 0, 20, 33);
    sprite.setSize(20, 33);
    sprite.setOrigin(10, 21);

}

void onDraw(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    uint curweapon = this.getUInt("curweapon");
    string gun = getGunName(this, curweapon);

    if (gun == "crossbow")
    {
        SpriteLayer@ sprite = this.getLayer("geist_sprite");
        sprite.setTextureRect(0, 0, 20, 33);

    }
    else if (gun == "pistol" || gun == "dual_pistols")
    {
        SpriteLayer@ sprite = this.getLayer("geist_sprite");
        sprite.setTextureRect(21, 0, 20, 33);

    }

}