//geistAnim.as

void onInit(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    SpriteLayer@ sprite = this.getLayer("joe_sprite");
    sprite.setTextureRect(0, 0, 20, 33);
    sprite.setSize(20, 33);
    sprite.setOrigin(10, 21);

}