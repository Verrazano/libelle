//geistInit.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    //needed for walkingAnim
    this.setString("main_sprite", "geist_sprite");

    //needed for healthBar
    this.setFloat("max_health", this.health);

    //needed for playerMovement
    this.setFloat("speed", 900);
    this.setFloat("max_vel", 6);

    //needed for walkingAnim
    this.setFloat("frame_width", 20);
    this.setFloat("frame_height", 33);

    giveGun(this, "minigun");
    giveGun(this, "pistol");
}