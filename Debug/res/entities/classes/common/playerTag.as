// playerTag.as

void onDraw(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    Player@ player = this.getPlayer();
    if (player is null)
    {
        return;
    }

    //print("drawing name: " + name);
    drawRect(this.getPosition().x - (player.name.length() * 7 / 2), this.getPosition().y - 50, player.name.length() * 8, 15, 80, 80, 80, 80);

    if (isDev(player.name))
    {
        drawText(player.name, this.getPosition().x - (player.name.length() * 7 / 2), this.getPosition().y - 50, 255, 0, 255);
    }
    else
    {
        drawText(player.name, this.getPosition().x - (player.name.length() * 7 / 2), this.getPosition().y - 50);
    }

}