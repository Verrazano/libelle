//jumpJet.as

#include "../../../common/timer.as"

void onInit(Entity@ this)
{
    resetTimer(this, "jumpJetTimer");
    advanceTimer(this, "jumpJetTimer", this.getFloat("util_charge_time"));

    this.setBool("isJumping", false);

}

void onTick(Entity@ this)
{
    Player@ player = this.getPlayer();
    if (player is null || !isLocalPlayer(player))
    {
        return;
    }

    if (player.isKeyPressed(player.action) && !this.getBool("isJumping") && getSecondsPast(this, "jumpJetTimer") > this.getFloat("util_charge_time"))
    {
        resetTimer(this, "jumpJetTimer");
        float a = atan2(getMouseInView().y - this.getPosition().y, getMouseInView().x - this.getPosition().x);
        float speed = this.getFloat("speed");
        float yforce = sin(a) * (speed * 30);
        float xforce = cos(a) * (speed * 30);
        this.applyForce(xforce, yforce);

        this.setBool("isJumping", true);
        this.addTag("disable controls");

    }

    if (this.getBool("isJumping") && getSecondsPast(this, "jumpJetTimer") > 0.4)
    {
        this.setBool("isJumping", false);
        this.remTag("disable controls");

    }

}

bool doesCollide(Entity@ this, Entity@ other)
{
    if (other.hasTag("wall"))
    {
        this.remTag("disable controls");
    }

    if (this.getBool("isJumping") && !other.hasTag("wall"))
    {
        return false;
    }
    else
    {
        return true;
    }

}