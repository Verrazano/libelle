//walkingAnim.as

void onInit(Entity@ this)
{
	if(isServer())
		return;

	//used for physical animation and sounds
	this.setFloat("stepaccum", 0);
	this.setFloat("xpos", this.getPosition().x);
	this.setFloat("ypos", this.getPosition().y);
	this.setBool("isBig", false);

}

void onTick(Entity@ this)
{
	if(isServer())
		return;

	float stepaccum = this.getFloat("stepaccum");
	float newstepaccum = stepaccum;

	vec2f pos;
	pos.x = this.getFloat("xpos");
	pos.y = this.getFloat("ypos");

	vec2f newpos(this.getPosition());
	this.setFloat("xpos", newpos.x);
	this.setFloat("ypos", newpos.y);

	if(!(pos.x == newpos.x && pos.y == newpos.y))
	{
		float dist = sqrt(pow(newpos.x - pos.x, 2) + pow(newpos.y - pos.y, 2));
		newstepaccum += dist;

	}

	if(newstepaccum > this.getFloat("speed")/10) //aprox 1 meter traveled for 1 step sound
	{
		newstepaccum = 0; //reset the stepaccum
		playSound("res/sounds/step.wav", newpos.x, newpos.y);

		bool isBig = this.getBool("isBig");
		this.setBool("isBig", !isBig);
		if(isBig) //make small
		{
			SpriteLayer@ sprite = this.getLayer(this.getString("main_sprite"));
			if(sprite !is null)
			{
				sprite.setSize(this.getFloat("frame_width"), this.getFloat("frame_height"));

			}

		}
		else //make big
		{
			SpriteLayer@ sprite = this.getLayer(this.getString("main_sprite"));
			if(sprite !is null)
			{
				sprite.setSize(this.getFloat("frame_width")*1.02, this.getFloat("frame_height")*1.02);

			}

		}

	}


	if(newstepaccum != stepaccum)
		this.setFloat("stepaccum", newstepaccum);

}