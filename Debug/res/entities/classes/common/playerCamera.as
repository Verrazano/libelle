//playerCamera.as

void onInit(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    this.setBool("leanoff_camera", true);
    this.setFloat("camera_width", 300);
    this.setFloat("camera_height", 225);

}

void onTick(Entity@ this)
{
    //if this is the server or no player or not the local player
    if (isServer() || this.getPlayer() is null || !isLocalPlayer(this.getPlayer()))
    {
        return;
    }

	Player@ p = this.getPlayer();

	/* Set me to false to disable xbox controller default */
	bool xbox = false;

    vec2f pos(this.getPosition());
    float width = this.getFloat("camera_width");
    float height = this.getFloat("camera_height");
	
    if (this.getBool("leanoff_camera"))
    {
        vec2f mouse(getMouseInView());
        vec2f leanoff;

		if(xbox)
		{
			leanoff.x = (mouse.x + pos.x) / 2;
			leanoff.y = (mouse.y + pos.y) / 2;

			// Up on the view-joystick
			if(p.isJoystickButtonPressed(106))
			{
				//leanoff.x = leanoff.x + 100;
			}
			if(p.isJoystickButtonPressed(107))
			{
				//leanoff.x = leanoff.x + 100;
			}
			if(p.isJoystickButtonPressed(108))
			{
				leanoff.x = leanoff.x - 100;
			}
			if(p.isJoystickButtonPressed(109))
			{
				leanoff.x = leanoff.x + 100;
			}
		}
		else
		{
			leanoff.x = (mouse.x + pos.x) / 2;
			leanoff.y = (mouse.y + pos.y) / 2;

        //TODO: make the camera snap to 0, 0 when the mouse is to close to the player

        //sides
        //right
		
			if (mouse.x - pos.x > width && !(mouse.y - pos.y > height || mouse.y < pos.y - height))
			{
				setView(pos.x, leanoff.y - height / 2, width, height);
				return;

			}
			//left
			else if (mouse.x < pos.x - width && !(mouse.y - pos.y > height || mouse.y < pos.y - height))
			{
				setView(pos.x - width, leanoff.y - height / 2, width, height);
				return;

			}
			//bot
			if (mouse.y - pos.y > height && !(mouse.x < pos.x - width || mouse.x - pos.x > width))
			{
				setView(leanoff.x - width / 2, pos.y, width, height);
				return;

			}
			//top
			else if (mouse.y < pos.y - height && !(mouse.x < pos.x - width || mouse.x - pos.x > width))
			{
				setView(leanoff.x - width / 2, pos.y - height, width, height);
				return;

			}

			//corners
			//botright
			if (mouse.x - pos.x > width && mouse.y - pos.y > height)
			{
				setView(pos.x, pos.y, width, height);
				return;

			}
			//botleft
			if (mouse.x < pos.x - width && mouse.y - pos.y > height)
			{
				setView(pos.x - width, pos.y, width, height);
				return;

			}
			//topright
			if (mouse.y < pos.y - height && mouse.x - pos.x > width)
			{
				setView(pos.x, pos.y - height, width, height);
				return;

			}
			//topleft
			if (mouse.y < pos.y - height && mouse.x < pos.x - width)
			{
				setView(pos.x - width, pos.y - height, width, height);
				return;

			}
		
		}
	
		setView(leanoff.x - width / 2, leanoff.y - height / 2, width, height);
        return;

    }
	
    setView(pos.x - width / 2, pos.y - height / 2, width, height);

}