//playerMovement.as

void onInit(Entity@ this)
{
    this.addTag("player");
    this.addTag("damageable");
		
}

void onTick(Entity@ this)
{
	
	/* Set me to false to disable xbox controller default */
	bool xbox = false;

    if (isServer())
    {
        return;
    }

    Player@ player = this.getPlayer();
	
    if (player is null || !isLocalPlayer(player) || this.hasTag("disable controls"))
    {
        return;
    }
	
    float a = toDegrees(atan2(getMouseInView().y - this.getPosition().y, getMouseInView().x - this.getPosition().x));
    this.setAngle(-a);

    float speed = this.getFloat("speed");
    float max_vel = this.getFloat("max_vel");

    bool moving = false;

	player.setJoystickControlsEnabled(xbox);
	
    //move the player if they are pressing a key	
    if (player.isKeyPressed(player.up) || player.isJoystickButtonPressed(player.up))
    {
        this.applyForce(0, -speed);
        moving = true;

    }
    else if (player.isKeyPressed(player.down) || player.isJoystickButtonPressed(player.down))
    {
        this.applyForce(0, speed);
        moving = true;

    }

    if (player.isKeyPressed(player.left) || player.isJoystickButtonPressed(player.left))
    {
        this.applyForce(-speed, 0);
        moving = true;

    }
    else if (player.isKeyPressed(player.right) || player.isJoystickButtonPressed(player.right))
    {
        this.applyForce(speed, 0);
        moving = true;

    }
	else if(!player.isJoystickConnected(0))
	{
		xbox = false;
	}
	//Enables xbox controls
	else if(player.isJoystickButtonPressed(8))
	{
		xbox = true;
	}

    if (!moving)
    {
        this.setVelocity(0, 0);
        return; //so we don't bother with the stuff below

    }

    //stop the player from going faster than max_vel
    float length = sqrt(pow(this.getVelocity().x, 2) + pow(this.getVelocity().y, 2));
    if (length > 15)
    {
        vec2f vel(this.getVelocity());
        vel.x *= (1 / length) * max_vel;
        vel.y *= (1 / length) * max_vel;
        this.setVelocity(vel.x, vel.y);

    }

}
