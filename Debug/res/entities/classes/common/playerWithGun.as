//playerWithGun.as

#include "../../common/guns.as"

void onInit(Entity@ this)
{
    if (isServer())
    {
        return;
    }

    this.setBool("switchweaponpressed", false);
    this.setBool("reloadpressed", false);
    initForGuns(this);

}

void onTick(Entity@ this)
{
    if (isServer() || this.getPlayer() is null || !isLocalPlayer(this.getPlayer()))
    {
        return;
    }

    Player@ p = this.getPlayer();

    uint weaponnum = this.getUInt("weaponnum");
    if (weaponnum == 0)
    {
        return;
    }

    uint curweapon = this.getUInt("curweapon");
    string gun = getGunName(this, curweapon);

    if (p.fireKeyPressed() || p.isJoystickButtonPressed(1))
    {
        if (!this.getBool("fired") && isSemiAuto(gun))
        {
            cancelReloading(this, curweapon);
            shoot(this, gun, curweapon);
            return;

        }
        else if (!isSemiAuto(gun))
        {
            cancelReloading(this, curweapon);
            shoot(this, gun, curweapon);
            return;

        }

    }
	
    else if (this.getBool("fired"))
    {
        this.setBool("fired", false);

    }

    if (p.isKeyPressed(p.reload))
    {
        if (!this.getBool("reloadpressed") && !isReloading(this, gun, curweapon) && getMagsLeft(this, curweapon) != 0)
        {
            this.setBool("reload", true);
            reload(this, gun);
            return;

        }

    }
	
    else if (this.getBool("reload"))
    {
        this.setBool("reload", false);

    }


    if (p.isKeyPressed(p.nextweapon))
    {
        if (!this.getBool("swithcweaponpressed"))
        {
            cancelReloading(this, curweapon);
            this.setBool("swithcweaponpressed", true);
            this.syncBool("swithcweaponpressed");
            nextWeapon(this);
            return;

        }

    }
    else if (p.isKeyPressed(p.prevweapon))
    {
        if (!this.getBool("swithcweaponpressed"))
        {
            cancelReloading(this, curweapon);
            this.setBool("swithcweaponpressed", true);
            this.syncBool("swithcweaponpressed");
            prevWeapon(this);
            return;

        }

    }
    else if (this.getBool("swithcweaponpressed"))
    {
        this.setBool("swithcweaponpressed", false);
        this.syncBool("swithcweaponpressed");

    }

    isReloading(this, gun, curweapon);
}

void nextWeapon(Entity@ this)
{
    uint i = this.getUInt("curweapon") + 1;
    if (i == this.getUInt("weaponnum"))
    {
        i = 0;
    }

    this.setUInt("curweapon", i);
    this.syncUInt("curweapon");

}

void prevWeapon(Entity@ this)
{
    uint i = this.getUInt("curweapon") - 1;
    if (i > this.getUInt("weaponnum"))
    {
        i = this.getUInt("weaponnum") - 1;
    }

    this.setUInt("curweapon", i);
    this.syncUInt("curweapon");

}