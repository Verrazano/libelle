//healthbar.as

void onDraw(Entity@ this)
{
    float percent = this.health / this.getFloat("max_health");

    // draws colored healthbar according to percent of health left (changes from green to red)
    drawRect(this.getPosition().x - (75 * percent / 2), this.getPosition().y - 28, 75 * percent, 2, 255 * abs(1 - percent), 255 * percent, 0, 255);
}