void onInit(Entity@ this)
{
    //needed for walkingAnim
    this.setString("main_sprite", "zombie_sprite");

	//needed for healthBar
	this.setFloat("max_health", this.health);

    //needed for playerMovement
    this.setFloat("speed", 900);
    this.setFloat("max_vel", 2.25);

    //needed for walkingAnim
    this.setFloat("frame_width", 22);
    this.setFloat("frame_height", 43);

    giveGun(this, "minigun");
    giveGun(this, "landmine_deployer");

    //needed for equipment
    this.setUInt("equipmentPlaceDist", 55);

}