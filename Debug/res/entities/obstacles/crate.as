//crate.as

void onInit(Entity@ this)
{
	SpriteLayer@ sprite = this.getLayer("crate_sprite");
	sprite.setTextureRect((rand()%3)*43, 0, 43, 43);
	sprite.setSize(43, 43);
	sprite.setOrigin((43/2), (43/2));
}