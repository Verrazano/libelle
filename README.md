To build libelle you will need the following libraries
SFML 2.1
Box2D ... (need to figure out what version we are on currently)
AngelScript 2.28.0
enet ... (need to figure out what version we are on currently)

you will need to link to the following libraries to build
Angelscript
Box2D
enet
sfml-network
sfml-audio
sfml-graphics
sfml-window
sfml-system

additionally on windows you need to link to windows socket libraries for enet to work
ws2_32
winmm

on linux you will need to link to pthread for Angelscript and sfml-threads

If you wish to build Libelle as a standalone define
NOLAUNCHER_AVAILABLE

otherwise you will need to build it as a .dll or .so2 portable library
and build the libelle launcher using curl seperately.