#ifndef GAMEMODE_H_
#define GAMEMODE_H_

#include <angelscript.h>
#include <assert.h>
#include <list>
#include <vector>
#include "Entity/VarManager/VarManager.h"
#include "Engine/Engine.h"
#include "ScriptEngine/ScriptEngine.h"
#include "GameModeSettings.h"

class GameMode : public VarManager
{
public:
	static void registerGameMode(asIScriptEngine* engine);

	GameMode(std::string config);
	GameMode(std::string config, std::string file);

	std::string tosync;

	void syncInt(std::string s)
	{
		int i = getInt(s);
		std::string syncstr = "int^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncint: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncUInt(std::string s)
	{
		//std::cout << "syncuint\n";
		unsigned int i = getUInt(s);
		std::string syncstr = "uint^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncuint: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncFloat(std::string s)
	{
		//std::cout << "syncfloat\n";
		float i = getFloat(s);
		std::string syncstr = "float^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncfloat: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncString(std::string s)
	{
		//std::cout << "syncstring\n";
		std::string i = getString(s);
		std::string syncstr = "string^&"+s+"^&"+i+"^&"; //.. hopefully no one will ever use this in a string character
		//std::cout << "syncstring: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncBool(std::string s)
	{
		//std::cout << "syncbool\n";
		bool i = getBool(s);
		std::string syncstr = "bool^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncbool: " << syncstr << "\n";
		tosync += syncstr;

	}

	void onInit();
	void onTick();
	void onDraw();
	void isGameOver();
	void onReset();
	void onEntityDie(Entity* o); //TODO update this on rem_entity call
	void onPlayerJoin(Player* p);
	void onPlayerLeave(Player* p);

	std::string getConfig();
	std::string getName();
	bool getState();

	unsigned int getPlayerCount(); //TODO match these with the ones in game or remove them from GameMode
	Player* getPlayer(unsigned int i);

	std::string configasstring;

private:
	void read();
	void read(std::string file);
	void build();

	GameModeSettings settings;

	std::string config;

	bool gameOverBool;

	std::vector<Player*> players;

	std::list<asIScriptFunction*> onInitCalls;
	std::list<asIScriptFunction*> onTickCalls;
	std::list<asIScriptFunction*> onDrawCalls;
	std::list<asIScriptFunction*> isGameOverCalls;
	std::list<asIScriptFunction*> onResetCalls;
	std::list<asIScriptFunction*> onEntityDieCalls;
	std::list<asIScriptFunction*> onPlayerJoinCalls;
	std::list<asIScriptFunction*> onPlayerLeaveCalls;

};

#endif /* GAMEMODE_H_ */
