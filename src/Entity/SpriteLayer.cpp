#include "SpriteLayer.h"
#include "../Engine/ResourceManager/ResourceManager.h"
#include "../Util/convert/convertstring.h"
#include <math.h>
#include "../Util/convert/units.h"
#include <assert.h>
#include "../Engine/Engine.h"
#include <iostream>
#include "../ScriptEngine/ScriptEngine.h"


std::vector<SpriteLayer*> SpriteLayer::sprites;

void SpriteLayer::registerSpriteLayer(asIScriptEngine* engine)
{
	assert(engine->RegisterGlobalProperty("float spriteScale", &spriteScale) >= 0);
	assert(engine->RegisterObjectType("SpriteLayer", sizeof(SpriteLayer), asOBJ_REF | asOBJ_NOCOUNT) >= 0);

	assert(engine->RegisterObjectMethod("SpriteLayer", "vec2f getPosition()", asMETHOD(SpriteLayer, getLocalPosition), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setPosition(float, float)", asMETHODPR(SpriteLayer, setLocalPosition, (float, float), void), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setAngle(float)", asMETHOD(SpriteLayer, setLocalAngle), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "int getAngle()", asMETHOD(SpriteLayer, getLocalAngle), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setAlpha(uint)", asMETHOD(SpriteLayer, setAlpha), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "uint getAlpha()", asMETHOD(SpriteLayer, getAlpha), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setZ(float)", asMETHOD(SpriteLayer, setZ), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "float getZ()", asMETHOD(SpriteLayer, getZ), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "float setSize(float, float)", asMETHODPR(SpriteLayer, setSize, (float, float), void), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setTextureRect(uint, uint, uint, uint)", asMETHODPR(SpriteLayer, setTextureRect, (unsigned int, unsigned int, unsigned int, unsigned int), void), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "void setOrigin(float, float)", asMETHOD(SpriteLayer, setOrigin), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "vec2f getSize()", asMETHOD(SpriteLayer, getSize), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("SpriteLayer", "vec2f getOrigin()", asMETHOD(SpriteLayer, getOrigin), asCALL_THISCALL) >= 0);

}

void SpriteLayer::drawSprites()
{
	for(unsigned int i = 0; i < sprites.size(); i++)
	{
		if(isClient())
		{
			sf::Vector2f pos = Engine::get()->gameview.getCenter();
			sf::Vector2f size = Engine::get()->gameview.getSize();
			pos.x -= size.x/2;
			pos.y -= size.x/2;
			size.y += 200;
			sf::FloatRect t(pos.x, pos.y, size.x, size.y);
			if(sprites[i]->getGlobalBounds().intersects(t))
			{
				sf::Shader* teamshader = &Engine::get()->teamshader;
				Engine::get()->prepareTeamShader(sprites[i]->team);

				if(teamshader->isAvailable() && sprites[i]->team != -255)
				{
					Engine::get()->window.draw(*sprites[i], teamshader);

				}
				else
				{
					Engine::get()->window.draw(*sprites[i]);

				}

			}

		}

	}

}

SpriteLayer::SpriteLayer(bool zorder) : sf::RectangleShape()
{
	localAngle = 0;
	z = 0;
	team = -1;
	if(zorder)
	{
		addSprite(this);

	}

}

SpriteLayer::SpriteLayer(sf::Vector2f v, bool zorder) : sf::RectangleShape(v)
{
	localAngle = 0;
	z = 0;
	team = -1;
	if(zorder)
	{
		addSprite(this);

	}

}

SpriteLayer::~SpriteLayer()
{
	remSprite(this);

}

void SpriteLayer::setSize(float x, float y)
{
	sf::RectangleShape::setSize(sf::Vector2f(x, y));

}

void SpriteLayer::setTextureRect(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
{
	sf::RectangleShape::setTextureRect(sf::IntRect(x, y, width, height));

}

void SpriteLayer::setOrigin(float x, float y)
{
	sf::RectangleShape::setOrigin(x, y);

}

void SpriteLayer::setPosition(sf::Vector2f v)
{
	float h;
	if(localPos.x == 0 && localPos.y == 0)
	{
		sf::RectangleShape::setPosition(v);

	}
	else
	{
		h = sqrt(pow(localPos.x, 2) + pow(localPos.y, 2));
		float a = atan2f(localPos.y, localPos.x) + toRadians(getRotation()); //using polar cordinates to position part sprites
		sf::Vector2f p(h*cos(a) + v.x, h*sin(a) + v.y);

		sf::RectangleShape::setPosition(p);

	}

}

void SpriteLayer::setPosition(float x, float y)
{
	sf::Vector2f v(x, y);
	SpriteLayer::setPosition(v);

}

void SpriteLayer::setLocalPosition(sf::Vector2f v)
{
	sf::Vector2f old = localPos;
	sf::Vector2f p = getPosition();
	p -= old;
	localPos = v;
	p += localPos;
	sf::RectangleShape::setPosition(p);

}

void SpriteLayer::setLocalPosition(float x, float y)
{
	setLocalPosition(sf::Vector2f(x, y));

}

vec2f SpriteLayer::getLocalPosition()
{
	return vec2f(localPos);

}

void SpriteLayer::setRotation(float a)
{
	sf::RectangleShape::setRotation(-(a + localAngle - 90));

}

void SpriteLayer::setLocalAngle(float a)
{
	float old = getLocalAngle();
	float p = getRotation();
	p -= old;
	localAngle = a;
	sf::RectangleShape::setRotation(p);

}

float SpriteLayer::getLocalAngle()
{
	return localAngle;

}

void SpriteLayer::setAlpha(unsigned int a)
{
	this->setFillColor(sf::Color(255, 255, 255, a));
}

unsigned int SpriteLayer::getAlpha()
{
	return this->getFillColor().a;

}

void SpriteLayer::setZ(float z)
{
	this->z = z;
	findSpot(this);

}

float SpriteLayer::getZ()
{
	return z;

}

void SpriteLayer::findSpot(SpriteLayer* sprite)
{
	std::sort(sprites.begin(), sprites.end(), compare);

}

unsigned int SpriteLayer::getIndex(SpriteLayer* sprite)
{
	for(unsigned int i = 0; i < sprites.size(); i++)
	{
		if(sprites[i] == sprite)
			return i;

	}

	return 0;

}

void SpriteLayer::addSprite(SpriteLayer* sprite)
{
	sprites.push_back(sprite);
	findSpot(sprite);

}

void SpriteLayer::remSprite(SpriteLayer* sprite)
{
	std::vector<SpriteLayer*>::iterator it;
	for(it = sprites.begin(); it != sprites.end(); it++)
	{
		if((*it) == sprite)
		{
			sprites.erase(it);
			return;

		}

	}

}

bool SpriteLayer::compare(SpriteLayer* s, SpriteLayer* s2)
{
    return s->z < s2->z;

}
