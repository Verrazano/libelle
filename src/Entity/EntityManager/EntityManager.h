#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

#include "../Entity.h"

class EntityType;

class EntityManager
{
public:
	EntityManager();

	void loadEntityType(std::string path);
	void loadEntityTypesFromVec(); //uses configsasline vec

	Entity* createEntity(std::string type, float x = 0, float y = 0, float a = 0);
	Entity* createEntity(std::string type, std::string id, float x = 0, float y = 0);
	Entity* getEntityByID(std::string id);
	unsigned int getNumOfEntities();
	Entity* getEntityByIndex(unsigned int i);
	void remEntity(Entity* e);

	void onTick();
	void onDraw();

	void clearEntities();
	void clearEntityTypes();

	std::vector<std::pair<std::string, std::string> > configsasline;

private:
	std::vector<EntityType*> types;
	std::vector<Entity*> entities;

};

#endif /* ENTITYMANAGER_H_ */
