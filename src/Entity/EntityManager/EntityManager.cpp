#include "EntityManager.h"
#include "../EntityType/EntityType.h"
#include <iostream>

void EntityManager::clearEntities()
{
	while(!entities.empty())
	{
		delete (*entities.begin());
		entities.erase(entities.begin());

	}

	entities.clear();

}

void EntityManager::clearEntityTypes()
{
	clearEntities();
	while(!types.empty())
	{
		delete (*types.begin());
		types.erase(types.begin());

	}

	types.clear();
	configsasline.clear();

}

EntityManager::EntityManager()
{
}

void EntityManager::loadEntityType(std::string path)
{
	for(unsigned int i = 0; i < types.size(); i++)
	{
		if(types[i]->getPath() == path)
		{
			return;

		}

	}
	EntityType* t = new EntityType(path, Config::load(path));
	configsasline.push_back(std::pair<std::string, std::string>(t->getPath(), t->getFile()));
	types.push_back(t);

}

void EntityManager::loadEntityTypesFromVec()
{
	for(unsigned int u = 0; u < configsasline.size(); u++)
	{
		std::string path = configsasline[u].first;
		std::string file = configsasline[u].second;
		bool skip = false;
		for(unsigned int i = 0; i < types.size(); i++)
		{
			if(types[i]->getPath() == path)
			{
				skip = true;
				break;

			}

		}
		if(!skip)
		{
			types.push_back(new EntityType(path, file));


		}

	}

}

Entity* EntityManager::createEntity(std::string type, float x, float y, float a)
{
	for(unsigned int i = 0; i < types.size(); i++)
	{
		if(types[i]->getName() == type)
		{
			//std::cout << "entity actually created " << type << "\n";
			Entity* e = new Entity(types[i], x, y, a);
			entities.push_back(e);
			return e;

		}

	}

	std::cout << "can't create entity: " << type << "\n";
	return NULL;

}

Entity* EntityManager::createEntity(std::string type, std::string id, float x, float y)
{
	for(unsigned int i = 0; i < types.size(); i++)
	{
		if(types[i]->getName() == type)
		{
			Entity* e = new Entity(types[i], id, x, y);
			entities.push_back(e);
			return e;

		}

	}

	std::cout << "can't create entity: " << type << "\n";
	return NULL;

}

Entity* EntityManager::getEntityByID(std::string id)
{
	for(unsigned int i = 0; i < entities.size(); i++)
	{
		if(entities[i]->getID() == id)
			return entities[i];

	}

	return NULL;

}

unsigned int EntityManager::getNumOfEntities()
{
	return entities.size();

}

Entity* EntityManager::getEntityByIndex(unsigned int i)
{
	if(i < entities.size())
	{
		return entities[i];

	}
	return NULL;

}

void EntityManager::onTick()
{
	for(unsigned int i = 0; i < entities.size(); i++)
	{
		entities[i]->onTick();

	}

}

void EntityManager::onDraw()
{
	for(unsigned int i = 0; i < entities.size(); i++)
		entities[i]->updateSprites();
	SpriteLayer::drawSprites();
	for(unsigned int i = 0; i < entities.size(); i++)
		entities[i]->onDraw();

}

void EntityManager::remEntity(Entity* e)
{
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it) == e)
		{
			std::cout << "killing: " << e->getName() << "\n";
			//Engine::get()->world->DestroyBody((*it)->body);
			delete (*it);
			entities.erase(it);
			break;

		}

	}

}
