#include "TeamEntity.h"

TeamEntity::TeamEntity(int team)
{
	setTeam(team);

}

int TeamEntity::getTeam()
{
	return team;

}

void TeamEntity::setTeam(int team)
{
	this->team = team;

}

bool TeamEntity::isOnTeam(int team)
{
	return  getTeam() == team && getTeam() != -1 && team != -1;

}
