#ifndef ENTITYCONTACTLISTENER_H_
#define ENTITYCONTACTLISTENER_H_

#include <Box2D/Box2D.h>
#include "Entity.h"

class EntityContactListener : public b2ContactListener, public b2ContactFilter
{
public:
	/// Called when two fixtures begin to touch.
	void BeginContact(b2Contact* contact)
	{
		Entity* myobj = static_cast<Entity*>(contact->GetFixtureA()->GetBody()->GetUserData());
		Entity* o = static_cast<Entity*>(contact->GetFixtureB()->GetBody()->GetUserData());

		myobj->onCollide(o);
		o->onCollide(myobj);

	}

	/// Called when two fixtures cease to touch.
	void EndContact(b2Contact* contact)
	{
		//do this later not important right now.
		Entity* myobj = static_cast<Entity*>(contact->GetFixtureA()->GetBody()->GetUserData());
		Entity* o = static_cast<Entity*>(contact->GetFixtureB()->GetBody()->GetUserData());

		myobj->onEndCollide(o);
		o->onEndCollide(myobj);

	}

	bool ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB)
	{
		Entity* myobj = static_cast<Entity*>(fixtureA->GetBody()->GetUserData());
		Entity* o = static_cast<Entity*>(fixtureB->GetBody()->GetUserData());

		bool r1 = myobj->doesCollide(o);
		bool r2 = o->doesCollide(myobj);

		if(r1 && r2)
			return true;

		return false;

	}

};

#endif /* ENTITYCONTACTLISTENER_H_ */
