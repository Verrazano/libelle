#include "EntityType.h"
#include "../../Util/Config/Config.h"
#include "../../Engine/Engine.h"
#include "../../Util/convert/b2tosf.h"
#include "../../Util/convert/units.h"
#include "../../ScriptEngine/ScriptEngine.h"
#include <sstream>
#include <iostream>
#include "../../add_on/scriptbuilder/scriptbuilder.h"

std::string EntityType::getFile()
{
	return cfg.getFile();

}

EntityType::EntityType(std::string path, std::string file)
{
	cfg.setPath(path);
	cfg.parse(file);
	read();
	build();

}

EntityType::~EntityType()
{
	while(!onInitCalls.empty())
	{
		//(*onInitCalls.begin())->Release();
		onInitCalls.erase(onInitCalls.begin());

	}

	onInitCalls.clear();

	while(!onTickCalls.empty())
	{
		//(*onTickCalls.begin())->Release();
		onTickCalls.erase(onTickCalls.begin());

	}

	onTickCalls.clear();

	while(!onDrawCalls.empty())
	{
		//(*onDrawCalls.begin())->Release();
		onDrawCalls.erase(onDrawCalls.begin());

	}

	onDrawCalls.clear();

	while(!onDeathCalls.empty())
	{
		//(*onDeathCalls.begin())->Release();
		onDeathCalls.erase(onDeathCalls.begin());

	}

	onDeathCalls.clear();

	while(!onCollideCalls.empty())
	{
		//(*onCollideCalls.begin())->Release();
		onCollideCalls.erase(onCollideCalls.begin());

	}

	onCollideCalls.clear();

	while(!onEndCollideCalls.empty())
	{
		//(*onEndCollideCalls.begin())->Release();
		onEndCollideCalls.erase(onEndCollideCalls.begin());

	}

	onEndCollideCalls.clear();

	while(!doesCollideCalls.empty())
	{
		//(*doesCollideCalls.begin())->Release();
		doesCollideCalls.erase(doesCollideCalls.begin());

	}

	doesCollideCalls.clear();


	/*while(!fixtures.empty())
	{
		delete (*fixtures.begin());
		fixtures.erase(fixtures.begin());

	}*/

	fixtures.clear();
}

void EntityType::onInit(Entity* myobj)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onInitCalls.size() != 0)
		for(it = onInitCalls.begin(); it != onInitCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}
	ctx->Release();

}

void EntityType::onTick(Entity* myobj)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onTickCalls.size() != 0)
		for(it = onTickCalls.begin(); it != onTickCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}

	if(myobj->onTickCalls.size() != 0)
		for(it = myobj->onTickCalls.begin(); it != myobj->onTickCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}
	ctx->Release();

}

void EntityType::onDraw(Entity* myobj)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onDrawCalls.size() != 0)
		for(it = onDrawCalls.begin(); it != onDrawCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}

	if(myobj->onDrawCalls.size() != 0)
		for(it = myobj->onDrawCalls.begin(); it != myobj->onDrawCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}
	ctx->Release();

}

void EntityType::onDeath(Entity* myobj)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onDeathCalls.size() != 0)
		for(it = onDeathCalls.begin(); it != onDeathCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}

	if(myobj->onDeathCalls.size() != 0)
		for(it = myobj->onDeathCalls.begin(); it != myobj->onDeathCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->Execute();

		}
	ctx->Release();

}

void EntityType::onCollide(Entity* myobj, Entity* o)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onCollideCalls.size() != 0)
		for(it = onCollideCalls.begin(); it != onCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();

		}

	if(myobj->onCollideCalls.size() != 0)
		for(it = myobj->onCollideCalls.begin(); it != myobj->onCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();

		}
	ctx->Release();

}

void EntityType::onEndCollide(Entity* myobj, Entity* o)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onEndCollideCalls.size() !=  0)
		for(it = onEndCollideCalls.begin(); it != onEndCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();

		}

	if(myobj->onEndCollideCalls.size() != 0)
		for(it = myobj->onEndCollideCalls.begin(); it != myobj->onEndCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();

		}
	ctx->Release();

}

bool EntityType::doesCollide(Entity* myobj, Entity* o)
{
	std::list<asIScriptFunction*>::iterator it;
	//std::cout << "doescollidecalls: " << doesCollideCalls.size() << "\n";
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(doesCollideCalls.size() != 0)
		for(it = doesCollideCalls.begin(); it != doesCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();
			bool shouldreturn = true;
			shouldreturn = (bool)ctx->GetReturnByte();


			if(!shouldreturn)
				return false;

		}

	if(myobj->doesCollideCalls.size() != 0)
		for(it = myobj->doesCollideCalls.begin(); it != myobj->doesCollideCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, myobj);
			ctx->SetArgAddress(1, o);
			ctx->Execute();

			bool shouldreturn = true;
			shouldreturn = (bool)ctx->GetReturnByte();

			if(!shouldreturn)
				return false;

		}
	ctx->Release();

	return true;

}

void EntityType::read()
{
	settings.read(cfg);

	for(unsigned int i = 0; i < cfg.getNumOfItems(); i++)
	{
		ConfigItem item = cfg.getItem(i);
		if(item.getType() == "fixture@")
		{
			b2FixtureDef fixture;
			fixture.friction = item.paramAsFloat(1);
			fixture.restitution = item.paramAsFloat(2);
			fixture.density = item.paramAsFloat(3);
			fixture.isSensor = item.paramAsBool(4);

			b2Shape* shape = NULL;

			std::string shapeKey = item.getParam(0);
			ConfigItem shapeItem = cfg.getItem(shapeKey);
			if(shapeItem.getType() == "shape@")
			{
				std::string shapeType = shapeItem.getParam(0);
				if(shapeType == "circ")
				{
					std::cout << "entitiy " << settings.name << " has a circle body.\n";
					shape = new b2CircleShape;
					static_cast<b2CircleShape*>(shape)->m_radius = toMeters(shapeItem.paramAsFloat(1));
					if(shapeItem.getNumOfParams() > 3)
					{
						static_cast<b2CircleShape*>(shape)->m_p.Set(toMeters(shapeItem.paramAsFloat(2)), -toMeters(shapeItem.paramAsFloat(3)));

					}
					else
					{
						static_cast<b2CircleShape*>(shape)->m_p.Set(0, 0);

					}

				}
				else if(shapeType == "rect")
				{
					shape = new b2PolygonShape;

					float width = toMeters(shapeItem.paramAsFloat(1))/2;
					float height = toMeters(shapeItem.paramAsFloat(2))/2;

					float x = 0, y = 0, a = 0;

					if(shapeItem.getNumOfParams() > 5)
					{
						x = toMeters(shapeItem.paramAsFloat(3));
						y = -toMeters(shapeItem.paramAsFloat(4));
						a = toMeters(shapeItem.paramAsFloat(5));


					}
					static_cast<b2PolygonShape*>(shape)->SetAsBox(width, height, b2Vec2(x, y), a);

				}

			}

			if(shape != NULL)
			{
				fixture.shape = shape;
				fixtures.push_back(fixture);

			}

		}
		else if(item.getType() == "sprite@")
		{
			float x = item.paramAsFloat(0);
			float y = item.paramAsFloat(1);
			float originx = item.paramAsFloat(2);
			float originy = item.paramAsFloat(3);
			std::string text = item.getParam(4);
			bool mask = item.paramAsBool(5);

			sf::Texture* temp = Engine::get()->files.get<sf::Texture>(text);
			SpriteLayer t(sf::Vector2f(temp->getSize()), false);
			t.setTexture(temp);
			t.setOrigin(originx, originy);
			t.setLocalPosition(x, y);
			layers.push_back(std::pair<std::string, SpriteLayer>(item.getKey(), t));

		}

	}

	settings.bdef.type = settings.isDynamic ? b2_dynamicBody : b2_staticBody;

}

void EntityType::build()
{
	for(unsigned int i = 0; i < settings.scripts.size(); i++)
	{
		asIScriptModule* mod;

		if(ScriptEngine::get()->GetModule(settings.scripts[i].c_str()) == NULL)
		{
			CScriptBuilder builder;
			builder.StartNewModule(ScriptEngine::get(), settings.scripts[i].c_str());
			std::string rcode;
			builder.AddSectionFromFile(settings.scripts[i].c_str(), rcode);
			builder.BuildModule();
			mod = builder.GetModule();

			std::istringstream f(rcode);
			std::string newrcode;
			while(!f.eof())
			{
				std::string line;
				std::getline(f, line);
				if(line.find("#include") > line.size()) //add line if no include
				{
					newrcode += line + "\n";

				}

			}

			ScriptEngine::getInstance()->modscript.push_back(std::pair<std::string, std::string>(settings.scripts[i], newrcode));

		}
		else
		{
			mod = ScriptEngine::get()->GetModule(settings.scripts[i].c_str());

		}

		if(mod->GetFunctionByDecl("void onInit(Entity@)") != NULL)
			onInitCalls.push_back(mod->GetFunctionByDecl("void onInit(Entity@)"));

		if(mod->GetFunctionByDecl("void onTick(Entity@)") != NULL)
			onTickCalls.push_back(mod->GetFunctionByDecl("void onTick(Entity@)"));

		if(mod->GetFunctionByDecl("void onDraw(Entity@)") != NULL)
			onDrawCalls.push_back(mod->GetFunctionByDecl("void onDraw(Entity@)"));

		if(mod->GetFunctionByDecl("void onDeath(Entity@)") != NULL)
			onDeathCalls.push_back(mod->GetFunctionByDecl("void onDeath(Entity@)"));

		if(mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)") != NULL)
			onCollideCalls.push_back(mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)"));

		if(mod->GetFunctionByDecl("void onEndCollide(Entity@, Entity@)") != NULL)
			onEndCollideCalls.push_back(mod->GetFunctionByDecl("void onEndCollide(Entity@, Entity@)"));

		if(mod->GetFunctionByDecl("bool doesCollide(Entity@, Entity@)") != NULL)
			doesCollideCalls.push_back(mod->GetFunctionByDecl("bool doesCollide(Entity@, Entity@)"));

	}

	//std::cout << "oninit: " << onInitCalls.size() << "\n";

}

Config& EntityType::getConfig()
{
	return cfg;

}

std::string EntityType::getPath()
{
	return cfg.getPath();

}

std::string EntityType::getName()
{
	return settings.name;

}
