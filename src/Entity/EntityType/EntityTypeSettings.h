#ifndef ENTITYTYPESETTINGS_H_
#define ENTITYTYPESETTINGS_H_

#include <Box2D/Box2D.h>
#include "../../Util/Settings/Settings.h"

class EntityTypeSettings : public Settings
{
public:
	EntityTypeSettings();

	std::string name;
	std::vector<std::string> scripts;
	bool isDynamic;
	bool frictionBody;
	float maxForce;
	float maxTorque;
	float health;

	b2BodyDef bdef;

};

#endif /* ENTITYTYPESETTINGS_H_ */
