#include "EntityTypeSettings.h"

EntityTypeSettings::EntityTypeSettings()
{
	name = "";
	isDynamic = false;
	frictionBody = false;
	maxForce = 0.0f;
	maxTorque = 0.0f;
	health = 100.0f;

	add("name", offsetof(EntityTypeSettings, name), name);
	add("scripts", offsetof(EntityTypeSettings, scripts), scripts);
	add("allowSleep", offsetof(EntityTypeSettings, bdef.allowSleep), bdef.allowSleep);
	add("isBullet", offsetof(EntityTypeSettings, bdef.bullet), bdef.bullet);
	add("fixedRotation", offsetof(EntityTypeSettings, bdef.fixedRotation), bdef.fixedRotation);
	add("gravityScale", offsetof(EntityTypeSettings, bdef.gravityScale), bdef.gravityScale);
	add("linearDamping", offsetof(EntityTypeSettings, bdef.linearDamping), bdef.linearDamping);
	add("isDynamic", offsetof(EntityTypeSettings, isDynamic), isDynamic);
	add("angularDamping", offsetof(EntityTypeSettings, bdef.angularDamping), bdef.angularDamping);
	add("frictionBody", offsetof(EntityTypeSettings, frictionBody), frictionBody);
	add("maxForce", offsetof(EntityTypeSettings, maxForce), maxForce);
	add("maxTorque", offsetof(EntityTypeSettings, maxTorque), maxTorque);
	add("health", offsetof(EntityTypeSettings, health), health);

}
