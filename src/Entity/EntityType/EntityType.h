#ifndef ENTITYTYPE_H_
#define ENTITYTYPE_H_

#include <string>
#include <vector>
#include "EntityTypeSettings.h"
#include <list>
#include <Box2D/Box2D.h>
#include "../SpriteLayer.h"
#include <angelscript.h>

class Entity;

class EntityType
{
public:
	EntityType(std::string path, std::string file);

	~EntityType();

	void onInit(Entity* myobj);
	void onTick(Entity* myobj);
	void onDraw(Entity* myobj);
	void onDeath(Entity* myobj);
	void onCollide(Entity* myobj, Entity* o);
	void onEndCollide(Entity* myobj, Entity* o);
	bool doesCollide(Entity* myobj, Entity* o);

	std::string getName();
	std::vector<std::string> getScripts();

	Config& getConfig();
	std::string getPath();
	std::string getFile();

protected:
	void read();
	void build();

private:
	Config cfg;

	EntityTypeSettings settings;
	// Start with the basics, TODO: adding more shapes and fixtures to a body

	std::list<b2FixtureDef> fixtures; //TODO: decide if they need names and need to be stored in entity

	std::vector<std::pair<std::string, SpriteLayer> > layers;

	std::list<asIScriptFunction*> onInitCalls;
	std::list<asIScriptFunction*> onTickCalls;
	std::list<asIScriptFunction*> onDrawCalls;
	std::list<asIScriptFunction*> onDeathCalls;
	std::list<asIScriptFunction*> onCollideCalls;
	std::list<asIScriptFunction*> onEndCollideCalls;
	std::list<asIScriptFunction*> doesCollideCalls;
	friend Entity;

};

#endif /* ENTITYTYPE_H_ */
