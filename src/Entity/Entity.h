#ifndef ENTITY_H_
#define ENTITY_H_

#include <angelscript.h>
#include <vector>
#include <list>
#include <string>

#include "TagManager/TagManager.h"
#include "VarManager/VarManager.h"
#include "ID/ID.h"
#include "TeamEntity.h"

#include <Box2D/Box2D.h>
#include "SpriteLayer.h"
#include "../vec2f.h"
#include "../Util/convert/convertstring.h"
#include "../Player.h"

template<class A, class B>
B* refCast(A* a)
{
    // If the handle already is a null handle, then just return the null handle
    if( !a ) return 0;
    // Now try to dynamically cast the pointer to the wanted type
    B* b = dynamic_cast<B*>(a);
    return b;
}

class EntityType;

class Entity : public TagManager, public VarManager, public ID, public TeamEntity
{
public:
	static void registerEntity(asIScriptEngine* engine);

	//change win to singleton
	Entity(EntityType* et, float x = 0, float y = 0, float a = 0);
	Entity(EntityType* et, std::string id, float x = 0, float y = 0, float a = 0);

	~Entity();

	void kill();

	int getTeam()
	{
		return team;

	}

	void setTeam(int team);

	bool isOnTeam(int team)
	{
		return  getTeam() == team && getTeam() != -1 && team != -1;

	}

	void addTag(std::string tag)
	{
		TagManager::addTag(tag);

	}

	bool hasTag(std::string tag)
	{
		return TagManager::hasTag(tag);
	}

	void remTag(std::string tag)
	{
		TagManager::remTag(tag);

	}

	void syncInt(std::string s)
	{
		int i = getInt(s);
		std::string syncstr = "int^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncint: " << syncstr << "\n";
		tosync += syncstr;
		esyncneeded = true;

	}

	void syncUInt(std::string s)
	{
		//std::cout << "syncuint\n";
		unsigned int i = getUInt(s);
		std::string syncstr = "uint^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncuint: " << syncstr << "\n";
		tosync += syncstr;
		esyncneeded = true;

	}

	void syncFloat(std::string s)
	{
		//std::cout << "syncfloat\n";
		float i = getFloat(s);
		std::string syncstr = "float^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncfloat: " << syncstr << "\n";
		tosync += syncstr;
		esyncneeded = true;

	}

	void syncString(std::string s)
	{
		//std::cout << "syncstring\n";
		std::string i = getString(s);
		std::string syncstr = "string^&"+s+"^&"+i+"^&"; //.. hopefully no one will ever use this in a string character
		//std::cout << "syncstring: " << syncstr << "\n";
		tosync += syncstr;
		esyncneeded = true;

	}

	void syncBool(std::string s)
	{
		//std::cout << "syncbool\n";
		bool i = getBool(s);
		std::string syncstr = "bool^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncbool: " << syncstr << "\n";
		tosync += syncstr;
		esyncneeded = true;

	}

	void setInt(std::string s, int u)
	{
		VarManager::setInt(s, u);

	}

	int getInt(std::string s)
	{
		return VarManager::getInt(s);

	}

	void setUInt(std::string s, unsigned int u)
	{
		VarManager::setUInt(s, u);

	}

	unsigned int getUInt(std::string s)
	{
		return VarManager::getUInt(s);

	}

	void setFloat(std::string s, float u)
	{
		VarManager::setFloat(s, u);

	}

	float getFloat(std::string s)
	{
		return VarManager::getFloat(s);

	}

	void setString(std::string s, std::string u)
	{
		VarManager::setString(s, u);

	}

	std::string getString(std::string s)
	{
		return VarManager::getString(s);

	}

	void setBool(std::string s, bool u)
	{
		VarManager::setBool(s, u);

	}

	bool getBool(std::string s)
	{
		return VarManager::getBool(s);

	}


	void applyForce(vec2f v);
	void applyForce(float x, float y);

	void setVelocity(vec2f v);
	void setVelocity(float x, float y);

	void setAngularVelocity(float a);
	void applyAngularImpulse(float a);
	float getAngularVelocity();

	vec2f getVelocity();

	void setPosition(float x, float y);
	vec2f getPosition();
	sf::Vector2f getPositionSF();

	void setAngle(float a);
	float getAngle();

	void onInit();
	void onTick();
	void updateSprites();
	void onDraw();
	void onDeath();
	void onCollide(Entity* o);
	void onEndCollide(Entity* o);
	bool doesCollide(Entity* o);

	std::string getName();
	Config& getConfig();

	SpriteLayer* getLayer(std::string name);
	SpriteLayer* addLayer(std::string name, std::string file, float x = 0, float y = 0, float ox = 0, float oy = 0, bool mask = true);

	Player* getPlayer();
	void plug(Player* p, bool callsync = false);
	void plugSync(Player* p);
	void unplug();

	std::string getID();

	std::string serialize();
	void unserialize(std::string);
	/*float getHealth();
	void setHealth(float a);

	void kill(); // needs an out side source to kill it but this flags it as dead
	*/

	void addScript(std::string script);

	bool shouldNetSync();
	vec2f getForce();
	void setForce(float x, float y);
	void killIfDead();

	bool esyncneeded;
	std::string tosync;

	Player* myplayer;
	vec2f lastpos;
	float health;
	float lasthealth;
	bool justcreated;
protected:
	EntityType* et;
	b2Body* body; // Important part because you can attach any number of fixtures and shapes to this
	//std::vector<std::pair<std::string, b2Fixture*> > fixtures;
	std::list<std::pair<std::string, SpriteLayer*> > layers; //don't want to invalidate sprite layers
	sf::RenderWindow* win;

	// TODO: Custom sprite layer class with animation ability (maybe local zordering)
	// TODO: figure out how I'm going to do zordering
	std::list<asIScriptFunction*> onInitCalls;
	std::list<asIScriptFunction*> onTickCalls;
	std::list<asIScriptFunction*> onDrawCalls;
	std::list<asIScriptFunction*> onDeathCalls;
	std::list<asIScriptFunction*> onCollideCalls;
	std::list<asIScriptFunction*> onEndCollideCalls;
	std::list<asIScriptFunction*> doesCollideCalls;

	friend EntityType;

};

#endif /* ENTITY_H_ */
