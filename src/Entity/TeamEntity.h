#ifndef TEAMENTITY_H_
#define TEAMENTITY_H_

class TeamEntity
{
public:
	TeamEntity(int team = -1);

	int getTeam();
	void setTeam(int team);

	bool isOnTeam(int team);

	int team;

};

#endif /* TEAMENTITY_H_ */
