#ifndef VAR_H_
#define VAR_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include "VarBase.h"

/**Var class.
 * Implementation of VarBase class handles specific
 * variable types; constructing, deconstructing, setting and getting.
 */
template <class T>
class Var : public VarBase
{
public:
	/**Var value constructor.
	 * @param name the name of the variable.
	 * @param val the value of the variable.
	 */
	Var(std::string name, T val);

	/**Var pointer constructor.
	 * @param name the name of the varaible.
	 * @param pval the pointer value of the variable.
	 */
	Var(std::string name, T* ptr);

	/**Var deconstructor.
	 */
	virtual ~Var();

	/**Sets the value of the variable.
	 * @param val the new value of the variable.
	 */
	void set(T val);

	/**Sets the pointer of the variable.
	 * @param ptr the new pointer of the variable.
	 */
	void set(T* ptr);

	/**Returns the value of the variable.
	 * @return the value of the variable.
	 */
	T get();

	/**Returns the pointer of the variable.
	 * @return the pointer of the variable.
	 */
	T* getPtr();

};

#include "Var.inl"

#endif /* VAR_H_ */
