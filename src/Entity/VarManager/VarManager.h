#ifndef VARMANAGER_H_
#define VARMANAGER_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <list>
#include <string>
#include <stdint.h>

#include "VarBase.h"
#include "Var.h"

/**VarManager class.
 * Stores data by a string identifier.
 */
class VarManager
{
public:
	/**VarManager constructor.
	 */
	VarManager();

	/**VarManager deconstructor.
	 */
	virtual ~VarManager();

	/**Sets the value of the specified int.
	 * @param name the name of the int to set.
	 * @param val the value of the variable.
	 */
	void setInt(std::string name, int32_t val);

	/**Sets the value of the specified uint.
	 * @param name the name of the uint to set.
	 * @param val the value of the variable.
	 */
	void setUInt(std::string name, uint32_t val);

	/**Sets the value of the specified float.
	 * @param name the name of the float to set.
	 * @param val the value of the variable.
	 */
	void setFloat(std::string name, float val);

	/**Sets the value of the specified string.
	 * @param name the name of the string to set.
	 * @param val the value of the variable.
	 */
	void setString(std::string name, std::string val);

	/**Sets the value of the specified bool.
	 * @param name the name of the bool to set.
	 * @param val the value of the variable.
	 */
	void setBool(std::string name, bool val);

	/**Returns the specified int.
	 * @param name the name of the int to return.
	 * @return the int specified by name, if not found 0.
	 */
	int32_t getInt(std::string name);

	/**Returns the specified uint.
	 * @param name the name of the uint to return.
	 * @return the uint specified by name, if not found 0.
	 */
	uint32_t getUInt(std::string name);

	/**Returns the specified float.
	 * @param name the name of the float to return.
	 * @return the float specified by name, if not found 0.
	 */
	float getFloat(std::string name);

	/**Returns the specified string.
	 * @param name the name of the string to return.
	 * @return the string specified by name, if not found "".
	 */
	std::string getString(std::string name);

	/**Returns the specified bool.
	 * @param name the name of the bool to return.
	 * @return the bool specified by name, if not found false.
	 */
	bool getBool(std::string name);

	/**Returns true if the variable exists in this VarManager.
	 * @param name the name of the variable to check for.
	 * @return true if the variable exists, otherwise false.
	 */
	bool exists(std::string name);

private:
	/*Sets Var of type T with a value of type T.
	 * @param name the name of the variable to set.
	 * @param val the value of the variable to set.
	 */
	template <class T>
	void set(std::string name, T val);

	/**Returns a value of a Var of type T.
	 * @param the name of the Var to return the value of.
	 * @param the default value to return if the Var is not found.
	 * @return the value of the Var specified, otherwise returns defaultValue.
	 */
	template <class T>
	T get(std::string name, T defaultValue);

	/**Returns the pointer to the value of the type T.
	 * @param the name of the Var to return the pointer from.
	 * @return the pointer to the data, otherwise null.
	 */
	template <class T>
	T* getPtr(std::string name);

	/**Returns the pointer to the value of Var by name.
	 * @param the name of the Var to get the data from.
	 * @return a void* for the data contained in the Var by name, otherwise null.
	 */
	void* getData(std::string name);

	/**Returns the VarBase by name.
	 * @param the name of the VarBase to return.
	 */
	VarBase* getVar(std::string name);

	std::list<VarBase*> m_vars; ///< A list containing all VarBases stored by the VarManager.

};

#include "VarManager.inl"

#endif /* VARMANAGER_H_ */
