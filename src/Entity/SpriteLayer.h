#ifndef SPRITELAYER_H_
#define SPRITELAYER_H_

#include <SFML/Graphics.hpp>
#include "../Util/Config/Config.h"
#include <angelscript.h>
#include "../vec2f.h"
#include <functional>

static float spriteScale = 3.75;

typedef std::function<void()> drawCallBack;

class SpriteLayer : public sf::RectangleShape
{
public:
	static void registerSpriteLayer(asIScriptEngine* engine);
	// x, y,  originx, originy, texture, mask,
	static void drawSprites();

	SpriteLayer(bool zorder = true);
	SpriteLayer(sf::Vector2f v, bool zorder = true);

	~SpriteLayer();

	void setSize(float x, float y);
	void setTextureRect(unsigned x, unsigned y, unsigned width, unsigned height);

	void setOrigin(float x, float y);
	vec2f getOrigin(){ return sf::RectangleShape::getOrigin();}

	void setPosition(sf::Vector2f v);
	void setPosition(float x, float y);

	void setLocalPosition(sf::Vector2f v);
	void setLocalPosition(float x, float y);

	vec2f getLocalPosition();

	void setRotation(float a);

	void setLocalAngle(float a);
	float getLocalAngle();

	void setAlpha(unsigned int a);
	unsigned int getAlpha();

	void setZ(float z);
	float getZ();

	static void addSprite(SpriteLayer* sprite);

	int team;

private:
	static void findSpot(SpriteLayer* sprite);
	static unsigned int getIndex(SpriteLayer* sprite);
	static void remSprite(SpriteLayer* sprite);
	static bool compare(SpriteLayer*, SpriteLayer*);

	static std::vector<SpriteLayer*> sprites;
	sf::Vector2f localPos;
	float localAngle;
	float z;

};

#endif /* SPRITELAYER_H_ */
