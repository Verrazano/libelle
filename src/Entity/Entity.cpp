#include "Entity.h"
#include <assert.h>
#include <sstream>
#include "../Util/convert/convertstring.h"

#include "../add_on/scriptbuilder/scriptbuilder.h"
#include "../Engine/ResourceManager/ResourceManager.h"
#include "../ScriptEngine/ScriptEngine.h"

#include "../Util/Config/Config.h"
//#include "../Config/box2dConfig.h"

#include "../Util/convert/units.h"
#include "../Util/convert/b2tosf.h"
#include "../Engine/Engine.h"
#include "../Game.h"
#include "EntityType/EntityType.h"

void Entity::setTeam(int team)
{
	this->team = team;

}

void Entity::registerEntity(asIScriptEngine* engine)
{
	assert(engine->RegisterObjectMethod("Entity", "void kill()", asMETHOD(Entity, kill), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "string getName()", asMETHOD(Entity, getName), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "string getConfig()", asMETHOD(Entity, getConfig), asCALL_THISCALL) >= 0);

	//TagEntity
	assert(engine->RegisterObjectMethod("Entity", "bool hasTag(string)", asMETHOD(Entity, hasTag), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void remTag(string)", asMETHOD(Entity, remTag), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void addTag(string)", asMETHOD(Entity, addTag), asCALL_THISCALL) >= 0);

	//VarManager
	assert(engine->RegisterObjectMethod("Entity", "void setInt(string, int)", asMETHOD(Entity, setInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "int getInt(string)", asMETHOD(Entity, getInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void setUInt(string, uint)", asMETHOD(Entity, setUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "uint getUInt(string)", asMETHOD(Entity, getUInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void setFloat(string, float)", asMETHOD(Entity, setFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "float getFloat(string)", asMETHOD(Entity, getFloat), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void setString(string, string)", asMETHOD(Entity, setString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "string getString(string)", asMETHOD(Entity, getString), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void setBool(string, bool)", asMETHOD(Entity, setBool), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "bool getBool(string)", asMETHOD(Entity, getBool), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void syncInt(string)", asMETHOD(Entity, syncInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void syncUInt(string)", asMETHOD(Entity, syncUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void syncFloat(string)", asMETHOD(Entity, syncFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void syncString(string)", asMETHOD(Entity, syncString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void syncBool(string)", asMETHOD(Entity, syncBool), asCALL_THISCALL) >= 0);

	//health
	assert(engine->RegisterObjectProperty("Entity", "float health", asOFFSET(Entity, health)) >= 0);

	//IDEntity
	assert(engine->RegisterObjectMethod("Entity", "string getID()", asMETHOD(Entity, Entity::getID), asCALL_THISCALL) >= 0);

	//TeamEntity
	assert(engine->RegisterObjectMethod("Entity", "int getTeam()", asMETHOD(Entity, getTeam), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void setTeam(int team)", asMETHOD(Entity, setTeam), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "bool isOnTeam(int team)", asMETHOD(Entity, isOnTeam), asCALL_THISCALL) >= 0);

	//Position
	assert(engine->RegisterObjectMethod("Entity", "vec2f getPosition()", asMETHOD(Entity, getPosition), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void setPosition(float, float)", asMETHOD(Entity, setPosition), asCALL_THISCALL) >= 0);

	//Angle
	assert(engine->RegisterObjectMethod("Entity", "float getAngle()", asMETHOD(Entity, getAngle), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void setAngle(float)", asMETHOD(Entity, setAngle), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void setAngularVelocity(float)", asMETHOD(Entity, setAngularVelocity), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void applyAngularImpulse(float)", asMETHOD(Entity, applyAngularImpulse), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "float getAngularVelocity()", asMETHOD(Entity, getAngularVelocity), asCALL_THISCALL) >= 0);

	//force/velocity
	assert(engine->RegisterObjectMethod("Entity", "void applyForce(float, float)", asMETHODPR(Entity, applyForce, (float, float), void), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void setVelocity(float, float)", asMETHODPR(Entity, setVelocity, (float, float), void), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "vec2f getVelocity()", asMETHOD(Entity, getVelocity), asCALL_THISCALL) >= 0);

	//Player stuff
	assert(engine->RegisterObjectMethod("Entity", "Player@ getPlayer()", asMETHOD(Entity, getPlayer), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void plug(Player@)", asMETHOD(Entity, plugSync), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "void unplug()", asMETHOD(Entity, unplug), asCALL_THISCALL) >= 0);

	//Sprite layer stuff
	assert(engine->RegisterObjectMethod("Entity", "SpriteLayer@ getLayer(string)", asMETHOD(Entity, getLayer), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Entity", "SpriteLayer@ addLayer(string, string, float, float, float, float, bool)", asMETHOD(Entity, addLayer), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Entity", "void addScript(string)", asMETHOD(Entity, addScript), asCALL_THISCALL) >= 0);

}

void Entity::killIfDead()
{
	if(isServer())
		if(health <= 0) //simple enough right?
			kill();

}

bool Entity::shouldNetSync()
{

	if(isServer() && body->GetType() == b2_dynamicBody && getPlayer() == NULL) //sync things that aren't owned by a player
	{
		vec2f thispos = getPosition();
		if(lastpos != thispos)
		{
			lastpos = thispos;
			esyncneeded = true;

		}

	}

	if(isServer())
	{
		if(lasthealth != health)
		{
			lasthealth = health;
			Engine::get()->game->healthupdate.push(this);

		}

	}

	return esyncneeded;

}

Entity::Entity(EntityType* et, float x, float y, float a) : TeamEntity(-1)
{
	this->et = et;
	this->win = &Engine::get()->window;

	if(et->settings.frictionBody)
		body = Engine::get()->world->CreateFrictionBody(&et->settings.bdef, et->settings.maxForce, et->settings.maxTorque);
	else
		body = Engine::get()->world->CreateBody(&et->settings.bdef);

	for(std::list<b2FixtureDef>::iterator it = et->fixtures.begin();
			it != et->fixtures.end();
			it++)
	{
		body->CreateFixture(&(*it));

	}


	body->SetUserData(this);

	setPosition(x, y);
	setAngle(a);

	for(unsigned int  i = 0; i < et->layers.size(); i++)
	{
		layers.push_back(std::pair<std::string, SpriteLayer*>(et->layers[i].first, new SpriteLayer(et->layers[i].second)));
		SpriteLayer::addSprite((*layers.rbegin()).second);

	}

	health = et->settings.health;
	lasthealth = health;

	esyncneeded = false;
	lastpos.x = x;
	lastpos.y = y;
	justcreated = true;
	myplayer = NULL;

	onInit();
	//if(isServer())
	//	esyncneeded = true;

}

Entity::Entity(EntityType* et, std::string id, float x, float y, float a) : TeamEntity(-1), ID(id)
{
	this->et = et;
	this->win = &Engine::get()->window;

	if(et->settings.frictionBody)
		body = Engine::get()->world->CreateFrictionBody(&et->settings.bdef, et->settings.maxForce, et->settings.maxTorque);
	else
		body = Engine::get()->world->CreateBody(&et->settings.bdef);

	for(std::list<b2FixtureDef>::iterator it = et->fixtures.begin();
			it != et->fixtures.end();
			it++)
	{
		body->CreateFixture(&(*it));

	}

	body->SetUserData(this);

	setPosition(x, y);
	setAngle(a);

	for(unsigned int  i = 0; i < et->layers.size(); i++)
	{
		layers.push_back(std::pair<std::string, SpriteLayer*>(et->layers[i].first, new SpriteLayer(et->layers[i].second)));
		SpriteLayer::addSprite((*layers.rbegin()).second);

	}

	myplayer = NULL;
	lastpos.x = x;
	lastpos.y = y;
	esyncneeded = false;

	health = et->settings.health;
	lasthealth = health;
	justcreated = true;

	onInit();
	//if(isServer())
	//	esyncneeded = true;

}

Entity::~Entity()
{
	Engine::get()->world->DestroyBody(body);


	while(!layers.empty())
	{
		delete (*layers.begin()).second;
		layers.erase(layers.begin());

	}

	layers.clear();

}

void Entity::kill()
{
	Engine::get()->game->remEntity(this);
	myplayer = NULL;
	//TODO notify engine/server/client

}

void Entity::applyForce(vec2f v)
{
	body->ApplyForceToCenter(toWorld(sf::Vector2f(v.x, v.y)), true);
	esyncneeded = true;

}

void Entity::applyForce(float x, float y)
{
	applyForce(vec2f(x, y));
	esyncneeded = true;

}

void Entity::setVelocity(vec2f v)
{
	body->SetLinearVelocity(b2Vec2(v.x, v.y));
	esyncneeded = true;

}

void Entity::setVelocity(float x, float y)
{
	setVelocity(vec2f(x, y));
	esyncneeded = true;

}

vec2f Entity::getVelocity()
{
	return vec2f(body->GetLinearVelocity().x, body->GetLinearVelocity().y);

}

void Entity::setAngularVelocity(float a)
{
	body->SetAngularVelocity(a);
	esyncneeded = true;

}

void Entity::applyAngularImpulse(float a)
{
	body->ApplyAngularImpulse(a, true);
	esyncneeded = true;

}

float Entity::getAngularVelocity()
{
	return body->GetAngularVelocity();

}

void Entity::setPosition(float x, float y)
{
	body->SetTransform(toWorld(sf::Vector2f(x, y)), body->GetAngle());
	body->SetAwake(true);
	esyncneeded = true;

}

vec2f Entity::getPosition()
{
	vec2f v(toScreen(body->GetPosition()));
	return v;

}

sf::Vector2f Entity::getPositionSF()
{
	return toScreen(body->GetPosition());

}

void Entity::setAngle(float a)
{
	body->SetTransform(body->GetPosition(), toRadians(a));
	esyncneeded = true;

}

float Entity::getAngle()
{
	return toDegrees(body->GetAngle());

}

void Entity::onInit()
{
	et->onInit(this);

}

void Entity::onTick()
{
	et->onTick(this);
	killIfDead();

}

void Entity::onDraw()
{
	et->onDraw(this);

}

void Entity::updateSprites()
{
	std::list<std::pair<std::string, SpriteLayer*> >::iterator it;
	for(it = layers.begin(); it != layers.end(); it++)
	{
		(*it).second->setPosition(getPositionSF());
		(*it).second->setRotation(getAngle());
		(*it).second->team = this->getTeam();

	}

}

void Entity::onDeath()
{
	et->onDeath(this);

}

void Entity::onCollide(Entity* o)
{
	et->onCollide(this, o);

}

void Entity::onEndCollide(Entity* o)
{
	et->onEndCollide(this, o);

}

bool Entity::doesCollide(Entity* o)
{
	return et->doesCollide(this, o);

}

std::string Entity::getName()
{
	return et->getName();

}

Config& Entity::getConfig()
{
	return et->getConfig();

}

SpriteLayer* Entity::getLayer(std::string name)
{
	std::list<std::pair<std::string, SpriteLayer*> >::iterator it;
	for(it = layers.begin(); it != layers.end(); it++)
		if((*it).first == name)
			return (*it).second;

	return NULL;
}

SpriteLayer* Entity::addLayer(std::string name, std::string file, float x, float y, float ox, float oy, bool mask)
{
	sf::Texture* temp = Engine::get()->files.get<sf::Texture>(file);
	SpriteLayer* t = new SpriteLayer(sf::Vector2f(temp->getSize()));
	t->setTexture(temp);
	t->setOrigin(ox, oy);
	t->setLocalPosition(x, y);
	layers.push_back(std::pair<std::string, SpriteLayer*>(name, t));
	return (*layers.rbegin()).second;

}

Player* Entity::getPlayer()
{
	return myplayer;

}

void Entity::plug(Player* p, bool callsync)
{
	this->myplayer = p;
	p->e = this;
	//this->addTag("player");
	if(callsync)
		Engine::get()->game->plugPlayer(p, this);
	//TODO notify engine/server/client

}

void Entity::plugSync(Player* p)
{
	plug(p, true);

}

void Entity::unplug()
{
	if(myplayer != NULL)
	{
		this->remTag("player");
		myplayer->e = NULL;

	}
	myplayer = NULL;

}

std::string Entity::getID()
{
	return ID::getIDString();

}

void Entity::addScript(std::string script)
{
	asIScriptModule* mod;

	if(ScriptEngine::get()->GetModule(script.c_str()) == NULL)
	{
		CScriptBuilder builder;
		builder.StartNewModule(ScriptEngine::get(), script.c_str());
		std::string rcode;
		builder.AddSectionFromFile(script.c_str(), rcode);
		builder.BuildModule();
		mod = builder.GetModule();

		std::istringstream f(rcode);
		std::string newrcode;
		while(!f.eof())
		{
			std::string line;
			std::getline(f, line);
			if(line.find("#include") > line.size()) //add line if no include
			{
				newrcode += line + "\n";

			}

		}

		ScriptEngine::getInstance()->modscript.push_back(std::pair<std::string, std::string>(script, newrcode));

	}
	else
	{
		mod = ScriptEngine::get()->GetModule(script.c_str());

	}

	if(mod->GetFunctionByDecl("void onTick(Entity@)") != NULL)
		onTickCalls.push_back(mod->GetFunctionByDecl("void onTick(Entity@)"));

	if(mod->GetFunctionByDecl("void onDraw(Entity@)") != NULL)
		onDrawCalls.push_back(mod->GetFunctionByDecl("void onDraw(Entity@)"));

	if(mod->GetFunctionByDecl("void onDeath(Entity@)") != NULL)
		onDeathCalls.push_back(mod->GetFunctionByDecl("void onDeath(Entity@)"));

	if(mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)") != NULL)
		onCollideCalls.push_back(mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)"));

	if(mod->GetFunctionByDecl("void onEndCollide(Entity@, Entity@)") != NULL)
		onEndCollideCalls.push_back(mod->GetFunctionByDecl("void onEndCollide(Entity@, Entity@)"));

	if(mod->GetFunctionByDecl("bool doesCollide(Entity@, Entity@)") != NULL)
		doesCollideCalls.push_back(mod->GetFunctionByDecl("bool doesCollide(Entity@, Entity@)"));

	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	asIScriptFunction* onInitC = mod->GetFunctionByDecl("void onInit(Entity@)");
	if(onInitC != NULL)
	{
		if(ctx->Prepare(onInitC) < 0)
			return;
		ctx->SetArgAddress(0, this);
		ctx->Execute();

	}
	ctx->Release();


}

std::string wrapval(std::string type, std::string name, std::string val)
{
	return type + " " + name + " = " + val + ";";

}

std::string Entity::serialize()
{
	/*std::string sync;
	vec2f p = getPosition();
	vec2f v = getVelocity();
	float a = getAngle();
	sync += wrapval("float", "worldangle", toString(a));

	std::string varsync = getSyncString();
	if(varsync != "")
	{
		sync += wrapval("string", "varsync", varsync);

	}*/
	esyncneeded = false;

	//return getSyncString();
	//do sprites later

}

void Entity::unserialize(std::string s)
{
	Config g;
	g.parse(s);

	vec2f p;
	g.getFloat("worldx", p.x);
	g.getFloat("worldy", p.y);
	setPosition(p.x, p.y);

	vec2f v;
	g.getFloat("velx", v.x);
	g.getFloat("vely", v.y);
	setVelocity(v.x, v.y);

	float a = 0;
	g.getFloat("worldangle", a);
	setAngle(a);

	std::string varsync;
	g.getString("varsync", varsync);
	//readSyncString(varsync);
	esyncneeded = false;

}
