#ifndef ID_H_
#define ID_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <string>
#include "uuidlib.h"

/**ID class.
 * Creates and stores a universal unique identifier.
 */
class ID
{
public:
	/**ID constructor.
	 * Generates a random uuid.
	 */
	ID();

	/**ID copy constructor.
	 */
	ID(ID& copy);

	/**ID idString constructor.
	 * copies the uuid of another ID given the idString.
	 */
	ID(std::string idString);

	/**ID deconstructor.
	 */
	virtual ~ID();

	/**ID equivalence operator.
	 */
	bool operator==(ID& rhs);

	/**uuid equivalence operator.
	 */
	bool operator==(uuid_t id);

	/**idString equivalence operator.
	 */
	bool operator==(std::string idString);

	/**ID non-equivalence operator.
	 */
	bool operator!=(ID& rhs);

	/**uuid non-equivalence operator.
	 */
	bool operator!=(uuid_t id);

	/**idString non-equivalence operator.
	 */
	bool operator!=(std::string idString);

	/**Returns the uuid.
	 * @return the uuid.
	 */
	uuid_t getUUID();

	/**Returns the idString.
	 * @return the idString.
	 */
	std::string getIDString();

private:
	uuid_t m_id; ///< The uuid.
	std::string m_idString; ///< The idString. Represents the uuid in a string format.

};

#endif /* ID_H_ */
