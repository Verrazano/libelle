#include "Game.h"
#include <time.h>

Game::Game(LibelleClient* client)
{
	this->client = client;
	registerClient();
	downloaded = false;
	ondownload = false;
	Engine::get()->isServer = false;
	this->win = &Engine::get()->window;
	win->setVisible(true);

	clock.setSampleDepth(100);
	hud = ClockHUD(clock, *Engine::get()->files.get<sf::Font>("consolas.ttf"));
	consoleOpen = false;
	homePressed = false;

	console = Console(win, sf::Vector2f(800, 300), sf::Vector2f(0, 0), 23);

}

Game::Game(LibelleServer* server)
{
	this->server = server;
	registerServer();
	Engine::get()->isServer = true;
	this->win = &Engine::get()->window;
	win->setVisible(false);

}


void Game::onDisconnect(ENetPeer* e)
{
	if(isServer())
	{
		Player* pl = getPlayer(e);
		if(pl != NULL)
		{
			remPlayer(e);

		}

	}
	else
	{
		downloaded = false;
		ondownload = false;
		Engine::get()->entities.clearEntityTypes();
		for(unsigned int i = 0; i < ScriptEngine::getInstance()->modscript.size(); i++)
		{
			ScriptEngine::get()->DiscardModule((ScriptEngine::getInstance()->modscript[i].first).c_str());

		}
		ScriptEngine::getInstance()->modscript.clear();
		clearTileTypes();
		clearMap();

	}

}

void Game::onConnect(ENetPeer* e)
{
	if(isClient())
	{
		client->request("download");
		client->request("tile_types");

	}
	/*if(isClient()) //connected to server
	{
		client->request("download");
		client->send(pack_player_info(sf::Packet(), client->getServerPeer()), 1, true);

	}
	else //player joined server
	{

	}*/

}

void Game::start()
{

	mychan = 0;
	time(&lastsync);
	//Soldier* playerS = libelleType->create(20, 300, "Verrazano");
	if(Engine::get()->isServer)
	{
		server->start();
		loadEntities("res/entities/entities.cfg");
		mode = new GameMode(Engine::get()->settings.gameMode);
		Engine::get()->gmode = mode;
		mode->onInit();
		mode->onReset();

		File map("res/map.cfg", true);
		std::cout << "\n\n\n\nREADINGMAP FILE\n\n\n\n\n";
		for(unsigned int i = 0; i < map.getLength(); i++)
		{
			std::string l =  map.getLine(i);
			//std::cout << "l: " << l << "\n";
			if(l.find("//") < l.size())
				continue;
			handleScript(l);

		}

		while(!server->isRunning()){sf::sleep(sf::seconds(0.01));}

		while(server->isRunning())
		{
			server->run();
			/*std::cout << "> ";
			std::string line;
			std::getline(std::cin, line);
			if(line == "quit" || line == "q" || line == "kill" || line == "k")
			{
				server->stop();

			}
			handleScript(line); run a new thread with this thing*/

			if(mode->getState())
			{
				mode->onReset();

			}

			Engine::get()->step();
			//Engine::get()->particleWorld->step();
			Engine::get()->entities.onTick();
			mode->onTick();
			//Engine::get()->sounds->update();

			//Engine::get()->particles->update();
			Engine::get()->entities.onDraw();

			mode->onDraw();

			if(downloaded && ondownload)
			{
				mode->isGameOver();

			}

			Engine::get()->tickCounter++;

			EntityManager* em = &Engine::get()->entities;
			for(unsigned int i = 0; i < em->getNumOfEntities(); i++)
			{
				Entity* en = em->getEntityByIndex(i);
				if(en->justcreated)
				{
					en->justcreated = false;
					syncJustCreated(en);

				}

			}

			if(mode->tosync != "")
			{
				std::cout << "SYNCING GAMEMODE\n";
				sf::Packet syncgamemode = this->pack_sync_gamemode(sf::Packet(), NULL);
				server->broadcast(syncgamemode, 1, true);

			}

			if(shouldSyncEntities())
			{
				sf::Packet syncentities = this->pack_sync_entities(sf::Packet(), NULL);
				server->broadcast(syncentities, 0, false);

			}

			for(unsigned int i = 0; i < playersCount(); i++)
			{
				Player* p = getPlayerByIndex(i);
				if(p != NULL)
				{
					if(p->tosync != "")
					{
						sf::Packet pack;
						pack << "sync_player";
						pack << p->tosync;
						p->tosync = "";
						server->send(pack, getPeer(p), 1, true);

					}

				}

			}

			if(healthupdate.size() > 0)
			{
				sf::Packet p = pack_sync_health(sf::Packet(), NULL);
				server->broadcast(p, 1, true);

			}

			sf::sleep(sf::seconds(0.01));

		}

	}
	else
	{
		client->start();
		sf::View gui(sf::FloatRect(0, 0, 800, 600));
		loadPercent = 0;
		loadingFile = "";

		loadSprite.setFillColor(sf::Color::Green);
		loadSprite.setOutlineColor(sf::Color::Black);
		loadSprite.setOutlineThickness(3);
		float length = loadPercent*100;
		loadStr.setFont(*Engine::get()->files.get<sf::Font>("consolas.ttf"));
		loadStr.setString("                 " + toString((int)(loadPercent*100)) + "%\nLoading . . . " + loadingFile);
		loadStr.setColor(sf::Color::Black);
		loadStr.setCharacterSize(20);
		loadStr.setPosition(500, 400);
		loadSprite.setSize(sf::Vector2f(length, 50));
		loadSprite.setPosition(625, 500);

		std::vector<sf::Text> loadStrs;

		sf::Text disconnectedText("DISCONNECTED", *Engine::get()->files.get<sf::Font>("consolas.ttf"));
		disconnectedText.setColor(sf::Color::Red);
		disconnectedText.setPosition(300, 300);

		downloaded  = false;
		ondownload = false;

		sf::Text pingtext("Ping:", *Engine::get()->files.get<sf::Font>("consolas.ttf"), 14);
		pingtext.setColor(sf::Color::Red);
		pingtext.setPosition(120, 0);

		while(client->isRunning())
		{
			client->run();
			clock.beginFrame();

			if(downloaded && !ondownload)
			{
				std::cout << "loading all the stuff in thread 1\n";
				Engine::get()->entities.loadEntityTypesFromVec();

				//mode = new GameMode(Engine::get()->gamemodestring, gamemodeconfig);
				mode = new GameMode(Engine::get()->settings.gameMode, Engine::get()->gameModeConfig);
				Engine::get()->gmode = mode;
				mode->onInit();
				mode->onReset();

				client->request("entities_list");
				sf::Packet player_info = pack_player_info(sf::Packet(), NULL);
				client->send(player_info, 1, true);
				loadStrs.clear();
				ondownload = true;

			}

			win->pollEvent(event);
			if(event.type == sf::Event::Closed || (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && Engine::get()->hasFocus))
			{
				std::cout << "CLOSING\n";
				client->stop();//graceful disconnect
				return;

			}

			if(event.type == sf::Event::LostFocus)
				Engine::get()->hasFocus = false;
			else if(event.type == sf::Event::GainedFocus)
				Engine::get()->hasFocus = true;

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Home) && Engine::get()->hasFocus)
			{
				if(!homePressed)
				{
					consoleOpen = !consoleOpen;
					homePressed = true;

				}

			}
			else if(homePressed)
			{
				homePressed = false;

			}

			if(downloaded && ondownload)
			{
				if(mode->getState())
				{
					mode->onReset();

				}

				Engine::get()->step();
				Engine::get()->particleWorld->step();
				Engine::get()->entities.onTick();
				mode->onTick();
				Engine::get()->sounds.update();

			}

			win->clear(sf::Color::White);


			if(downloaded && ondownload)//have fun
			{
				win->setView(Engine::get()->gameview);
				Engine::get()->particles.update();
				Engine::get()->entities.onDraw();

				/*if(Engine::get()->settings.debugLevel >= 1)
					Engine::get()->world->DrawDebugData();*/

				win->setView(gui);
				mode->onDraw();

			}

			win->setView(gui);

			if(consoleOpen)
			{
				if(console.update(event))
					handleScript(console.getInput());
				console.draw();

			}
			else
			{
				if(Engine::get()->settings.debugLevel > 0)
					win->draw(hud);

				if(client->isConnected())
				{
					pingtext.setString("Ping: " + toString(client->getPing()));
					win->draw(pingtext);

				}

			}

			if(!client->isConnected())
			{
				win->draw(disconnectedText);

			}

			if(!downloaded && client->isConnected())
			{
				float length = loadPercent*100;

				if(loadStrs.size() == 0)
				{
					sf::Text temp;
					temp.setFont(*Engine::get()->files.get<sf::Font>("consolas.ttf"));
					temp.setString(loadingFile);
					temp.setColor(sf::Color::Black);
					temp.setCharacterSize(20);
					temp.setPosition(0, 22*loadStrs.size());
					loadStrs.push_back(temp);

				}
				else if(loadingFile != loadStrs.rbegin()->getString().toAnsiString())
				{
					sf::Text temp;
					temp.setFont(*Engine::get()->files.get<sf::Font>("consolas.ttf"));
					temp.setString(loadingFile);
					temp.setColor(sf::Color::Black);
					temp.setCharacterSize(20);
					temp.setPosition(0, 22*loadStrs.size());
					loadStrs.push_back(temp);
				}

				if(loadStrs.size() > 25)
				{
					loadStrs.erase(loadStrs.begin());
					for(unsigned int i = 0; i < loadStrs.size(); i++)
					{
						loadStrs[i].setPosition(0, 22*i);

					}

				}


				loadStr.setString("                 " + toString((int)(loadPercent*100)) + "%\n         Loading . . . ");
				loadSprite.setSize(sf::Vector2f(length, 50));
				win->draw(loadSprite);
				win->draw(loadStr);

				for(unsigned int i = 0; i < loadStrs.size(); i++)
				{
					win->draw(loadStrs[i]);

				}


			}

			win->display();

			if(downloaded && ondownload)
			{
				mode->isGameOver();

			}

			Engine::get()->tickCounter++;

			if(downloaded && ondownload)
			{
				EntityManager* em = &Engine::get()->entities;
				for(unsigned int i = 0; i < em->getNumOfEntities(); i++)
				{
					Entity* en = em->getEntityByIndex(i);
					if(en->justcreated)
					{
						en->justcreated = false;
						syncJustCreated(en);

					}

				}

			}

			if(downloaded && ondownload && getLocalPlayer()->tosync != "")
			{
				sf::Packet syncplayer = this->pack_sync_player(sf::Packet(), NULL);
				client->send(syncplayer, 1, true);

			}

			if(downloaded && ondownload && shouldSyncEntities())
			{
				sf::Packet syncentities = this->pack_sync_entities(sf::Packet(), NULL);
				client->send(syncentities, mychan, false);

			}


			sf::sleep(sf::seconds(0.01));
			clock.endFrame();

		}

		win->close();
		return;

	}
}

