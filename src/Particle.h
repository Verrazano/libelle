#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

class ParticleManager;

class Particle
{
public:
	Particle(float x, float y, std::string texture, float size, bool collide, float velx, float vely, float lifetime);
	Particle(float xp, float yp, std::string texture,
			unsigned int x, unsigned int y, unsigned int width, unsigned int height, //frame part
			float size, bool collide, float velx, float vely, float lifetime);

	void update();
private:
	sf::RectangleShape sprite;
	b2Body* b;

	sf::Clock lifeclock;
	float lifetime;

	friend ParticleManager;

};

class ParticleManager
{
public:
	ParticleManager();
	~ParticleManager();

	void createParticle(Particle* p);
	void deleteParticle(Particle* p);

	void update();
private:
	std::vector<Particle*> particles;

};

#endif /* PARTICLE_H_ */
