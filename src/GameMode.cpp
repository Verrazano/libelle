#include "GameMode.h"
#include <iostream>
#include <sstream>
#include "add_on/scripthelper/scripthelper.h"
#include "add_on/scriptbuilder/scriptbuilder.h"

void GameMode::registerGameMode(asIScriptEngine* engine)
{
	assert(engine->RegisterObjectType("GameMode", sizeof(GameMode), asOBJ_REF | asOBJ_NOCOUNT) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "string getName()", asMETHOD(GameMode, getName), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "string getConfig()", asMETHOD(GameMode, getConfig), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "bool getState()", asMETHOD(GameMode, getState), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "uint getPlayerCount()", asMETHOD(GameMode, getPlayerCount), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "Player@ getPlayer(uint)", asMETHOD(GameMode, getPlayer), asCALL_THISCALL) >= 0);

	//VarManager
	assert(engine->RegisterObjectMethod("GameMode", "void setInt(string, int)", asMETHOD(GameMode, setInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "int getInt(string)", asMETHOD(GameMode, getInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "void setUInt(string, uint)", asMETHOD(GameMode, setUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "uint getUInt(string)", asMETHOD(GameMode, getUInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "void setFloat(string, float)", asMETHOD(GameMode, setFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "float getFloat(string)", asMETHOD(GameMode, getFloat), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "void setString(string, string)", asMETHOD(GameMode, setString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "string getString(string)", asMETHOD(GameMode, getString), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "void setBool(string, bool)", asMETHOD(GameMode, setBool), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "bool getBool(string)", asMETHOD(GameMode, getBool), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("GameMode", "void syncInt(string)", asMETHOD(GameMode, syncInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "void syncUInt(string)", asMETHOD(GameMode, syncUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "void syncFloat(string)", asMETHOD(GameMode, syncFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "void syncString(string)", asMETHOD(GameMode, syncString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("GameMode", "void syncBool(string)", asMETHOD(GameMode, syncBool), asCALL_THISCALL) >= 0);

}

GameMode::GameMode(std::string config)
{
	this->config = config;
	configasstring = Config::load(config);
	read();
	build();
	gameOverBool = false;
	players.push_back(Engine::get()->getLocalPlayer());

}

GameMode::GameMode(std::string config, std::string file)
{
	this->config = config;
	this->configasstring = file;
	read(configasstring);
	build();
	gameOverBool = false;
	players.push_back(Engine::get()->getLocalPlayer());

}

void GameMode::onInit()
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext(); //TODO figure out if making new ctx on each call eats to much cpu if so just allocate a bunch on class creation
	if(onInitCalls.size() != 0)
		for(it = onInitCalls.begin(); it != onInitCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();
}

void GameMode::onTick()
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onTickCalls.size() != 0)
		for(it = onTickCalls.begin(); it != onTickCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();

}

void GameMode::onDraw()
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onDrawCalls.size() != 0)
		for(it = onDrawCalls.begin(); it != onDrawCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();

}

void GameMode::isGameOver()
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(isGameOverCalls.size() != 0)
	for(it = isGameOverCalls.begin(); it != isGameOverCalls.end(); it++)
	{
		if(ctx->Prepare((*it)) < 0)
			continue;
		ctx->SetArgAddress(0, this);
		ctx->Execute();
		PrintException(ctx);

		bool r = (bool)ctx->GetReturnByte();
		if(r)
		{
			gameOverBool = true;
			return;

		}

	}
	ctx->Release();

}

void GameMode::onReset()
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onResetCalls.size() != 0)
		for(it = onResetCalls.begin(); it != onResetCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();
	gameOverBool = false;

}

void GameMode::onEntityDie(Entity* o)
{
	if(o == Engine::get()->localPlayer->e)
	{
		Engine::get()->localPlayer->e = NULL;

	}

	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onEntityDieCalls.size() != 0)
		for(it = onEntityDieCalls.begin(); it != onEntityDieCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->SetArgAddress(1, o);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();

}

std::string GameMode::getConfig()
{
	return config;

}

std::string GameMode::getName()
{
	return settings.name;

}

bool GameMode::getState()
{
	return gameOverBool;

}

unsigned int GameMode::getPlayerCount()
{
	return players.size();

}

Player* GameMode::getPlayer(unsigned int i)
{
	if(i > players.size())
		return NULL;
	return players[i];

}

void GameMode::onPlayerJoin(Player* p)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onPlayerJoinCalls.size() != 0)
		for(it = onPlayerJoinCalls.begin(); it != onPlayerJoinCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0) //contexts are colliding
				continue;
			ctx->SetArgAddress(0, this);
			ctx->SetArgAddress(1, p);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();

}

void GameMode::onPlayerLeave(Player* p)
{
	std::list<asIScriptFunction*>::iterator it;
	asIScriptContext* ctx = Engine::get()->script->CreateContext();
	if(onPlayerLeaveCalls.size() != 0)
		for(it = onPlayerLeaveCalls.begin(); it != onPlayerLeaveCalls.end(); it++)
		{
			if(ctx->Prepare((*it)) < 0)
				continue;
			ctx->SetArgAddress(0, this);
			ctx->SetArgAddress(1, p);
			ctx->Execute();
			PrintException(ctx);

		}
	ctx->Release();

}

void GameMode::read()
{
	settings.read(configasstring);
	for(unsigned int i = 0; i < settings.scripts.size(); i++)
		std::cout << "rule script: " << settings.scripts[i] << "\n";

}

void GameMode::read(std::string file)
{
	configasstring = file;
	read();

}

void GameMode::build()
{
	//std::cout << "scripts: ";
	for(unsigned int i = 0; i < settings.scripts.size(); i++)
	{
		//std::cout << scripts[i] << ", ";
		asIScriptModule* mod;

		if(ScriptEngine::get()->GetModule(settings.scripts[i].c_str()) == NULL)
		{
			CScriptBuilder builder;
			builder.StartNewModule(ScriptEngine::get(), settings.scripts[i].c_str());
			std::string rcode;
			builder.AddSectionFromFile(settings.scripts[i].c_str(), rcode);
			builder.BuildModule();
			mod = builder.GetModule();

			std::istringstream f(rcode);
			std::string newrcode;
			while(!f.eof())
			{
				std::string line;
				std::getline(f, line);
				if(line.find("#include") > line.size()) //add line if no include
				{
					newrcode += line + "\n";

				}

			}


			ScriptEngine::getInstance()->modscript.push_back(std::pair<std::string, std::string>(settings.scripts[i], newrcode));

		}
		else
		{
			mod = ScriptEngine::get()->GetModule(settings.scripts[i].c_str());

		}

		if(mod->GetFunctionByDecl("void onInit(GameMode@)") != NULL)
			onInitCalls.push_back(mod->GetFunctionByDecl("void onInit(GameMode@)"));

		if(mod->GetFunctionByDecl("void onTick(GameMode@)") != NULL)
			onTickCalls.push_back(mod->GetFunctionByDecl("void onTick(GameMode@)"));

		if(mod->GetFunctionByDecl("void onDraw(GameMode@)") != NULL)
			onDrawCalls.push_back(mod->GetFunctionByDecl("void onDraw(GameMode@)"));

		if(mod->GetFunctionByDecl("bool isGameOver(GameMode@)") != NULL)
			isGameOverCalls.push_back(mod->GetFunctionByDecl("bool isGameOver(GameMode@)"));

		if(mod->GetFunctionByDecl("void onReset(GameMode@)") != NULL)
			onResetCalls.push_back(mod->GetFunctionByDecl("void onReset(GameMode@)"));

		if(mod->GetFunctionByDecl("void onEntityDie(GameMode@, Entity@)") != NULL)
			onEntityDieCalls.push_back(mod->GetFunctionByDecl("void onEntityDie(GameMode@, Entity@)"));

		if(mod->GetFunctionByDecl("void onPlayerJoin(GameMode@, Player@)") != NULL)
			onPlayerJoinCalls.push_back(mod->GetFunctionByDecl("void onPlayerJoin(GameMode@, Player@)"));

		if(mod->GetFunctionByDecl("void onPlayerLeave(GameMode@, Player@)") != NULL)
			onPlayerLeaveCalls.push_back(mod->GetFunctionByDecl("void onPlayerLeave(GameMode@, Player@)"));

	}

}

