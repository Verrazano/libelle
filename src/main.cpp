#ifndef MAIN_CPP_
#define MAIN_CPP_

#include "libelle.h"

#ifdef NOLAUNCHER_AVAILABLE

    int main(int argc, char* argv[])
    {
        return libelle(argc, argv);

    }

#else

    #include <string>

    #ifdef EXPORT
    #define LIBELLE __declspec(dllexport)
    #else
    #define LIBELLE __declspec(dllimport)
    #endif

    const unsigned int BUILD_NUM = 1;
    const char* DEV_STAGE = "alpha";

    extern "C"
    {
        LIBELLE int LIBELLE_MAIN(int argc, char* argv[]){ return libelle(argc, argv); }

        LIBELLE unsigned int LIBELLE_BUILD_NUM(){ return BUILD_NUM; }

        LIBELLE char* LIBELLE_DEV_STAGE(){ return (char*)DEV_STAGE; }

    }

#endif

#endif /* MAIN_CPP_ */
