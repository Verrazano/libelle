#include <SFML/Graphics.hpp>
#include "File/File.h"
#include "Util/bslib/bslib.h"
#include "Game.h"
#include "Engine/Log/Log.h"
#include "Engine/ResourceManager/ResourceManager.h"
#include <time.h>
#include "Util/convert/units.h"
#include <math.h>
#include "primitiveGui.h"
#include <enet/enet.h>

// TODO:
//		- Fix fullscreen crashing.

//25%cpu is unacceptable I want it to be 10 or below especially with very few entities and scripts running
int playerLogin(std::string name);
bool isValidPlayerLogin(std::string name, std::string password);
int serverBrowser(std::string& ip, unsigned int& port);
void startServer(std::string ip, unsigned int port);
void startClient(std::string ip, unsigned int port);

void libelle(int argc, char* argv[])
{
	Engine::get();

	sf::Image logoIMG = Engine::get()->files.get<sf::Texture>("libelle.png")->copyToImage();

	sf::RenderWindow splash(sf::VideoMode(800, 150), "", sf::Style::None);
	splash.setIcon(logoIMG.getSize().x, logoIMG.getSize().y, logoIMG.getPixelsPtr());

	sf::RectangleShape logo(sf::Vector2f(logoIMG.getSize()));
	logo.setTexture(Engine::get()->files.get<sf::Texture>("libelle.png"));
	logo.setPosition(328, 3);

	sf::Text loadingText("Loading . . .", *Engine::get()->files.get<sf::Font>("consolas.ttf"), 20); //update this when loading the engine
	loadingText.setPosition(10, 70);

	splash.clear(sf::Color(80, 80, 80));
	splash.draw(logo);
	splash.draw(loadingText);
	splash.display();

	// or load stuff resoruces out side of the engine initalize . . . sounds reasonable

	sf::sleep(sf::seconds(0));

	splash.close();

	std::cout << "ARGC: " << argc;
	for(unsigned int i = 0; i < argc; i++)
		std::cout << " ARGV["<<i<<"] " << argv[i];
	std::cout << "\n";

	enet_initialize();

	if(argc > 1)
	{
		if(std::string(argv[1]) == "host")
		{
			startServer(Engine::get()->settings.ip, Engine::get()->settings.port);
			return;

		}
		else if(std::string(argv[1]) == "local")
		{
			startClient(Engine::get()->settings.ip, Engine::get()->settings.port);
			return;

		}

	}


	pFont::Instance()->loadFromFile("res/gui/fonts/consolas.ttf");

	int loginReturn = 2;
	while(loginReturn != 0)
	{
		loginReturn = playerLogin(Engine::get()->localPlayer->name);
		if(loginReturn == -1 || loginReturn == 1)
			return;

		std::cout << "code: " << loginReturn << "\n";

	}

	std::string ip = Engine::get()->settings.ip;
	unsigned int port = Engine::get()->settings.port;

	int browserReturn = serverBrowser(ip, port);
	if(browserReturn == -1 || browserReturn == 1)
		return;

	File settings("settings.cfg", true);
	for(unsigned int i = 0; i < settings.getLength(); i++)
	{
		if(settings.getLine(i).find("ip") < settings.getLine(i).size())
			settings.setLine("string ip = " + ip + ";", i);

		if(settings.getLine(i).find("port") < settings.getLine(i).size())
			settings.setLine("uint port = " + toString(port) + ";", i);

		if(settings.getLine(i).find("player_name") < settings.getLine(i).size())
			settings.setLine("string player_name = " + Engine::get()->localPlayer->name + ";", i);

	}
	settings.write();

	if(browserReturn == 2)
	{
		startServer(ip, port);
	}
	else
		startClient(ip, port);

	enet_deinitialize();

}

//-1 quit application, 1 return to previous menu, 0 successful login, 2 unsuccessful login
int playerLogin(std::string name)
{
	sf::RenderWindow* win = &Engine::get()->window;
	win->setVisible(true);
	sf::View guiView(sf::FloatRect(0, 0, win->getSize().x, win->getSize().y));
	win->setView(guiView);

	pGui gui;

	pWindow LoginWin(100, 50, 300, 500);
	LoginWin.setWindowTitle("Player Login");

	pTextEdit playerName(10, 40, 280, 25);
	playerName.setText(name);
	playerName.acceptSpace = false;

	pTextEdit playerPassword(10, 80, 280, 25);

	pButton loginButton(75, 200, 150, 40);
	loginButton.setText("Login");

	pButton offlinePlay(75, 250, 150, 40);
	offlinePlay.setText("Play Offline");

	pButton quitButton(75, 300, 150, 40);
	quitButton.setText("Quit");

	LoginWin.addWidget(&playerName);
	LoginWin.addWidget(&playerPassword);
	LoginWin.addWidget(&loginButton);
	LoginWin.addWidget(&offlinePlay);
	LoginWin.addWidget(&quitButton);

	gui.addWindow(&LoginWin);

	sf::Event event;
	std::string password;

	while(win->isOpen())
	{
		if(win->pollEvent(event))
		{
			gui.update(event, *win);

			if(event.type == sf::Event::Closed || quitButton.widgetEvent == pWidget::WidgetClicked)
			{
				win->close();
				return -1;

			}

			if(loginButton.widgetEvent == pWidget::WidgetClicked/* || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)*/)
			{
				name  = playerName.getText();

				if(isValidPlayerLogin(name, password))
				{
					Engine::get()->localPlayer->name = name;
					return 0;

				}
				else
				{
					return 2;

				}

			}

			password = playerPassword.getText();
			std::string temp;
			while(temp.size() < password.size())
				temp.insert(temp.end(), '*');

			playerPassword.setText(temp);

		}

		win->clear(sf::Color(90, 90, 90));
		//win.draw(jins);
		win->draw(gui);
		win->display();

	}
	return 1;

}

bool isValidPlayerLogin(std::string name, std::string password)
{
	return name != "jshreder" && name != "";

}

//-1 quit application, 1 return to previous menu, 0 join server, 2 for host
int serverBrowser( std::string& ip, unsigned int& port)
{
	sf::RenderWindow* win = &Engine::get()->window;
	win->setVisible(true);
	sf::View guiView(sf::FloatRect(0, 0, win->getSize().x, win->getSize().y));
	win->setView(guiView);

	pGui gui;

	pWindow browserWin(100, 50, 300, 500);
	browserWin.setWindowTitle("Server Browser");

	pTextEdit serverIp(10, 40, 280, 25);
	serverIp.setText(ip);

	pTextEdit serverPort(10, 80, 280, 25);
	serverPort.setText(toString(port));

	pButton joinServer(100, 120, 100, 40);
	joinServer.setText("Join Server");

	pButton hostServer(100, 180, 100, 40);
	hostServer.setText("Host Server");

	pButton backButton(100, 240, 100, 40);
	backButton.setText("Back");

	browserWin.addWidget(&serverIp);
	browserWin.addWidget(&serverPort);
	browserWin.addWidget(&joinServer);
	browserWin.addWidget(&hostServer);
	browserWin.addWidget(&backButton);

	gui.addWindow(&browserWin);

	sf::Event event;
	while(win->isOpen())
	{
		if(win->pollEvent(event))
		{
			gui.update(event, *win);

			if(event.type == sf::Event::Closed)
			{
				win->close();
				return -1;

			}

			if(joinServer.widgetEvent == pWidget::WidgetClicked)
			{
				ip = serverIp.getText();
				std::stringstream ss;
				ss << serverPort.getText().toAnsiString();
				ss >> port;

				return 0;

			}

			if(hostServer.widgetEvent == pWidget::WidgetClicked)
			{
				ip = "localhost";
				std::stringstream ss;
				ss << serverPort.getText().toAnsiString();
				ss >> port;

				return 2;

			}

			if(backButton.widgetEvent == pWidget::WidgetClicked)
			{
				return 1;

			}

		}

		win->clear(sf::Color(90, 90, 90));
		//win.draw(jins);
		win->draw(gui);
		win->display();

	}
	return 1;

}

void startServer(std::string ip, unsigned int port)
{

	Game game(new LibelleServer(port, 32, "LibelleServer", "it's a server..."));
	Engine::get()->game = &game;
	game.start();

}

void startClient(std::string ip, unsigned int port)
{
	Game game(new LibelleClient(port, ip));
	Engine::get()->game = &game;
	game.start();

}
