#ifndef GAMEMODESETTINGS_H_
#define GAMEMODESETTINGS_H_

#include "Util/Settings/Settings.h"

class GameModeSettings : public Settings
{
public:
	GameModeSettings();

	std::string name;
	std::vector<std::string> scripts;

};

#endif /* GAMEMODESETTINGS_H_ */
