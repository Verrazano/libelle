#include "Game.h"


#include "World/TopDownWorld.h"
#include "Box2DDebugDrawer/Box2DDebugDrawer.h"
#include <SFML/Graphics.hpp>
#include "Console/Console.h"
#include "FrameClock/ClockHUD.h"
#include "Util/bslib/bslib.h"
#include "Engine/Log/Log.h"
#include "Engine/ResourceManager/ResourceManager.h"
#include "Entity/EntityManager/EntityManager.h"
#include "Entity/Entity.h"

#include "Util/Config/Config.h"
#include "Engine/Engine.h"
#include "GameMode.h"
#include "Network/LibelleClient.h"
#include "Network/LibelleServer.h"
#include <iostream>
#include <enet/enet.h>
#include "Util/convert/convertstring.h"

	void Game::unpack_tile_types(sf::Packet p, ENetPeer* e)
	{
		clearTileTypes();
		unsigned int num;
		p >> num;
		for(unsigned int i = 0; i < num; i++)
		{
			std::string name;
			std::string sprite;
			float width, height, z;
			p >> name >> sprite >> width >> height >> z;
			addTileType(name, sprite, width, height, z);

		}
		client->request("map");

	}

	void Game::unpack_map(sf::Packet p, ENetPeer* e)
	{
		clearMap();
		unsigned int num;
		p >> num;
		for(unsigned int i = 0; i < num; i++)
		{
			std::string name;
			float x, y, z;
			p >> name >> x >> y >> z;
			addTile(name, x, y, z);

		}

	}

	sf::Packet Game::pack_tile_types(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "tile_types";
		unsigned int num = Engine::get()->tiletypes.size();
		p << num;
		for(unsigned int i = 0; i < num; i++)
		{
			std::string name;
			std::string sprite;
			float width, height, z;
			TileType* tt = Engine::get()->tiletypes[i];
			name = tt->tilename;
			sprite = tt->tilesprite;
			width = tt->width;
			height = tt->height;
			z = tt->defaultz;
			p << name << sprite << width << height << z;

		}
		return p;


	}

	sf::Packet Game::pack_map(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "map";
		unsigned int num = Engine::get()->map.size();
		p << num;
		for(unsigned int i = 0; i < num; i++)
		{
			std::string name;
			float x, y, z;
			Tile* t = Engine::get()->map[i];
			name = t->tilename;
			x = t->tilesprite.getPosition().x;
			y = t->tilesprite.getPosition().y;
			z = t->tilesprite.getZ();

			p << name << x << y << z;

		}
		return p;

	}

	bool Game::shouldSyncEntities()
	{
		EntityManager* em = &Engine::get()->entities;
		for(unsigned int i = 0; i < em->getNumOfEntities(); i++)
		{
			Entity* en = em->getEntityByIndex(i);
			if(en->shouldNetSync())
			{
				return true;

			}

		}
		return false;

	}


	void Game::unpackMemberClient(std::string requestName, void(Game::*member)(sf::Packet, ENetPeer*))
	{
		client->addUnpacker(requestName, [this, member](sf::Packet p, ENetPeer* e){(this->*member)(p, e);});

	}

	void Game::packMemberClient(std::string requestName, sf::Packet(Game::*member)(sf::Packet, ENetPeer*))
	{
		client->addPacker(requestName, [this, member](sf::Packet p, ENetPeer* e){return ((this->*member)(p, e));});

	}

	void Game::unpackMemberServer(std::string requestName, void(Game::*member)(sf::Packet, ENetPeer*))
	{
		server->addUnpacker(requestName, [this, member](sf::Packet p, ENetPeer* e){(this->*member)(p, e);});

	}

	void Game::packMemberServer(std::string requestName, sf::Packet(Game::*member)(sf::Packet, ENetPeer*))
	{
		server->addPacker(requestName, [this, member](sf::Packet p, ENetPeer* e){return ((this->*member)(p, e));});

	}


	void Game::unpack_create_entity_client(sf::Packet p, ENetPeer* e)
	{
		if(downloaded && ondownload)
		{
			std::string id;
			std::string name;
			float x, y;
			p >> id >> name >> x >> y;
			Entity* en = Engine::get()->createEntity(name, id, x, y);
			en->esyncneeded = false;
			en->justcreated = false;

		}

	}

	void Game::unpack_create_entity(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		std::string name;
		float x, y;
		p >> id >> name >> x >> y;
		Entity* en = Engine::get()->createEntity(name, id, x, y);
		en->esyncneeded = false;
		en->justcreated = false;
		p.clear();
		p << "create_entity" << id << name << x << y;
		server->broadcastExcluding(p, e, 1, true);

	}

	void Game::SynccreateEntity(Entity* e)
	{
		if(!((isServer() && server->isRunning()) || (isClient() && client->isRunning())))
				return;

		sf::Packet p;
		p << "create_entity";
		p << e->getID();
		p << e->getName();
		p << e->getPosition().x;
		p << e->getPosition().y;
		if(isServer())
		{
			server->broadcast(p, 1, true);

		}
		else
		{
			client->send(p, 1, true);

		}

	}

	void Game::remEntity(Entity* e)
	{
		if(!((isServer() && server->isRunning()) || (isClient() && client->isRunning())))
				return;

		sf::Packet p;
		p << "rem_entity";
		p << e->getID();
		if(isServer())
		{
		    std::cout << "server removing entity: " << e << "\n";
			server->broadcast(p, 1, true);

		}
		else
		{
		    std::cout << "client removing entity: " << e << "\n";
			client->send(p, 1, true);

		}

		mode->onEntityDie(e);
		Engine::get()->entities.remEntity(e);

	}

	void Game::plugPlayer(Player* pl, Entity* e)
	{
		if(!((isServer() && server->isRunning()) || (isClient() && client->isRunning())))
				return;

		if(isServer())
		{
			//don't call plug because plug is what triggers this
			ENetPeer* peer = getPeer(pl);
			sf::Packet p;
			p << "plug";
			p << pl->name << e->getID();
			server->broadcastExcluding(p, peer, 1, true);
			p.clear();
			p << "pluginto" << e->getID();
			server->send(p, peer, 1, true);

		}
		else
		{

			//pl better be local player
			sf::Packet p;
			p << "plug" << e->getID();
			client->send(p, 1, true);

		}

	}
	void Game::unplugPlayer(Entity* e)
	{
		if(!((isServer() && server->isRunning()) || (isClient() && client->isRunning())))
				return;

		if(isServer())
		{
			sf::Packet p;
			p << "unplug" << e->getID();
			server->broadcast(p, 1, true);

		}
		else
		{
			sf::Packet p;
			p << "unplug" << e->getID();
			client->send(p, 1, true);

		}

	}

	unsigned int Game::playersCount(){return players.size();}
	Player* Game::getPlayerByIndex(unsigned int i){if(i > playersCount()){return NULL;} return players[i].second.first; }

	void Game::unpack_sync_entities(sf::Packet p, ENetPeer* e)
	{

		sf::Packet p2; //creating packet for server
		p2.clear(); //just to make sure
		p2 << "sync_entities";

		unsigned int num = 0; //just so we don't inf loop and crash client thread

		p >> num;
		p2 << num;

		for(unsigned int i = 0; i < num; i++)
		{
			std::string id;
			float x, y;
			float velx, vely;
			float a, vela;
			std::string sync;
			int team;
			p >> id >> x >> y >> velx >> vely >> a >> vela >> sync >> team;
			p2 << id << x << y << velx << vely << a << vela << sync << team;

			Entity* en = getEntityByID(id);
			if(en != NULL && !en->justcreated)
			{
				en->setPosition(x, y);
				en->setVelocity(velx, vely);
				en->setAngularVelocity(vela);
				en->setAngle(a);

				while(sync.find("^&")+2 < sync.size())
				{
					std::string type = sync.substr(0, sync.find("^&"));
					sync = sync.substr(sync.find("^&")+2);
					std::string name = sync.substr(0, sync.find("^&"));
					sync = sync.substr(sync.find("^&")+2);
					std::string val = sync.substr(0, sync.find("^&"));
					sync = sync.substr(sync.find("^&")+2);

					if(type == "int")
						en->setInt(name, toInt(val));
					else if(type == "uint")
						en->setUInt(name, toUInt(val));
					else if(type == "float")
						en->setFloat(name, toFloat(val));
					else if(type == "string")
						en->setString(name, val);
					else if(type == "bool")
						en->setBool(name, toBool(val));

				}

				en->shouldNetSync();
				en->lastpos = en->getPosition();
				en->esyncneeded = false; //just incase you didn't already know

			}

		}

		if(isServer())
		{
			server->broadcastExcluding(p2, e, 0, false);

		}

	}
	void Game::unpack_rem_entity_client(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		p >> id;
		Entity* en = getEntityByID(id);
		if(en == NULL)
		{
			return;

		}
		std::cout << "removing entity: " << en << "\n";
		Engine::get()->entities.remEntity(en);

	}
	void Game::unpack_plug_client(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		std::string plugname;
		p >> plugname >> id; //TODO do some other stuff with this stuff
		Entity* en = getEntityByID(id);
		if(en == NULL)
		{
			return;

		}
		en->plug(new Player(plugname)); //TODO not this
		//en->plug(getLocalPlayer());

	}
	void Game::unpack_pluginto(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		p >> id;
		Entity* en = getEntityByID(id);
		if(en == NULL)
		{
			return;

		}
		en->plug(getLocalPlayer());


	}
	sf::Packet Game::pack_sync_player_entity(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "sync_player_entity";
		p << getLocalPlayer()->e->serialize();
		return p;

	}
	void Game::unpack_sync_gamemode(sf::Packet p, ENetPeer* e)
	{
		if(!downloaded || !ondownload)
			return;

		std::string sync;
		p >> sync;
		if(mode != NULL)
		{
			while(sync.find("^&")+2 < sync.size())
			{
				std::string type = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string name = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string val = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);

				if(type == "int")
					mode->setInt(name, toInt(val));
				else if(type == "uint")
					mode->setUInt(name, toUInt(val));
				else if(type == "float")
					mode->setFloat(name, toFloat(val));
				else if(type == "string")
					mode->setString(name, val);
				else if(type == "bool")
					mode->setBool(name, toBool(val));

			}


		}

	}
	sf::Packet Game::pack_sync_player(sf::Packet p, ENetPeer* e)
	{

		p.clear();
		p << "sync_player";
		p << getLocalPlayer()->tosync;
		getLocalPlayer()->tosync = "";
		return p;

	}
	void Game::unpack_unplug_client(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		p >> id;
		Engine::get()->entities.getEntityByID(id)->unplug();

	}

	//SERVER
	sf::Packet Game::pack_sync_entities(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "sync_entities";

		//gathers the data needed for the packet
		std::vector<std::string> syncvec;
		EntityManager* em = &Engine::get()->entities;
		for(unsigned int i = 0; i < em->getNumOfEntities(); i++)
		{
			Entity* en = em->getEntityByIndex(i);
			if(en->shouldNetSync())
			{
				syncvec.push_back(en->getID());
				en->esyncneeded = false;

			}

		}

		//puts together the actual packet
		p << (int)syncvec.size();
		for(unsigned int i = 0; i < syncvec.size(); i++)
		{
			std::string id = syncvec[i];
			Entity* e = getEntityByID(id);
			p << id << e->getPosition().x << e->getPosition().y << e->getVelocity().x << e->getVelocity().y << e->getAngle() << e->getAngularVelocity();
			p << e->tosync << e->getTeam();
			e->tosync = "";

			e->esyncneeded = false;
		}

		return p;

	}

	void Game::unpack_rem_entity(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		p >> id;
		Entity* en = getEntityByID(id);
		if(en == NULL)
		{
		    return;

		}
		mode->onEntityDie(en);
		Engine::get()->entities.remEntity(en);
		p.clear();
		p << "rem_entity" << id;
		server->broadcastExcluding(p, e, 1, true);

	}
	void Game::unpack_plug(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		p >> id;
		Entity* en = getEntityByID(id);
		Player* pl = getPlayer(e);
		en->plug(pl);
		p.clear();
		p << "plug" << pl->name << id;
		server->broadcastExcluding(p, e, 1, true);

	}
	void Game::unpack_sync_player_entity(sf::Packet p, ENetPeer* e)
	{
		Player* pl = getPlayer(e);
		Entity* en = pl->e;
		std::string sync;
		p >> sync;
		while(sync.find("^&")+2 < sync.size())
		{
			std::string type = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);
			std::string name = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);
			std::string val = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);

			if(type == "int")
				en->setInt(name, toInt(val));
			else if(type == "uint")
				en->setUInt(name, toUInt(val));
			else if(type == "float")
				en->setFloat(name, toFloat(val));
			else if(type == "string")
				en->setString(name, val);
			else if(type == "bool")
				en->setBool(name, toBool(val));

		}

		en->esyncneeded = false;

	}
	sf::Packet Game::pack_sync_gamemode(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "sync_gamemode";
		p << mode->tosync;
		mode->tosync = "";
		return p;

	}
	void Game::unpack_sync_player(sf::Packet p, ENetPeer* e)
	{
		std::string sync;
		p >> sync;
		if(isServer())
		{
			Player* pl = getPlayer(e);
			while(sync.find("^&")+2 < sync.size())
			{
				std::string type = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string name = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string val = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);

				if(type == "int")
					pl->setInt(name, toInt(val));
				else if(type == "uint")
					pl->setUInt(name, toUInt(val));
				else if(type == "float")
					pl->setFloat(name, toFloat(val));
				else if(type == "string")
					pl->setString(name, val);
				else if(type == "bool")
					pl->setBool(name, toBool(val));

			}

		}
		else
		{
			Player* pl = getLocalPlayer();
			while(sync.find("^&")+2 < sync.size())
			{
				std::string type = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string name = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);
				std::string val = sync.substr(0, sync.find("^&"));
				sync = sync.substr(sync.find("^&")+2);

				if(type == "int")
					pl->setInt(name, toInt(val));
				else if(type == "uint")
					pl->setUInt(name, toUInt(val));
				else if(type == "float")
					pl->setFloat(name, toFloat(val));
				else if(type == "string")
					pl->setString(name, val);
				else if(type == "bool")
					pl->setBool(name, toBool(val));

			}


		}


	}
	void Game::unpack_unplug(sf::Packet p, ENetPeer* e)
	{
		unsigned int id;
		p >> id;
		p.clear();
		p << "unplug" << id;
		server->broadcastExcluding(p, e, 1, true);

	}


	ENetPeer* Game::getPeer(Player* p)
	{
		for(unsigned int i = 0; i < players.size(); i++)
		{
			if(players[i].second.first == p)
			{
				return players[i].first;

			}

		}

		return NULL; //please check if this happens

	}
	Player* Game::getPlayer(ENetPeer* e)
	{
		for(unsigned int i = 0; i < players.size(); i++)
		{
			if(players[i].first == e)
			{
				return players[i].second.first;

			}
		}

		return NULL;

	}


void Game::unpack_download(sf::Packet p, ENetPeer* e)
{
	if(downloaded)
		return;

	unsigned int numofscripts;
	unsigned int numofconfigs;
	std::vector<std::pair<std::string, std::string> > scripts;
	std::vector<std::pair<std::string, std::string> > configs;
	std::string gamemode;

	p >> numofscripts;
	loadTotal = (float)numofscripts;
	std::string pathsoccer;
	std::string filesoccer;
	for(unsigned int i = 0; i < numofscripts; i++)
	{
		std::string path;
		std::string file;
		p >> path;
		p >> file;
		loadPercent = (float)i/(float)loadTotal;
		loadingFile = path;
		if(path.find("weaponUser.as") < path.size())
		{
			filesoccer = file;
			pathsoccer = path;

		}
		scripts.push_back(std::pair<std::string, std::string>(path, file));

	}

	p >> numofconfigs;
	loadTotal = (float)numofconfigs;
	for(unsigned int i = 0; i < numofconfigs; i++)
	{
		std::string path;
		std::string file;
		p >> path;
		p >> file;
		loadPercent = (float)i/(float)loadTotal;
		loadingFile = path;
		configs.push_back(std::pair<std::string, std::string>(path, file));

	}

	p >> gamemode;
	p >> gameModeConfig;

	for(unsigned int i = 0; i < scripts.size(); i++)
	{
		CScriptBuilder builder;
		builder.StartNewModule(ScriptEngine::get(), scripts[i].first.c_str());
		builder.AddSectionFromMemory(scripts[i].first.c_str(), scripts[i].second.c_str());
		builder.BuildModule();

	}

	Engine::get()->entities.configsasline = configs;
	//Engine::get()->entities->loadEntityTypesFromVec();

	Engine::get()->settings.gameMode = gamemode;
	Engine::get()->gameModeConfig = gameModeConfig;
	std::cout << "GAMEMODE:: " << gamemode << "\n";
	//mode = new GameMode(Engine::get()->gamemodestring, gamemodeconfig);
	//Engine::get()->gmode = mode;

	//std::cout << "\n\n" << gamemodeconfig << "\n\n";
	//std::cout << "PATHMOTHERPATH: " << pathsoccer << "\n";
	//std::cout << "\n\n" << filesoccer <<"\n\n";
	//Engine::get()->append("\n\n" + gamemodeconfig + "\n");
	//Engine::get()->append("PATHMOTHERPATH: " + pathsoccer);
	//Engine::get()->append("\n\n" + filesoccer + "\n");

	downloaded = true;

}

void Game::unpack_entities_list(sf::Packet p, ENetPeer* e)
{
	if(!downloaded || !ondownload)
		return;

	unsigned int  num;
	p >> num;
	for(unsigned int i = 0; i < num; i++)
	{
		std::string id;
		std::string name;
		float x, y;
		std::string playername;
		int team;
		p >> id >> name >> x >> y >> playername >> team;
		if(getEntityByID(id) == NULL)
		{
			Engine::get()->entities.createEntity(name, id);
			Entity* en = getEntityByID(id);
			en->setPosition(x, y);
			en->setTeam(team);
			en->esyncneeded = false;
			if(playername != "")
			{
				en->plug(new Player(playername));

			}
			en->justcreated = false;

		}
		else
		{
			getEntityByID(id)->setPosition(x, y);

		}


	}

}

sf::Packet Game::pack_player_info(sf::Packet p, ENetPeer* e)
{
	p.clear();
	p << "player_info";
	Player* pl = getLocalPlayer();
	p << pl->name;
	return p;

}

sf::Packet Game::pack_download(sf::Packet p, ENetPeer* e)
{
	p.clear();
	p << "download";

	std::vector<std::pair<std::string, std::string> > scripts = ScriptEngine::getInstance()->modscript;
	p << (int)scripts.size();
	for(unsigned int i = 0; i < scripts.size(); i++)
	{
		std::string path = scripts[i].first;
		std::string file = scripts[i].second;

		p << path << file;

	}

	std::vector<std::pair<std::string, std::string> > configs = Engine::get()->entities.configsasline;
	p << (int)configs.size();
	for(unsigned int i = 0; i < configs.size(); i++)
	{
		std::string path = configs[i].first;
		std::string file = configs[i].second;

		p << path << file;

	}

	p << Engine::get()->settings.gameMode;
	p << Engine::get()->gmode->configasstring;
	return p;

}

sf::Packet Game::pack_entities_list(sf::Packet p, ENetPeer* e)
{
	p.clear();
	p << "entities_list";
	unsigned int num = Engine::get()->entities.getNumOfEntities();
	p << num;
	for(unsigned int i = 0; i < num; i++)
	{
		Entity* e = Engine::get()->entities.getEntityByIndex(i);
		std::string id = e->getID();
		std::string name = e->getName();
		vec2f pos = e->getPosition();
		int team = e->getTeam();
		std::string playername = e->getPlayer() == NULL ? "" : e->getPlayer()->name;

		p << id << name << pos.x << pos.y << playername << team;

	}

	return p;

}

void Game::unpack_player_info(sf::Packet p, ENetPeer* e)
{
	std::string playername;
	p >> playername;
	Player* pl = new Player(playername);
	unsigned int chan = getNextAvailableChan();
	players.push_back(std::pair<ENetPeer*, std::pair<Player*, unsigned int> >(e, std::pair<Player*, unsigned int>(pl, chan)));
	mode->onPlayerJoin(pl);
	p.clear();
	p << "assign_chan" << chan;
	server->send(p, e, 1, true);

}

void Game::remPlayer(ENetPeer* e)
{
	std::vector<std::pair<ENetPeer*, std::pair<Player*, unsigned int> > >::iterator it;
	for(it = players.begin(); it != players.end(); it++)
	{
		if(e == it->first)
		{
			mode->onPlayerLeave(it->second.first);
			players.erase(it);
			return;
			//do stuff for player leaving game; TODO

		}

	}

}

//TODO SYNC GAME MODE CRAP

void Game::registerServer()
{
    server->setConnectHandler([this](ENetPeer* e){ this->onConnect(e);});
    server->setDisconnectedHandler([this](ENetPeer* e){ this->onDisconnect(e);});
	packMemberServer("download", &Game::pack_download);
	packMemberServer("entities_list", &Game::pack_entities_list);
	unpackMemberServer("player_info", &Game::unpack_player_info);
	packMemberServer("sync_entities", &Game::pack_sync_entities);
	unpackMemberServer("sync_entities", &Game::unpack_sync_entities);
	unpackMemberServer("rem_entity", &Game::unpack_rem_entity);
	unpackMemberServer("plug", &Game::unpack_plug);
	//unpackMemberServer("sync_player_entity", &Game::unpack_sync_player_entity);
	packMemberServer("sync_gamemode", &Game::pack_sync_gamemode);
	unpackMemberServer("sync_player", &Game::unpack_sync_player);
	//unpackMemberServer("unplug", &Game::unpack_unplug);
	unpackMemberServer("create_entity", &Game::unpack_create_entity);
	packMemberServer("tile_types", &Game::pack_tile_types);
	packMemberServer("map", &Game::pack_map);
	unpackMemberServer("sync_just_created", &Game::unpack_sync_just_created);
	packMemberServer("sync_health", &Game::pack_sync_health);

}



void Game::registerClient()
{
    client->setConnectHandler([this](ENetPeer* e){ this->onConnect(e);});
    client->setDisconnectedHandler([this](ENetPeer* e){ this->onDisconnect(e);});
    unpackMemberClient("download", &Game::unpack_download);
    unpackMemberClient("entities_list", &Game::unpack_entities_list);
    packMemberClient("player_info", &Game::pack_player_info);
	packMemberClient("sync_entities", &Game::pack_sync_entities);
    unpackMemberClient("sync_entities", &Game::unpack_sync_entities);
    unpackMemberClient("rem_entity", &Game::unpack_rem_entity_client);
    unpackMemberClient("plug", &Game::unpack_plug_client);
	unpackMemberClient("pluginto", &Game::unpack_pluginto);
    //packMemberClient("sync_player_entity", &Game::pack_sync_player_entity);
    unpackMemberClient("sync_gamemode", &Game::unpack_sync_gamemode);
    packMemberClient("sync_player", &Game::pack_sync_player);
    unpackMemberClient("sync_player", &Game::unpack_sync_player);
   // unpackMemberClient("unplug", &Game::unpack_unplug_client);
    unpackMemberClient("create_entity", &Game::unpack_create_entity_client);
    unpackMemberClient("tile_types", &Game::unpack_tile_types);
    unpackMemberClient("map", &Game::unpack_map);
	unpackMemberClient("sync_just_created", &Game::unpack_sync_just_created);
	unpackMemberClient("assign_chan", &Game::unpack_assign_chan);
	unpackMemberClient("sync_health", &Game::unpack_sync_health);

}

void Game::handleScript(std::string l)
{
	// Used for scripting for loops.
	unsigned int i = 1;

	if(isFunctOnLine("for", l))
		i = findIntVal("for", l);

	/*if(isFunctOnLine("rem", l))
	{
		unsigned int r = findIntParam("id", l);
		std::string type = findStringParam("type", l);

		for(unsigned int k = 0; k < objectTypes.size(); k++)
		{
			if(objectTypes[k]->getName() == type)
			{
				world->DestroyBody(objectTypes[k]->getObjectWithID(r)->getBody());
				objectTypes[k]->remObjectWithID(r);

			}

		}

	}
	else*/ if(isFunctOnLine("new", l))
	{
		std::string type = findStringParam("type", l);
		float x = findFloatParam("x", l);
		float y = findFloatParam("y", l);
		float r = findFloatParam("r", l);

		for(unsigned int u = 0; u < i; u++)
			Engine::get()->entities.createEntity(type, x, y, r);

	}
	else if(isFunctOnLine("debug_level", l))
		Engine::get()->settings.debugLevel = findIntVal("debug_level", l);
	else if(isFunctOnLine("quit", l))
	{
		win->close();
		return;

	}

}

void Game::loadEntities(std::string file)
{
	Config cfg;
	cfg.read(file);
	std::string dir = cfg.getValue("dir");

	for(unsigned int i = 0; i < cfg.getNumOfItems(); i++)
	{
		std::string type = cfg.getType(i);
		if(type == "string" && cfg.getKey(i) != "dir")
		{
			Engine::get()->entities.loadEntityType(dir + cfg.getValue(i));

		}
		else if(type == "string@")
		{
			ConfigItem item = cfg.getItem(i);
			for(unsigned int j = 0; j < item.getNumOfParams(); j++)
			{
				Engine::get()->entities.loadEntityType(dir + item.getParam(j));

			}

		}

	}

}

void Game::loadAmmoTypes(std::string file)
{
}

void Game::loadWeapons(std::string file)
{

}

