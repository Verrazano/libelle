#include "TopDownWorld.h"

TopDownWorld::TopDownWorld() : b2World(b2Vec2(0, 0))
{

}

TopDownWorld::TopDownWorld(float time_step, float vel_iter, float pos_iter) : b2World(b2Vec2(0, 0))
{
	this->time_step = time_step;
	this->vel_iter = vel_iter;
	this->pos_iter = pos_iter;

	b2BodyDef groundDef;
	ground = CreateBody(&groundDef);

	groundFriction.localAnchorA.SetZero();
	groundFriction.localAnchorB.SetZero();
	groundFriction.bodyA = ground;
	groundFriction.maxForce = 25;
	groundFriction.maxTorque = 400;
	groundFriction.collideConnected = true;

}

b2Body* TopDownWorld::CreateFrictionBody(const b2BodyDef* def, float force, float torque)
{
	b2Body* tempBody = b2World::CreateBody(def);
	groundFriction.bodyB = tempBody;

	groundFriction.maxForce = force;
	groundFriction.maxTorque = torque;

	CreateJoint(&groundFriction);
	return tempBody;

}

void TopDownWorld::step()
{
	Step(time_step, vel_iter, pos_iter);

}
