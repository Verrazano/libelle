#ifndef TOPDOWNWORLD_H_
#define TOPDOWNWORLD_H_

#include <Box2D/Box2D.h>

class TopDownWorld : public b2World
{
public:
	TopDownWorld();
	TopDownWorld(float time_step, float vel_iter, float pos_iter);

	b2Body* CreateFrictionBody(const b2BodyDef* def, float force = 25, float torque = 400);

	void step();


private:
	b2Body* ground;
	b2FrictionJointDef groundFriction;

	float time_step;
	float vel_iter;
	float pos_iter;

};

#endif /* TOPDOWNWORLD_H_ */
