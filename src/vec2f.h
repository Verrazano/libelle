/*==============================*/
//Author: Harrison M
//Date 11/23/12
//Description: vec2f holds x, and y values, for lines and points.
/*==============================*/

#ifndef VEC2F_H_
#define VEC2F_H_

#include <Box2D/Common/b2Math.h>
#include <SFML/System/Vector2.hpp>
#include <angelscript.h>

struct vec2f
{
public:
	static void registerVec2f(asIScriptEngine* engine);

	static void vec2fConstruct(asIScriptGeneric * gen);
	static void vec2fCopyConstruct(asIScriptGeneric * gen);
	static void vec2fParamConstruct(asIScriptGeneric * gen);
	static void vec2fDestruct(asIScriptGeneric * gen);

	//constructors for vec2f
	vec2f();
	vec2f(const vec2f& copy);
	vec2f(sf::Vector2f copy);
	vec2f(b2Vec2 copy);
	vec2f(float x, float y);

	//operators
	bool operator ==(vec2f a);
	bool operator !=(vec2f a);
	vec2f operator =(vec2f a);
	vec2f operator +(vec2f a);
	vec2f operator -(vec2f a);
	vec2f operator +=(vec2f a);
	vec2f operator -=(vec2f a);
	vec2f operator /(float n);
	vec2f operator /=(float n);
	float operator *(vec2f a); //dot product

	//vector math functions
	float length(); //returns the length of this
	float lengthSquared(); //returns the length squared of this
	void normalize(); //normalizes this
	vec2f normalized(); //returns the normalized vector
	//project, projected, reflect = not functional yet
	void project(vec2f a); //projects this onto a
	vec2f projected(vec2f a); //returns the projected vector
	void reflect(vec2f a); //reflects this about a

	float x;
	float y;

};

//vector helper functions
vec2f line(vec2f a, vec2f b); //returns the line made by a and b
vec2f normal(vec2f a); //returns the normal of a

float distance(vec2f a, vec2f b); //gets the distance between a and b
float angle(vec2f a, vec2f b); //gets the angle between a and b

bool parallel(vec2f a, vec2f b); //returns true if a is parallel to b

void print(vec2f a); //prints the x and y of a

#endif /* VEC2F_H_ */
