#ifndef GAME_H_
#define GAME_H_

#include "World/TopDownWorld.h"
#include "Box2DDebugDrawer/Box2DDebugDrawer.h"
#include <SFML/Graphics.hpp>
#include "Console/Console.h"
#include "FrameClock/ClockHUD.h"
#include "Util/bslib/bslib.h"
#include "Engine/Log/Log.h"
#include "Engine/ResourceManager/ResourceManager.h"
#include "add_on/scriptbuilder/scriptbuilder.h"
#include "Entity/EntityManager/EntityManager.h"
#include "Entity/Entity.h"

#include "Util/Config/Config.h"
#include "Engine/Engine.h"
#include "GameMode.h"
#include "Network/LibelleClient.h"
#include "Network/LibelleServer.h"
#include <iostream>
#include <enet/enet.h>
#include <SFML/System.hpp>
#include <time.h>

class Game
{
public:
	bool shouldSyncEntities();
	Game(LibelleClient* client);
	Game(LibelleServer* server);

	void start();



	void handleScript(std::string l);

	void loadEntities(std::string file);
	void loadAmmoTypes(std::string file);
	void loadWeapons(std::string file);

	void unpackMemberClient(std::string requestName, void(Game::*member)(sf::Packet, ENetPeer*));

	void packMemberClient(std::string requestName, sf::Packet(Game::*member)(sf::Packet, ENetPeer*));

	void unpackMemberServer(std::string requestName, void(Game::*member)(sf::Packet, ENetPeer*));
	void packMemberServer(std::string requestName, sf::Packet(Game::*member)(sf::Packet, ENetPeer*));

	void SynccreateEntity(Entity* e);
	void remEntity(Entity* e);
	void plugPlayer(Player* pl, Entity* e);
	void unplugPlayer(Entity* e);

	unsigned int playersCount();
	Player* getPlayerByIndex(unsigned int i);

	void syncJustCreated(Entity* e)
	{
		sf::Packet p;
		p << "sync_just_created";
		p << e->getID();
		std::string syncstr = e->tosync;
		e->tosync = "";
		p << syncstr;
		p << e->getPosition().x << e->getPosition().y << e->getVelocity().x << e->getVelocity().y << e->getAngle() << e->getAngularVelocity();
		p << e->getTeam();
		//std::cout << "velx " << e->getVelocity().x << " vely " << e->getVelocity().y << "\n";
		if(isServer())
		{
			server->broadcast(p, 1, true);

		}
		else
		{
			client->send(p, 1, true);

		}

	}

	sf::Packet pack_sync_health(sf::Packet p, ENetPeer* e)
	{
		p.clear();
		p << "sync_health";
		p << (unsigned int)healthupdate.size();
		while(!healthupdate.empty())
		{
			p << healthupdate.front()->getID() << healthupdate.front()->health;
			healthupdate.pop();

		}

		return p;

	}

	void unpack_sync_health(sf::Packet p, ENetPeer* e)
	{
		unsigned int num;
		p >> num;
		for(unsigned int i = 0; i < num; i++)
		{
			std::string id;
			float health;
			p >> id >> health;
			Entity* en = getEntityByID(id);
			if(en != NULL)
			    en->health = health;

		}

	}


	void unpack_sync_just_created(sf::Packet p, ENetPeer* e)
	{
		std::string id;
		float x, y, velx, vely, a, vela;
		std::string sync;
		int team;
		p >> id >> sync >> x >> y >> velx >> vely >> a >> vela >> team;
		//std::cout << "velx: " << velx << " vely" << vely << "\n";
		Entity* en = getEntityByID(id);
		if(en == NULL)
		{
			return;
			std::cout << "returning because null\n";

		}
		//THIS IS GROSS BUT FUCK IT FOR NOW
		while(sync.find("^&")+2 < sync.size())
		{
			std::string type = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);
			std::string name = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);
			std::string val = sync.substr(0, sync.find("^&"));
			sync = sync.substr(sync.find("^&")+2);

			if(type == "int")
				en->setInt(name, toInt(val));
			else if(type == "uint")
				en->setUInt(name, toUInt(val));
			else if(type == "float")
				en->setFloat(name, toFloat(val));
			else if(type == "string")
				en->setString(name, val);
			else if(type == "bool")
				en->setBool(name, toBool(val));

		}

		en->setPosition(x, y);
		en->setVelocity(velx, vely);
		en->setAngle(a);
		en->setAngularVelocity(vela);
		en->setTeam(team);
		if(isServer())
		{
			p.clear();
			p << "sync_just_created";
			p << id << sync << x << y << velx << vely << a << vela << team;
			server->broadcastExcluding(p, e, 1, true);

		}


	}

	Player* getPlayerByName(std::string name)
	{
		for(unsigned int i = 0; i < players.size(); i++)
		{
			if(players[i].second.first->name == name)
				return players[i].second.first;

		}

		return NULL;

	}

	unsigned int getNextAvailableChan()
	{
		unsigned int chan = 1;
		for(unsigned int u = 2; u < 65; u++)
		{
			bool haschan = false;
			for(unsigned int i = 0; i < players.size(); i++)
			{
				if(players[i].second.second == u)
				{
					haschan = true;
					break;

				}

			}

			if(!haschan)
			{
				chan = u;
				break;

			}

		}

		std::cout << "nextchan: " << chan << "\n";
		return chan;

	}

	void unpack_assign_chan(sf::Packet p, ENetPeer* e)
	{
		p >> mychan;
		std::cout << "mychan: " << mychan << "\n";

	}

	unsigned int mychan;
	std::queue<Entity*> healthupdate;

protected:
	void registerServer();
	void registerClient();

	void onDisconnect(ENetPeer* e);
	void onConnect(ENetPeer* e);

	bool downloaded;
	bool ondownload;

	float loadPercent;
	unsigned int loadTotal;
	sf::Text loadStr;
	std::string loadingFile;
	sf::RectangleShape loadSprite;

	time_t lastsync;

	/*syncing
	 *
	 * client syncs players entities only
	 * server syncs all entities
	 *
	 */

	//CLIENT
	void unpack_download(sf::Packet p, ENetPeer* e); //unpack all the shit
	void unpack_entities_list(sf::Packet p, ENetPeer* e);
	sf::Packet pack_player_info(sf::Packet p, ENetPeer* e);
	void unpack_sync_entities(sf::Packet p, ENetPeer* e);
	void unpack_rem_entity_client(sf::Packet p, ENetPeer* e);
	void unpack_plug_client(sf::Packet p, ENetPeer* e);
	void unpack_pluginto(sf::Packet p, ENetPeer* e);
	sf::Packet pack_sync_player_entity(sf::Packet p, ENetPeer* e);
	void unpack_sync_gamemode(sf::Packet p, ENetPeer* e);
	sf::Packet pack_sync_player(sf::Packet p, ENetPeer* e);
	void unpack_unplug_client(sf::Packet p, ENetPeer* e);
	void unpack_create_entity_client(sf::Packet p, ENetPeer* e);
	void unpack_tile_types(sf::Packet p, ENetPeer* e);
	void unpack_map(sf::Packet p, ENetPeer* e);


	//SERVER
	sf::Packet pack_download(sf::Packet p, ENetPeer* e); //packet contains hashes of existing files TODO
	sf::Packet pack_entities_list(sf::Packet p, ENetPeer* e);
	void unpack_player_info(sf::Packet p, ENetPeer* e);
	sf::Packet pack_sync_entities(sf::Packet p, ENetPeer* e);
	void unpack_rem_entity(sf::Packet p, ENetPeer* e);
	void unpack_plug(sf::Packet p, ENetPeer* e);
	void unpack_sync_player_entity(sf::Packet p, ENetPeer* e);
	sf::Packet pack_sync_gamemode(sf::Packet p, ENetPeer* e);
	void unpack_sync_player(sf::Packet p, ENetPeer* e);
	void unpack_unplug(sf::Packet p, ENetPeer* e);
	void unpack_create_entity(sf::Packet p, ENetPeer* e);
	sf::Packet pack_tile_types(sf::Packet p, ENetPeer* e);
	sf::Packet pack_map(sf::Packet p, ENetPeer* e);

	void remPlayer(ENetPeer* e);
	ENetPeer* getPeer(Player* p);
	Player* getPlayer(ENetPeer* e);

	sf::RenderWindow* win;
	sf::Event event;

	LibelleClient* client;
	LibelleServer* server;

	GameMode* mode;

	sfx::FrameClock clock;
	ClockHUD hud;

	std::vector<std::pair<ENetPeer*, std::pair<Player*, unsigned int> > > players;

	/*std::vector<ObjectType*> objectTypes;
	std::vector<WeaponType*> weaponTypes;
	std::vector<AmmoType*> ammoTypes;*/

	Console console;
	bool consoleOpen;
	bool homePressed;

	//SoldierType* libelleType;

	std::string gameModeConfig;
	//Player* localPlayer;

};

#endif /* GAME_H_ */
