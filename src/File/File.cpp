//file.cpp

#include "File.h"
#include <fstream>

File::File()
{
}

File::File(const File& copy)
{
	m_fileName = copy.m_fileName;
	m_fileVector = copy.m_fileVector;

}

File::File(std::string fileName, bool readOnOpen)
{
	m_fileName = fileName;
	if(readOnOpen)
		read();

}

File::~File()
{

}

void File::read()
{
	m_fileVector.clear();

	std::fstream f((m_fileName).c_str());
	std::string line;

	if(!f)
	{
	  write();
	  return;

	}

	while(!f.eof())
	{
		std::getline(f, line);
		m_fileVector.push_back(line);

	}

}

void File::write()
{
	std::ofstream f((m_fileName).c_str());
	for(int i = 0; i < m_fileVector.size(); i++)
	{
		if(i != 0)
			f << "\n" << m_fileVector[i];
		else
			f << m_fileVector[i];

	}

}

std::string File::getFileName()
{
	return m_fileName;

}

void File::setFileName(std::string fileName)
{
	m_fileName = fileName;

}

std::vector<std::string>& File::getFile()
{
	return m_fileVector;

}

void File::setFile(std::vector<std::string> fileVector)
{
	m_fileVector = fileVector;

}

std::string File::getLine(unsigned int lineNum)
{
	if(lineNum <= m_fileVector.size())
		return m_fileVector[lineNum];

	return "";

}

void File::setLine(std::string line, unsigned int lineNum)
{
	if(lineNum <= m_fileVector.size())
		m_fileVector[lineNum] = line;

}

//adds line to the end of the file
void File::append(std::string line)
{
	m_fileVector.push_back(line);

}

void File::deleteLine(unsigned int lineNum)
{
	if(lineNum <= m_fileVector.size())
	{
		std::vector<std::string>::iterator it = m_fileVector.begin();
		for(int i = 0; i < lineNum; i++, it++);
			m_fileVector.erase(it);

	}


}

void File::deleteFirstLine()
{
	m_fileVector.erase(m_fileVector.begin());

}

void File::deleteLastLine()
{
	m_fileVector.erase(m_fileVector.end());

}

unsigned int File::getLength()
{
	return m_fileVector.size();

}

std::string File::asString()
{
	std::string s;
	for(unsigned int i = 0; i < m_fileVector.size(); i++)
	{
		s += m_fileVector[i] + "\n";

	}
	return s;

}
