#ifndef ENGINE_H_
#define ENGINE_H_

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "ResourceManager/EngineResourceManager.h"
#include "Log/Log.h"
#include "../Box2DDebugDrawer/Box2DDebugDrawer.h"
#include "../World/TopDownWorld.h"
#include "../Entity/EntityContactListener.h"
#include "SoundManager.h"
#include "../Entity/EntityManager/EntityManager.h"
#include "../Player.h"
#include "../Particle.h"
#include "../Map/Tile.h"
#include "EngineSettings.h"

class GameMode;
class Game;

class Engine
{
public:
	static Engine* get();

	void draw(const sf::Drawable& drawable);
	void step();

	void append(std::string msg);
	//void playSound(std::string sound);
	//void playSound(std::string sound, float x, float y);

	Entity* createEntity(std::string type, std::string id, float x = 0, float y = 0);
	Entity* createEntity(std::string type, float x = 0, float y = 0);
	Entity* getEntityByID(std::string id);
	void clearEntities();

	void resetCounters();
	unsigned int getTicksElapsed();
	float getGameTime();
	Player* getLocalPlayer();

	sf::RenderWindow window;
	TopDownWorld* world;
	EngineResourceManager files;
	asIScriptEngine* script;
	Log console;
	SoundManager sounds;
	EntityManager entities;
	sf::Text textdrawer;
	sf::RectangleShape spritedrawer;
	Game* game;

	TopDownWorld* particleWorld;
	ParticleManager particles;

	sf::View gameview;
	sf::FloatRect gamePortView; //the real deal

	Player* localPlayer;
	sf::Clock gameTimer;
	unsigned int tickCounter;
	GameMode* gmode;

	std::vector<TileType*> tiletypes;
	std::vector<Tile*> map;

	bool isServer;
	bool hasFocus;

	sf::Shader teamshader;
	void prepareTeamShader(int team)
	{
		if(team == 0)
		{
			teamshader.setParameter("r1", sf::Color(175, 37, 57));
			teamshader.setParameter("r2", sf::Color(101, 22, 45));
			teamshader.setParameter("r3", sf::Color(54, 9, 34));
			teamshader.setParameter("r4", sf::Color(212, 78, 71));

		}
		else if(team == 1)
		{
			teamshader.setParameter("r1", sf::Color(0, 135, 173));
			teamshader.setParameter("r2", sf::Color(0, 83, 154));
			teamshader.setParameter("r3", sf::Color(0, 61, 100));
			teamshader.setParameter("r4", sf::Color(17, 187, 174));

		}
		else if(team == -1)
		{
			teamshader.setParameter("r1", sf::Color(175, 175, 175));
			teamshader.setParameter("r2", sf::Color(101, 101, 101));
			teamshader.setParameter("r3", sf::Color(54, 54, 54));
			teamshader.setParameter("r4", sf::Color(212, 212, 212));

		}

	}

	EngineSettings settings;
	std::string gameModeConfig;

private:
	void initialize();

	Box2DDebugDrawer* b2DebugDrawer;
	EntityContactListener* contactListener;

	static Engine* instance;

	Engine();
	~Engine();
	Engine(Engine const&){}; // Non-copyable
	void operator = (Engine const&){}; // No assignment

};

#endif /* ENGINE_H_ */
