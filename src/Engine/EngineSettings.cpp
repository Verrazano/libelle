#include "EngineSettings.h"

EngineSettings::EngineSettings()
{
	width = 800;
	height = 600;
	frameRate = 60.0f;
	fullScreen = false;
	debugLevel = 1;
	ip = "localhost";
	port = 50301;
	gameMode = "deathmatch";
	playerName = "Unmamed_Player";

	add("width", offsetof(EngineSettings, width), width);
	add("height", offsetof(EngineSettings, height), height);
	add("frameRate", offsetof(EngineSettings, frameRate), frameRate);
	add("fullScreen", offsetof(EngineSettings, fullScreen), fullScreen);
	add("debugLevel", offsetof(EngineSettings, debugLevel), debugLevel);
	add("ip", offsetof(EngineSettings, ip), ip);
	add("port", offsetof(EngineSettings, port), port);
	add("gameMode", offsetof(EngineSettings, gameMode), gameMode);
	add("playerName", offsetof(EngineSettings, playerName), playerName);

}
