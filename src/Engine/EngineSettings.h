#ifndef ENGINESETTINGS_H_
#define ENGINESETTINGS_H_

#include "../Util/Settings/Settings.h"

class EngineSettings : public Settings
{
public:
	EngineSettings();

	float width;
	float height;
	float frameRate;
	bool fullScreen;
	unsigned int debugLevel;
	std::string ip;
	unsigned int port;
	std::string gameMode;
	std::string playerName;

};


#endif /* ENGINESETTINGS_H_ */
