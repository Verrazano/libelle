#ifndef RESOURCE_H_
#define RESOURCE_H_

#include "ResourceFactory.h"

class Resource
{
public:
	Resource();
	Resource(void* data, std::string path, ResourceFactory::Recycler recycler);

	~Resource();

	void* getData();

	std::string getPath();

	ResourceFactory::Recycler getRecycler();

private:
	void* m_data;
	std::string m_path;
	ResourceFactory::Recycler m_recycler;

};

#endif /* RESOURCE_H_ */
