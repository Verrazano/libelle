#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <string>
#include <vector>

#ifdef _WIN32
#include "dirent.h"
#elif __linux__
#include <dirent.h>
#endif

class FileSystem
{
public:
	FileSystem();
	~FileSystem();

	void search(std::string path);

	void reset();

	bool hasPath(std::string name);

	std::string fullPath(std::string name);

	unsigned int getNumOfPaths();

	std::string getPath(unsigned int index);

	unsigned int getIndex(std::string name);

	std::vector<std::string> getPaths();

	void setPaths(std::vector<std::string> paths);

	static DIR* isFolder(std::string path);

	static std::string getExtension(std::string path);

	static bool hasExtension(std::string path, std::string extension);

	static std::string getName(std::string path);

private:
	bool poll(std::string path);

	std::vector<std::string> m_paths;

};

#endif /* FILESYSTEM_H_ */
