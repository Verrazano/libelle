#ifndef RESOURCEFACTORY_H_
#define RESOURCEFACTORY_H_

#include <string>

class Resource;

class ResourceFactory
{
public:
    typedef Resource*(*Factory)(std::string);
    typedef void(*Recycler)(void*);

	ResourceFactory();
	ResourceFactory(std::string extension, ResourceFactory::Factory factory, ResourceFactory::Recycler recycler);

	~ResourceFactory();

	std::string getExtension();

	ResourceFactory::Factory getFactory();

	ResourceFactory::Recycler getRecycler();

	Resource* create(std::string path);

private:
	std::string m_extension;

	Factory m_factory;
	Recycler m_recycler;

};

#endif /* RESOURCEFACTORY_H_ */
