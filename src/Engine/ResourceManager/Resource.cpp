#include "Resource.h"

Resource::Resource()
{
	m_data = NULL;

}

Resource::Resource(void* data, std::string path, ResourceFactory::Recycler recycler)
{
	m_data = data;
	m_path = path;
	m_recycler = recycler;

}

Resource::~Resource()
{
	if(m_data == NULL)
	{
		return;

	}

	m_recycler(m_data);

}

void* Resource::getData()
{
	return m_data;

}

std::string Resource::getPath()
{
	return m_path;

}

ResourceFactory::Recycler Resource::getRecycler()
{
	return m_recycler;

}
