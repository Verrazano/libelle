#include "Engine.h"
#include <ctime>
#include "../Util/Config/Config.h"
#include "../ScriptEngine/ScriptEngine.h"
#include <iostream>
#include "../GameMode.h"
#include "../File/File.h"

Engine* Engine::instance;

Engine* Engine::get()
{
	static bool derp = false;
	if(!derp)
	{
		std::cout << "creating new engine\n";
		derp = true;
		instance = new Engine;
		std::cout << "engine instance: " << instance << "\n";

	}

	return instance;

}

void Engine::draw(const sf::Drawable& drawable)
{
	//if(win != NULL)
		window.draw(drawable);

}

void Engine::step()
{
	world->step();

}

void Engine::append(std::string msg)
{
	std::cout << msg;

}

Entity* Engine::createEntity(std::string type, float x, float y)
{
	return entities.createEntity(type, x, y);

}

Entity* Engine::createEntity(std::string type, std::string id, float x, float y)
{
	return entities.createEntity(type, id, x, y);

}

Entity* Engine::getEntityByID(std::string id)
{
	return entities.getEntityByID(id);

}

void Engine::clearEntities()
{
	entities.clearEntities();

}

void Engine::initialize()
{
	srand(time(NULL));
	// console - loaded before script so that stuff can be printed out
	// resource
	settings.read(Config::load("settings.cfg"));

	files.search("res"); //TODO make this path loaded from the settings.cfg

	settings.gameMode = "res/rules/"+settings.gameMode+"/"+settings.gameMode+".cfg";

	localPlayer = new Player(settings.playerName);
	localPlayer->setBool("local_player", true);
	localPlayer->e = NULL;

	window.setFramerateLimit(settings.frameRate);
	sf::ContextSettings windowSettings;
	//TODO antialiasing from config
	if(settings.fullScreen)
	{
		window.create(sf::VideoMode::getFullscreenModes()[0], "Libelle Alpha", sf::Style::Fullscreen, windowSettings);

	}
	else
	{
		window.create(sf::VideoMode(settings.width, settings.height), "Libelle Alpha", sf::Style::Close, windowSettings);

	}

	window.setVisible(false);


	// world
	world = new TopDownWorld(1.f/60.f, 10, 8);

	b2DebugDrawer = new Box2DDebugDrawer(&window);
	world->SetDebugDraw(b2DebugDrawer);

	contactListener = new EntityContactListener;
	world->SetContactListener(contactListener);
	world->SetContactFilter(contactListener);

	// script
	script = ScriptEngine::get();

	textdrawer = sf::Text("", *files.get<sf::Font>("consolas.ttf"), 14);

	gameview = sf::View(sf::FloatRect(0, 0, 800, 600));
	resetCounters();

	particleWorld = new TopDownWorld(1.f/60.f, 10, 8);

	gmode = NULL;
	isServer = false;
	hasFocus = true;

	//res/gui/shaders/teamcolor.frag
	teamshader.loadFromFile("res/gui/shaders/teamcolor.frag", sf::Shader::Fragment);
	teamshader.setParameter("texture", sf::Shader::CurrentTexture);


}

Engine::Engine() :
		console("logs/console")
{
	//win = NULL;
	//resource = NULL;
	script = NULL;

	initialize();

}

void Engine::resetCounters()
{
	gameTimer.restart();
	tickCounter = 0;

}

unsigned int Engine::getTicksElapsed()
{
	return tickCounter;

}

float Engine::getGameTime()
{
	return gameTimer.getElapsedTime().asSeconds();

}

Player* Engine::getLocalPlayer()
{
	return localPlayer;

}

Engine::~Engine()
{
	//delete win;

}
