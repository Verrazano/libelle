#include "SoundManager.h"
#include "Engine.h"
#include <math.h>
#include <iostream>

SoundManager::SoundManager()
{
	sfx_static.clear();
	sfx.clear();

}

SoundManager::~SoundManager()
{
	while(!sfx_static.empty())
	{
		delete (*sfx_static.begin());
		sfx_static.erase(sfx_static.begin());

	}

	while(!sfx.empty())
	{
		delete (*sfx.begin()).first;
		sfx.erase(sfx.begin());

	}

}

void SoundManager::playSound(std::string sound)
{
	sf::Sound* s = new sf::Sound;
	s->setBuffer(*Engine::get()->files.get<sf::SoundBuffer>(sound));
	sfx_static.push_back(s);
	s->play();

}

void SoundManager::playSound(std::string sound, float x, float y)
{
	sf::Sound* s = new sf::Sound;
	s->setBuffer(*(Engine::get()->files.get<sf::SoundBuffer>(sound)));
	sfx.push_back(std::pair<sf::Sound*, vec2f>(s, vec2f(x, y)));
	s->play();

}

void SoundManager::update()
{
	for(unsigned int i = 0; i < sfx.size(); i++)
	{
		vec2f p = sfx[i].second;
		vec2f c = Engine::get()->gameview.getCenter();

		float d = distance(p, c);
		d = d/800;
		d = 1-d; //flip it
		if(d < 0)
			d = 0;
		else if(d > 1)
			d = 1;

		sfx[i].first->setVolume(d*100);

	}

	std::vector<sf::Sound*>::iterator it;
	for(it = sfx_static.begin(); it != sfx_static.end(); it++)
	{
		if((*it)->getStatus() != sf::SoundSource::Playing)
		{
			delete (*it);
			sfx_static.erase(it);
			break;

		}


	}

	std::vector<std::pair<sf::Sound*, vec2f> >::iterator it2;
	for(it2 = sfx.begin(); it2 != sfx.end(); it2++)
	{
		if((*it2).first->getStatus() != sf::SoundSource::Playing)
		{
			delete (*it2).first;
			sfx.erase(it2);
			break;

		}

	}

}
