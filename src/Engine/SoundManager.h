#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_

#include <SFML/Audio.hpp>
#include "../vec2f.h"
#include <vector>

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	void playSound(std::string sound);
	void playSound(std::string sound, float x, float y);

	void update();

private:
	std::vector<sf::Sound*> sfx_static;
	std::vector<std::pair<sf::Sound*, vec2f> > sfx;

};

#endif /* SOUNDMANAGER_H_ */
