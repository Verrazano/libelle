#ifndef LOG_H_
#define LOG_H_

#include <string>
#include <fstream>

class Log
{
public:
	Log();
	Log(std::string name);

	~Log();

	void append(std::string s);

	std::string getName();
	void setName(std::string name);

private:
	std::string getLogTitle();
	std::string name;
	std::ofstream file;

};

#endif /* LOG_H_ */
