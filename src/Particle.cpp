#include "Particle.h"
#include "Engine/Engine.h"
#include "Util/convert/b2tosf.h"
#include "Util/convert/units.h"

Particle::Particle(float x, float y, std::string texture, float size, bool collide, float velx, float vely, float lifetime)
{
	sprite.setSize(sf::Vector2f(Engine::get()->files.get<sf::Texture>(texture)->getSize()));
	sprite.setTexture(Engine::get()->files.get<sf::Texture>(texture));
	sprite.setOrigin(sprite.getSize().x/2, sprite.getSize().y/2);

	b2Shape* shape;
	shape =  new b2CircleShape;
	static_cast<b2CircleShape*>(shape)->m_radius = toMeters(size);
	static_cast<b2CircleShape*>(shape)->m_p.Set(0, 0);

	b2FixtureDef* fdef = new b2FixtureDef;
	fdef->shape = shape;

	fdef->isSensor = !collide;
	b2BodyDef* bdef = new b2BodyDef;
	bdef->position = toWorld(sf::Vector2f(x, y));
	bdef->angularVelocity = (rand()%20)-10;
	bdef->type = b2_dynamicBody;
	b = Engine::get()->particleWorld->CreateFrictionBody(bdef, 20, 20);

	b->CreateFixture(fdef);
	b->ApplyForceToCenter(toWorld(sf::Vector2f(velx, vely)), true);

	this->lifetime = lifetime;
	lifeclock.restart();

}

Particle::Particle(float xp, float yp, std::string texture,
		unsigned int x, unsigned int y, unsigned int width, unsigned int height, //frame part
		float size, bool collide, float velx, float vely, float lifetime)
{
	sprite.setSize(sf::Vector2f(width, height));
	sprite.setTexture(Engine::get()->files.get<sf::Texture>(texture));
	sprite.setTextureRect(sf::IntRect(x, y, width, height));
	sprite.setOrigin(width/2, height/2);

	b2Shape* shape;
	shape =  new b2CircleShape;
	static_cast<b2CircleShape*>(shape)->m_radius = toMeters(size);
	static_cast<b2CircleShape*>(shape)->m_p.Set(0, 0);

	b2FixtureDef* fdef = new b2FixtureDef;
	fdef->shape = shape;

	fdef->isSensor = !collide;
	b2BodyDef* bdef = new b2BodyDef;
	bdef->position = toWorld(sf::Vector2f(xp, yp));
	bdef->angularVelocity = (rand()%20)-10;
	bdef->type = b2_dynamicBody;
	b = Engine::get()->particleWorld->CreateFrictionBody(bdef, 20, 20);

	b->CreateFixture(fdef);
	b->ApplyForceToCenter(b2Vec2(velx, vely), true);

	this->lifetime = lifetime;
	lifeclock.restart();

}

void Particle::update()
{
	sprite.setRotation(toDegrees(b->GetAngle()));
	sprite.setPosition(toScreen(b->GetPosition()));
	Engine::get()->window.draw(sprite);

}

ParticleManager::ParticleManager()
{
}

ParticleManager::~ParticleManager()
{
	while(!particles.empty())
	{
		deleteParticle(*particles.begin());

	}

}

void ParticleManager::createParticle(Particle* p)
{
	particles.push_back(p);

}

void ParticleManager::deleteParticle(Particle* p)
{
	std::vector<Particle*>::iterator it;
	for(it = particles.begin(); it != particles.end(); it++)
	{
		if(p == (*it))
		{
			Engine::get()->particleWorld->DestroyBody((*it)->b);
			particles.erase(it);
			return;

		}

	}

}

void ParticleManager::update()
{
	for(unsigned int i = 0; i < particles.size(); i++)
	{
		particles[i]->update();
		if(particles[i]->lifeclock.getElapsedTime().asSeconds() > particles[i]->lifetime)
		{
			deleteParticle(particles[i]);
			i--;

		}

	}

}
