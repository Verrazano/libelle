#ifndef PLAYER_H_
#define PLAYER_H_

class Player;

//a struct for holding info about a player
#include <SFML/Graphics.hpp>
#include "Util/convert/mousetoview.h"
#include <angelscript.h>
#include "Entity/VarManager/VarManager.h"
#include "Util/convert/convertstring.h"
#include <queue>

#include "XboxController.h"

class Entity;

class Player : public VarManager
{
public:
	static void registerPlayer(asIScriptEngine* engine);

	std::string tosync;

	Player(std::string name);

	void setKeys(unsigned int up, unsigned int down,
			unsigned int left, unsigned int right,
			unsigned int fire1, unsigned int reload,
			unsigned int nextweapon, unsigned int prevweapon,
			unsigned int action,
			bool useClickFire, bool useScrollSwitch);

	Entity* getEntity();

	void setKeysDefault();

	void setJoystickDefault(bool);

	void syncInt(std::string s)
	{
		int i = getInt(s);
		std::string syncstr = "int^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncint: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncUInt(std::string s)
	{
		//std::cout << "syncuint\n";
		unsigned int i = getUInt(s);
		std::string syncstr = "uint^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncuint: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncFloat(std::string s)
	{
		//std::cout << "syncfloat\n";
		float i = getFloat(s);
		std::string syncstr = "float^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncfloat: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncString(std::string s)
	{
		//std::cout << "syncstring\n";
		std::string i = getString(s);
		std::string syncstr = "string^&"+s+"^&"+i+"^&"; //.. hopefully no one will ever use this in a string character
		//std::cout << "syncstring: " << syncstr << "\n";
		tosync += syncstr;

	}

	void syncBool(std::string s)
	{
		//std::cout << "syncbool\n";
		bool i = this->getBool(s);
		std::string syncstr = "bool^&"+s+"^&"+toString(i)+"^&";
		//std::cout << "syncbool: " << syncstr << "\n";
		tosync += syncstr;

	}

	void setInt(std::string s, int u)
	{
		VarManager::setInt(s, u);

	}

	int getInt(std::string s)
	{
		return VarManager::getInt(s);

	}

	void setUInt(std::string s, unsigned int u)
	{
		VarManager::setUInt(s, u);

	}

	unsigned int getUInt(std::string s)
	{
		return VarManager::getUInt(s);

	}

	void setFloat(std::string s, float u)
	{
		VarManager::setFloat(s, u);

	}

	float getFloat(std::string s)
	{
		return VarManager::getFloat(s);

	}

	void setString(std::string s, std::string u)
	{
		VarManager::setString(s, u);

	}

	std::string getString(std::string s)
	{
		return VarManager::getString(s);

	}

	void setBool(std::string s, bool u)
	{
		VarManager::setBool(s, u);

	}

	bool getBool(std::string s)
	{
		return VarManager::getBool(s);

	}

	bool isKeyPressed(unsigned int);
	bool fireKeyPressed();

	bool isJoystickButtonPressed(unsigned int);

	bool isJoystickConnected(unsigned int);

	std::string name;

	unsigned int up, down, left, right, fire, reload, nextweapon, prevweapon, action;
	bool useClickFire;
	bool useScrollSwitch;
	bool prevButton;
	bool nextButton;

	bool firedLast;

	Entity* e;


	//Xbox
	XboxController controller;
};

#endif /* PLAYER_H_ */
