#include "Tile.h"
#include "../Engine/Engine.h"

Tile::Tile(TileType* tt, float x, float y, float z)
{
	if(z == 0)
		z = tt->defaultz;

	tilename = tt->tilename;
	tilesprite.setSize(tt->width, tt->height);
	tilesprite.setPosition(x, y);
	tilesprite.setTexture(Engine::get()->files.get<sf::Texture>(tt->tilesprite));
	tilesprite.setZ(z);

}

Tile::Tile(float x, float y, std::string name, std::string sprite, float width, float height, float z)
{
	tilename = name;
	tilesprite.setSize(width, height);
	tilesprite.setPosition(x, y);
	tilesprite.setTexture(Engine::get()->files.get<sf::Texture>(sprite));
	tilesprite.setZ(z);

}

TileType::TileType(std::string name, std::string sprite, float width, float height, float defaultz)
{
	tilename = name;
	tilesprite = sprite;
	this->width = width;
	this->height = height;
	this->defaultz = defaultz;

}
