#ifndef TILE_H_
#define TILE_H_

#include "../Entity/SpriteLayer.h"

class TileType;

class Tile
{
public:
	Tile(TileType* tt, float x, float y, float z = 0);
	Tile(float x, float y, std::string name, std::string sprite, float width, float height, float z = 0);

	std::string tilename;
	SpriteLayer tilesprite;

};

class TileType
{
public:
	TileType(std::string name, std::string sprite, float width, float height, float defaultz = 0);
	std::string tilename;
	std::string tilesprite;
	float defaultz;
	float width, height;

};

#endif /* TILE_H_ */
