#ifndef MAP_H_
#define MAP_H_

#include <string>

class Map
{
public:
	Map(std::string file);

	void load();
	void parse();

private:
	std::string file;

};

Map::Map(std::string f)
{
	file = f;

}


//LOADING
/* load tile types
 * load entity types
 * load multi occurance types
 *
 * load 3 tile map files
 * load entities on all three levels
 * load walls
 *
 */

//SAVING
/* save all entity types used in map
 * save multi occurance types
 * save tile types and represent with a number
 *
 */

void Map::load()
{


}

void Map::parse()
{

}

#endif /* MAP_H_ */
