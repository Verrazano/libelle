#ifndef SCRIPTENGINE_H_
#define SCRIPTENGINE_H_

#include <angelscript.h>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "../vec2f.h"
#include "../Entity/Entity.h"
#include <vector>
#include "../Map/Tile.h"
class GameMode;

unsigned int mrandom();
bool isServer();
bool isClient();
bool isDev(std::string);
Player* getPlayerByName(std::string);

//if z is 0 use defaultz of tiletype
void addTile(std::string name, float x, float y, float z);
std::string getTileAt(float x, float y);
void remTileAt(float x, float y);
void clearMap();
void clearTileTypes();
void addTileType(std::string name, std::string sprite, float width, float height, float defaultz);
bool hasTileType(std::string tilename);
TileType* getTileType(std::string tilename);

unsigned int playersCount();
Player* getPlayerByIndex(unsigned int i);
bool isLocalPlayer(Player* p);

vec2f getMousePos();

vec2f getMouseInView();

void setView(float x, float y, float w, float h);

sf::View* getView();
GameMode* getGameMode();

vec2f viewSize(sf::View v);

void addParticle(float x, float y, std::string texture, float size, bool collide, float velx, float vely, float lifetime);
void addParticle(float xp, float yp, std::string texture,
		unsigned int x, unsigned int y, unsigned int width, unsigned int height,
		float size, bool collide, float velx, float vely, float lifetime);

void playSound(std::string sound);
void playSound(std::string sound, float x, float y);

Entity* createEntity(std::string type, float x, float y);
Entity* createEntity(std::string type);
Entity* getEntityByID(std::string id);
void clearEntities();

unsigned int getTicksElapsed();
float getGameTime();
Player* getLocalPlayer();

void registerEngine(asIScriptEngine* engine);

void drawText(std::string text, float x, float y, unsigned int r, unsigned int g, unsigned int b);
void drawText(std::string text, float x, float y);
void drawSprite(std::string sprite, float x, float y, float f);
void drawSprite(std::string sprite, float x, float y, float f, int team);
void drawSprite(std::string sprite, float x, float y);
void drawRect(float x, float y, float w, float h, unsigned int r, unsigned int g, unsigned int b, unsigned int o);

class ScriptEngine
{
public:
	static void print(const std::string& msg);

	static ScriptEngine* getInstance();
	static asIScriptEngine* get();

	asIScriptEngine* engine;

	std::vector<std::pair<std::string, std::string> > modscript; //modulename, scriptasline

private:
	void configure();
	static void messageCallBack(const asSMessageInfo* msg);

	ScriptEngine();
	ScriptEngine(ScriptEngine const&){}
	void operator = (ScriptEngine const&){}

	static ScriptEngine* instance;

};

#endif /* SCRIPTENGINE_H_ */
