#include <iostream>
#include <assert.h>
#include <time.h>

#include "ScriptEngine.h"

#include "scriptmath/scriptmath.h"
#include "scriptstdstring/scriptstdstring.h"
#include "scriptfile/scriptfile.h"
#include "scriptdictionary/scriptdictionary.h"
#include "scriptarray/scriptarray.h"
#include "scriptany/scriptany.h"

#include "../Entity/Entity.h"
#include "../Engine/ResourceManager/ResourceManager.h"
#include "../Util/convert/convertstring.h"
#include "../vec2f.h"
#include "../Util/convert/units.h"
#include "../Entity/SpriteLayer.h"
#include "../Player.h"
#include "../Engine/Engine.h"
#include "../Util/convert/mousetoview.h"
#include "../Util/Config/Config.h"
#include "../GameMode.h"
#include "../File/File.h"
#include "../Game.h"
#include "../add_on/scripthelper/scripthelper.h"

ScriptEngine* ScriptEngine::instance;

unsigned int mrandom()
{
	return rand();

}

Player* getPlayerByName(std::string name)
{
	return Engine::get()->game->getPlayerByName(name);

}

bool isServer()
{
	return Engine::get()->isServer;

}

bool isClient()
{
	return !isServer();

}

bool isDev(std::string name)
{
	return name == "Verrazano" || name == "Rayne" || name == "Sharkrave" || name == "jshreder" || name == "porthog";

}

//if z is 0 use defaultz of tiletype
void addTile(std::string name, float x, float y, float z)
{
	if(!hasTileType(name))
		return;

	Engine::get()->map.push_back(new Tile(getTileType(name), x, y, z));

}

std::string getTileAt(float x, float y)
{
	for(unsigned int i = 0; i < Engine::get()->map.size(); i++)
	{
		if(Engine::get()->map[i]->tilesprite.getGlobalBounds().contains(sf::Vector2f(x, y)))
			return Engine::get()->map[i]->tilename;

	}

	return "";

}

void remTileAt(float x, float y)
{
	std::vector<Tile*>::iterator it;
	for(it = Engine::get()->map.begin(); it != Engine::get()->map.end(); it++)
	{
		if((*it)->tilesprite.getGlobalBounds().contains(sf::Vector2f(x, y)))
		{
			Engine::get()->map.erase(it);
			return;

		}

	}

}

void drawSprite(std::string sprite, float x, float y, float f, int team)
{
    if(isServer())
        return;
    sf::Vector2u u = Engine::get()->files.get<sf::Texture>(sprite)->getSize();
    Engine::get()->spritedrawer.setSize(sf::Vector2f(u.x*f, u.y*f));
    Engine::get()->spritedrawer.setTexture(Engine::get()->files.get<sf::Texture>(sprite));
    Engine::get()->spritedrawer.setPosition(x, y);
    Engine::get()->spritedrawer.setFillColor(sf::Color(255, 255, 255, 255));

    sf::Shader* teamshader = &Engine::get()->teamshader;
    Engine::get()->prepareTeamShader(team);
    if(teamshader->isAvailable() && team != -255)
    {
        Engine::get()->window.draw(Engine::get()->spritedrawer, teamshader);

    }
    else
    {
        Engine::get()->window.draw(Engine::get()->spritedrawer);

    }

}

void clearMap()
{
	while(!Engine::get()->map.empty())
	{
		delete (*Engine::get()->map.begin());
		Engine::get()->map.erase(Engine::get()->map.begin());

	}

	Engine::get()->map.clear();

}

void clearTileTypes()
{
	while(!Engine::get()->tiletypes.empty())
	{
		delete (*Engine::get()->tiletypes.begin());
		Engine::get()->tiletypes.erase(Engine::get()->tiletypes.begin());

	}

	Engine::get()->tiletypes.clear();

}

void addTileType(std::string name, std::string sprite, float width, float height, float defaultz)
{
	if(hasTileType(name))
		return;

	Engine::get()->tiletypes.push_back(new TileType(name, sprite, width, height, defaultz));

}

bool hasTileType(std::string tilename)
{
	for(unsigned int i = 0; i < Engine::get()->tiletypes.size(); i++)
	{
		if(Engine::get()->tiletypes[i]->tilename == tilename)
			return true;

	}

	return false;

}

//internal only don't bind
TileType* getTileType(std::string tilename)
{
	for(unsigned int i = 0; i < Engine::get()->tiletypes.size(); i++)
	{
		if(Engine::get()->tiletypes[i]->tilename == tilename)
			return Engine::get()->tiletypes[i];

	}

	return NULL;

}


unsigned int playersCount()
{
	return Engine::get()->game->playersCount();

}

Player* getPlayerByIndex(unsigned int i)
{
	return Engine::get()->game->getPlayerByIndex(i);

}

GameMode* getGameMode()
{
	return Engine::get()->gmode;

}

void addParticle(float x, float y, std::string texture, float size, bool collide, float velx, float vely, float lifetime)
{
	if(isServer())
		return;

	Particle* p = new Particle(x, y, texture, size, collide, velx, vely, lifetime);
	Engine::get()->particles.createParticle(p);

}

void addParticle(float xp, float yp, std::string texture,
		unsigned int x, unsigned int y, unsigned int width, unsigned int height,
		float size, bool collide, float velx, float vely, float lifetime)
{
	if(isServer())
		return;
	Particle* p = new Particle(xp, yp, texture, x, y, width, height, size, collide, velx, vely, lifetime);
	Engine::get()->particles.createParticle(p);

}

vec2f getMousePos()
{
	return sf::Vector2f(sf::Mouse::getPosition(Engine::get()->window));
}

vec2f getMouseInView()
{
	return mouseToView(Engine::get()->gameview, Engine::get()->window);

}

void setMousePosition(float x, float y)
{

	sf::Vector2i mouseCoords(x, y);
	sf::Mouse::setPosition(mouseCoords, Engine::get()->window);
}

void setView(float x, float y, float w, float h)
{
	if(isServer())
		return;
	Engine::get()->gameview = sf::View(sf::FloatRect(x, y, w, h));

}

sf::View* getView()
{
	return &Engine::get()->gameview;

}

vec2f viewSize(sf::View v)
{
	return v.getSize();

}

bool isLocalPlayer(Player* p)
{
	return p == getLocalPlayer();

}

void playSound(std::string sound)
{
	if(isServer())
		return; //TODO: play sound for all players
	Engine::get()->sounds.playSound(sound);

}

void playSound(std::string sound, float x, float y)
{
	if(isServer())
		return; //TODO above
	Engine::get()->sounds.playSound(sound, x, y);

}

void registerView(asIScriptEngine* engine)
{
	assert(engine->RegisterObjectType("View", sizeof(sf::View), asOBJ_REF | asOBJ_NOCOUNT) >= 0);

	assert(engine->RegisterGlobalFunction("vec2f getMouseInView()", asFUNCTION(getMouseInView), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void setView(float, float, float, float)", asFUNCTION(setView), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("View@ getView()", asFUNCTION(getView), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("vec2f viewSize(const View& in)", asFUNCTION(viewSize), asCALL_CDECL) >= 0);

}

Entity* createEntity(std::string type, float x, float y)
{
	Entity* e = Engine::get()->createEntity(type, x, y);
	Engine::get()->game->SynccreateEntity(e);
	return e;

}

Entity* createEntity(std::string type)
{
	Entity* e = Engine::get()->createEntity(type);
	Engine::get()->game->SynccreateEntity(e);
	return e;

}

Entity* getEntityByID(std::string id)
{
	return Engine::get()->getEntityByID(id);

}

void clearEntities()
{
	Engine::get()->clearEntities();

}

unsigned int getTicksElapsed()
{
	return Engine::get()->getTicksElapsed();

}

float getGameTime()
{
	return Engine::get()->getGameTime();

}

Player* getLocalPlayer()
{
	return Engine::get()->getLocalPlayer();

}

bool isMouseButtonPressed(unsigned int i)
{
	if(isServer())
		return false;
	return sf::Mouse::isButtonPressed(sf::Mouse::Button(i));

}

void registerEngine(asIScriptEngine* engine)
{

	assert(engine->RegisterGlobalFunction("void setMousePosition(float x, float y)", asFUNCTION(setMousePosition), asCALL_CDECL) >= 0);


	assert(engine->RegisterGlobalFunction("void playSound(const string& in)", asFUNCTIONPR(playSound, (std::string), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void playSound(const string& in, float, float)", asFUNCTIONPR(playSound, (std::string, float, float), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("vec2f getMousePos()", asFUNCTION(getMousePos), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Entity@ createEntity(string, float, float)", asFUNCTIONPR(createEntity, (std::string, float, float), Entity*), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Entity@ createEntity(string)", asFUNCTIONPR(createEntity, (std::string), Entity*), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Entity@ getEntityByID(string id)", asFUNCTION(getEntityByID), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void clearEntities()", asFUNCTION(clearEntities), asCALL_CDECL) >= 0);

	assert(engine->RegisterGlobalFunction("void drawText(string, float, float, uint, uint, uint)", asFUNCTIONPR(drawText, (std::string, float, float, unsigned int, unsigned int, unsigned int), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void drawText(string, float, float)", asFUNCTIONPR(drawText, (std::string, float, float), void), asCALL_CDECL) >= 0);
    assert(engine->RegisterGlobalFunction("void drawSprite(string, float, float, float, int)", asFUNCTIONPR(drawSprite, (std::string, float, float, float), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void drawSprite(string, float, float, float)", asFUNCTIONPR(drawSprite, (std::string, float, float, float), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void drawSprite(string, float, float)", asFUNCTIONPR(drawSprite, (std::string, float, float), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void drawRect(float, float, float, float, uint, uint, uint, uint)", asFUNCTIONPR(drawRect, (float, float, float, float, unsigned int, unsigned int, unsigned int, unsigned int), void), asCALL_CDECL) >= 0);

	assert(engine->RegisterGlobalFunction("uint getTicksElapsed()", asFUNCTION(getTicksElapsed), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("float getGameTime()", asFUNCTION(getGameTime), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Player@ getLocalPlayer()", asFUNCTION(getLocalPlayer), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool isMouseButtonPressed(uint)", asFUNCTION(isMouseButtonPressed), asCALL_CDECL) >=0);
	assert(engine->RegisterGlobalFunction("uint rand()", asFUNCTION(mrandom), asCALL_CDECL) >= 0);

	assert(engine->RegisterGlobalFunction("void addParticle(float, float, string, float, bool, float, float, float)", asFUNCTIONPR(addParticle,
			(float, float, std::string, float, bool, float, float, float), void), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void addParticle(float, float, string, uint, uint, uint, uint, float, bool, float, float, float)", asFUNCTIONPR(addParticle,
			(float, float, std::string, unsigned int, unsigned int, unsigned int, unsigned int, float, bool, float, float, float), void), asCALL_CDECL) >= 0);

	assert(engine->RegisterGlobalFunction("GameMode@ getGameMode()", asFUNCTION(getGameMode), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool isServer()", asFUNCTION(isServer), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool isClient()", asFUNCTION(isClient), asCALL_CDECL) >= 0);

	assert(engine->RegisterGlobalFunction("uint playersCount()", asFUNCTION(playersCount), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Player@ getPlayerByIndex(uint)", asFUNCTION(getPlayerByIndex), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool isLocalPlayer(Player@)", asFUNCTION(isLocalPlayer), asCALL_CDECL) >= 0);
	//TODO make this work client and server side


	assert(engine->RegisterGlobalFunction("void addTile(string, float, float, float)", asFUNCTION(addTile), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("string getTileAt(float, float)", asFUNCTION(getTileAt), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void remTileAt(float, float)", asFUNCTION(remTileAt), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void clearMap()", asFUNCTION(clearMap), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void clearTileTypes()", asFUNCTION(clearTileTypes), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("void addTileType(string, string, float, float, float)", asFUNCTION(addTileType), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool hasTileType(string)", asFUNCTION(hasTileType), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool isDev(string)", asFUNCTION(isDev), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("Player@ getPlayerByName(string)", asFUNCTION(getPlayerByName), asCALL_CDECL) >= 0);


}

void drawText(std::string text, float x, float y, unsigned int r, unsigned int g, unsigned int b)
{
	if(isServer())
		return;
	Engine::get()->textdrawer.setString(text);
	Engine::get()->textdrawer.setPosition((int)x, (int)y);
	Engine::get()->textdrawer.setColor(sf::Color(r, g, b));
	Engine::get()->window.draw(Engine::get()->textdrawer);

}

void drawText(std::string text, float x, float y)
{
	if(isServer())
		return;
	Engine::get()->textdrawer.setString(text);
	Engine::get()->textdrawer.setPosition((int)x, (int)y);
	Engine::get()->textdrawer.setColor(sf::Color::Black);
	Engine::get()->window.draw(Engine::get()->textdrawer);

}

void drawSprite(std::string sprite, float x, float y, float f)
{
	if(isServer())
		return;
	sf::Vector2u u = Engine::get()->files.get<sf::Texture>(sprite)->getSize();
	Engine::get()->spritedrawer.setSize(sf::Vector2f(u.x*f, u.y*f));
	Engine::get()->spritedrawer.setTexture(Engine::get()->files.get<sf::Texture>(sprite));
	Engine::get()->spritedrawer.setPosition(x, y);
	Engine::get()->spritedrawer.setFillColor(sf::Color(255, 255, 255, 255));
	Engine::get()->window.draw(Engine::get()->spritedrawer);

}

void drawSprite(std::string sprite, float x, float y)
{
	if(isServer())
		return;
	Engine::get()->spritedrawer.setSize(sf::Vector2f(Engine::get()->files.get<sf::Texture>(sprite)->getSize()));
	Engine::get()->spritedrawer.setTexture(Engine::get()->files.get<sf::Texture>(sprite));
	Engine::get()->spritedrawer.setPosition(x, y);
	Engine::get()->spritedrawer.setFillColor(sf::Color(255, 255, 255, 255));
	Engine::get()->window.draw(Engine::get()->spritedrawer);

}



void drawRect(float x, float y, float w, float h, unsigned int r, unsigned int g, unsigned int b, unsigned int o)
{
	if(isServer())
		return;
	Engine::get()->spritedrawer = sf::RectangleShape(sf::Vector2f(w, h));
	Engine::get()->spritedrawer.setPosition(x, y);
	Engine::get()->spritedrawer.setFillColor(sf::Color(r, g, b, o));
	Engine::get()->window.draw(Engine::get()->spritedrawer);

}

void ScriptEngine::print(const std::string& msg){ std::cout << msg << "\n"; }

ScriptEngine* ScriptEngine::getInstance()
{
	static bool created = false;
	if(!created)
	{
		std::cout << "creating new script engine?\n";
		created = true;
		instance = new ScriptEngine();

	}

	return instance;

}

asIScriptEngine* ScriptEngine::get()
{
	return getInstance()->engine;

}

void ScriptEngine::configure()
{
	assert(engine->SetMessageCallback(asFUNCTION(ScriptEngine::messageCallBack), 0, asCALL_CDECL) >= 0);
	assert(engine->RegisterObjectType("Entity", sizeof(Entity), asOBJ_REF | asOBJ_NOCOUNT) >= 0);
	RegisterScriptMath(engine);
	RegisterScriptArray(engine, true);
	RegisterStdString(engine);
	RegisterScriptFile(engine);
	RegisterScriptDictionary(engine);
	RegisterScriptAny(engine);
	vec2f::registerVec2f(engine);

	assert(engine->RegisterGlobalFunction("int toInt(const string& in)", asFUNCTION(toInt), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("uint toUInt(const string& in)", asFUNCTION(toUInt), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("float toFloat(const string& in)", asFUNCTION(toFloat), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("bool toBool(const string& in)", asFUNCTION(toBool), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("string toString(int)", asFUNCTIONPR(toString, (int), std::string), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("string toString(uint)", asFUNCTIONPR(toString, (unsigned int), std::string), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("string toString(float)", asFUNCTIONPR(toString, (float), std::string), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("string toString(bool)", asFUNCTIONPR(toString, (bool), std::string), asCALL_CDECL) >= 0);

	registerConvertUnits(engine);
	SpriteLayer::registerSpriteLayer(engine);
	Player::registerPlayer(engine);
	registerView(engine);

	//todo register config stuffs
	//Config::registerConfig(engine);

	assert(engine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(ScriptEngine::print), asCALL_CDECL) >= 0);
	GameMode::registerGameMode(engine);
	Entity::registerEntity(engine);
	registerEngine(engine);

	WriteConfigToFile(engine, "res/interface.txt");
/*
	File f("res/interface.txt", true);
	f.read();
	f.getFile().clear();
	f.append("OBJECTS");
	f.append("");
	unsigned int objectcount = engine->GetObjectTypeCount();
	for(unsigned int i = 0; i < objectcount; i++)
	{
		std::string s(engine->GetObjectTypeByIndex(i)->GetName());
		std::cout << s << "\n";
		f.append(s);
		for(unsigned int u = 0; u < engine->GetObjectTypeByIndex(i)->GetMethodCount(); u++)
		{
				std::string s2(engine->GetObjectTypeByIndex(i)->GetMethodByIndex(u)->GetDeclaration());
				std::cout << "\t" << s2 << "\n";
				f.append("\t"+s2);

		}

		f.append("");

		for(unsigned int u = 0; u < engine->GetObjectTypeByIndex(i)->GetPropertyCount(); u++)
		{
			std::string s2(engine->GetObjectTypeByIndex(i)->GetPropertyDeclaration(u));
			std::cout << "\t" << s2 << "\n";
			f.append("\t"+s2);

		}

		f.append("");

	}

	f.append("GLOBAL");
	f.append("");

	for(unsigned int i = 0; i < engine->GetGlobalFunctionCount(); i++)
	{
		std::string s(engine->GetGlobalFunctionByIndex(i)->GetDeclaration());

	}

	f.write();*/


}

void ScriptEngine::messageCallBack(const asSMessageInfo* msg)
{
	if(true)//surpress errors
		return;
	const char *type = "ERR ";
	if( msg->type == asMSGTYPE_WARNING )
		type = "WARN";
	else if( msg->type == asMSGTYPE_INFORMATION )
		type = "INFO";
	std::string message = msg->message;
	if(message.find("truncated") != -1)
		return;

	std::string s = "an error occurred on line: " + toString(msg->row) + "\n\tof file: " + std::string(msg->section) + " . . . \n\t" + std::string(msg->message) +"\n";
	Engine::get()->append(s);
	//std::cout << s << "\n";

}


ScriptEngine::ScriptEngine()
{
	engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
	configure();

}
