#include "ClockHUD.h"


#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include <SFML/Graphics.hpp>
#include "FrameClock.h"


    ClockHUD::ClockHUD(){}
    ClockHUD::ClockHUD(sfx::FrameClock& clock, sf::Font& font)
        : m_clock (&clock)
        , m_font  (&font)
    {}

    void ClockHUD::draw(sf::RenderTarget& rt, sf::RenderStates states) const
    {
        // Gather the available frame time statistics.
        const Stats_t stats = build();

        sf::Text elem;
        elem.setFont(*m_font);
        elem.setCharacterSize(10);
        elem.setPosition(5.0f, 5.0f);

        // Draw the available frame time statistics.
        for (std::size_t i = 0; i < stats.size(); ++i)
        {
            elem.setString(stats[i].str);
            elem.setColor(stats[i].color);

            rt.draw(elem, states);

            // Next line.
            elem.move(0.0f, 16.0f);
        }
    }


    std::string ClockHUD::format(std::string name, std::string resolution, float value)
    {
        std::ostringstream os;
        os.precision(4);
        os << std::left << std::setw(5);
        os << name << " : ";
        os << std::setw(5);
        os << value << " " << resolution;
        return os.str();
    }

    ClockHUD::Stats_t ClockHUD::build() const
    {
        const int count = 3;
        const Stat stats[count] = {
            { sf::Color::Red, format("Time",  "(sec)", m_clock->getTotalFrameTime().asSeconds())        },
            { sf::Color::Red,  format("Frame", "",      m_clock->getTotalFrameCount())                   },
            { sf::Color::Red,  format("FPS",   "",      (unsigned int)m_clock->getFramesPerSecond())	}/*,
            { sf::Color::Red,  format("min.",  "",      m_clock->getMinFramesPerSecond())                },
            { sf::Color::Red,  format("avg.",  "",      m_clock->getAverageFramesPerSecond())            },
            { sf::Color::Red,  format("max.",  "",      m_clock->getMaxFramesPerSecond())                },
            { sf::Color::Red,   format("Delta", "(ms)",  m_clock->getLastFrameTime().asMilliseconds())    },
            { sf::Color::Red,   format("min.",  "(ms)",  m_clock->getMinFrameTime().asMilliseconds())     },
            { sf::Color::Red,   format("avg.",  "(ms)",  m_clock->getAverageFrameTime().asMilliseconds()) },
            { sf::Color::Red,   format("max.",  "(ms)",  m_clock->getMaxtFrameTime().asMilliseconds())    }*/
        };
        return Stats_t(&stats[0], &stats[0] + count);
    }

