#ifndef CLOCKHUD
#define CLOCKHUD

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include <SFML/Graphics.hpp>
#include "FrameClock.h"

class ClockHUD : public sf::Drawable
{
    struct Stat
    {
        sf::Color color;
        std::string str;
    };

    typedef std::vector<Stat> Stats_t;

public:
    ClockHUD();
    ClockHUD(sfx::FrameClock& clock, sf::Font& font);

private:

    void draw(sf::RenderTarget& rt, sf::RenderStates states) const;

private:

    static std::string format(std::string name, std::string resolution, float value);

    Stats_t build() const;
private:

    sf::Font* m_font;
    sfx::FrameClock* m_clock;

};

#endif /*CLOCKHUD*/
