/*==============================*/
//Author: Harrison M
//Date 11/23/12
//Description: vec2f holds x, and y values, for lines and points.
/*==============================*/

#include "vec2f.h"
#include <iostream>
#include <math.h>
#include <assert.h>

void vec2f::registerVec2f(asIScriptEngine* engine)
{
	assert(engine->RegisterObjectType("vec2f", sizeof(vec2f), asOBJ_VALUE | asOBJ_APP_CLASS_CDAK) >= 0);
	assert(engine->RegisterObjectBehaviour("vec2f", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(vec2fConstruct), asCALL_GENERIC) >= 0);
	assert(engine->RegisterObjectBehaviour("vec2f", asBEHAVE_CONSTRUCT, "void f(const vec2f& in)", asFUNCTION(vec2fCopyConstruct), asCALL_GENERIC) >= 0);
	assert(engine->RegisterObjectBehaviour("vec2f", asBEHAVE_CONSTRUCT, "void f(float, float)", asFUNCTION(vec2fParamConstruct), asCALL_GENERIC) >= 0);
	assert(engine->RegisterObjectBehaviour("vec2f", asBEHAVE_DESTRUCT,  "void f()", asFUNCTION(vec2fDestruct), asCALL_GENERIC) >= 0);

	assert(engine->RegisterObjectProperty("vec2f", "float x", asOFFSET(vec2f, x)) >= 0);
	assert(engine->RegisterObjectProperty("vec2f", "float y", asOFFSET(vec2f, y)) >= 0);

	//register operators later

}

void vec2f::vec2fConstruct(asIScriptGeneric * gen)
{
	new (gen->GetObject()) vec2f;

}

void vec2f::vec2fCopyConstruct(asIScriptGeneric * gen)
{
	vec2f* a = static_cast<vec2f*>(gen->GetArgObject(0));
	new (gen->GetObject()) vec2f(*a);

}

void vec2f::vec2fParamConstruct(asIScriptGeneric * gen)
{
	float x = *static_cast<float*>(gen->GetArgObject(0));
	float y = *static_cast<float*>(gen->GetArgObject(1));
	new (gen->GetObject()) vec2f(x, y);

}

void vec2f::vec2fDestruct(asIScriptGeneric * gen)
{
	  vec2f* ptr = static_cast<vec2f*>(gen->GetObject());
	  ptr->~vec2f();

}

//constructors for vec2f

//default constructor sets x and y to 0
vec2f::vec2f()
{
	x = 0;
	y = 0;

}

//creates a copy of const vec2f& copy
vec2f::vec2f(const vec2f& copy)
{
	x = copy.x;
	y = copy.y;

}

vec2f::vec2f(sf::Vector2f copy)
{
	x = copy.x;
	y = copy.y;

}

vec2f::vec2f(b2Vec2 copy)
{
	x = copy.x;
	y = copy.y;

}

//sets x and y to nx and ny
vec2f::vec2f(float nx, float ny)
{
	x = nx;
	y = ny;

}

//operators

//check for equality
bool vec2f::operator ==(vec2f a)
{
	return (x == a.x && y == a.y);

}

//check for inequality
bool vec2f::operator !=(vec2f a)
{
	return (x != a.x || y != a.y);

}

//set this to a
vec2f vec2f::operator =(vec2f a)
{
	x = a.x;
	y = a.y;
	return *this;

}

//returns the added vec2f but doesn't change this
vec2f vec2f::operator +(vec2f a)
{
	return vec2f(x + a.x, y + a.y);

}

//returns the subtracted vec2f but doesn't change this
vec2f vec2f::operator -(vec2f a)
{
	return vec2f(x - a.x, y - a.y);

}

//adds a to this then returns this
vec2f vec2f::operator +=(vec2f a)
{
	x += a.x;
	y += a.y;
	return *this;

}

//subtracts a from this then returns this
vec2f vec2f::operator -=(vec2f a)
{
	x -= a.x;
	y -= a.y;
	return *this;

}

vec2f vec2f::operator /(float n)
{
	vec2f temp(*this);
	return temp /= n;

}

vec2f vec2f::operator /=(float n)
{
	x /= n;
	y /= n;
	return *this;

}

//returns the dot product of this and a
float vec2f::operator *(vec2f a)
{
	return x*a.x + y*a.y;

}

//vector math functions

//gets the length of the vector
float vec2f::length()
{
	return (float)sqrt(static_cast<double>(*this * *this));

}

float vec2f::lengthSquared()
{
	return (float)(*this * *this);

}

//normalizes this vector
void vec2f::normalize()
{
	*this/=length();

}

//returns the normalized vector
vec2f vec2f::normalized()
{
	return *this/length();

}

//projects this vector onto a

//TODO: implement project, projected, reflect
void vec2f::project(vec2f a)
{
	//*this = a * (vec2f(x, y) * a)/(a * a);

}

//returns the vector projected onto a
vec2f vec2f::projected(vec2f a)
{
	//return a * (vec2f(x, y) * a)/(a * a);
	return vec2f();
}

//reflects this vector about a
void vec2f::reflect(vec2f a)
{
	/*vec2f orig(*(this));
	project(a);
	vec2f b(*(this) * orig-2);
	x = b.x;
	y = b.y;*/

}

//external vector math functions

//returns the line that a and b make
vec2f line(vec2f a, vec2f b)
{
	return vec2f(b.x - a.x, b.y - a.y);

}

//returns the normal of line a
vec2f normal(vec2f a)
{
	return vec2f(-a.y, a.x);

}

//returns the distance between points a and b
float distance(vec2f a, vec2f b)
{
	return (float)sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2));

}

//gets the angle between a and b
float angle(vec2f a, vec2f b)
{
	return (float)acosf((a*b)/(a.length()*b.length()));

}

//returns true if lines a and b are parallel
bool parallel(vec2f a, vec2f b)
{
	//TODO: find out how to arbitrarily check if two lines are parallel
	return false;
}

//prints the x and y of vec2f a
void print(vec2f a)
{
	std::cout << "vector: x:" << a.x << " y: " << a.y << "\n";

}
