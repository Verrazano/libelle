#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <SFML/Graphics.hpp>
#include "../Engine/ResourceManager/ResourceManager.h"
#include "../Engine/Engine.h"

class Console
{
public:
	Console();
	Console(sf::RenderWindow* win, sf::Vector2f size, sf::Vector2f pos, unsigned int max_lines);

	void append(std::string s);
	bool update(sf::Event& event);

	std::string getInput();
	void draw();

private:
	std::string temp_input;
	std::string input;
	sf::RenderWindow* win;

	void setupBacking(sf::Vector2f size, sf::Vector2f pos);
	sf::RectangleShape backing;

	unsigned int max_lines;

	void updateLines();
	std::vector<sf::Text> lines;

};

#endif /* CONSOLE_H_ */
