#include "Console.h"

Console::Console()
{
	win = NULL;
	max_lines = 0;
	setupBacking(sf::Vector2f(0, 0), sf::Vector2f(0, 0));

}

Console::Console(sf::RenderWindow* win, sf::Vector2f size, sf::Vector2f pos, unsigned int max_lines)
{
	this->win = win;
	setupBacking(size, pos);
	this->max_lines = max_lines;

}

void Console::append(std::string s)
{
	sf::Text temp(s, *Engine::get()->files.get<sf::Font>("consolas.ttf"), 10);
	temp.setPosition(backing.getPosition().x, backing.getPosition().y + (lines.size()*10));
	lines.push_back(temp);
	updateLines();

}

bool Console::update(sf::Event& event)
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		if(temp_input != "")
		{
			input = temp_input;
			temp_input = "";
			append(input);
			return true;

		}

	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
	{
		temp_input = temp_input.substr(0, temp_input.size()-1);
		sf::sleep(sf::milliseconds(100));

	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		temp_input = input;

	}
	else if(event.type == sf::Event::TextEntered
			&& !sf::Keyboard::isKeyPressed(sf::Keyboard::Home) && event.text.unicode != 8)
	{
		temp_input += sf::String(event.text.unicode);
		sf::sleep(sf::milliseconds(100));

	}

	return false;

}

std::string Console::getInput()
{
	return input;

}

void Console::draw()
{
	win->draw(backing);
	for(unsigned int i = 0; i < lines.size(); i++)
		win->draw(lines[i]);

	sf::Text inputText("> " + temp_input, *Engine::get()->files.get<sf::Font>("consolas.ttf"), 10);
	inputText.setPosition(backing.getPosition().x, backing.getPosition().y + (max_lines*10));

	win->draw(inputText);

}

void Console::setupBacking(sf::Vector2f size, sf::Vector2f pos)
{
	backing.setSize(size);
	backing.setPosition(pos);

	backing.setFillColor(sf::Color(80, 80, 80, 180));

}

void Console::updateLines()
{
	if(lines.size() > max_lines)
	{
		lines.erase(lines.begin());
		for(unsigned int i = 0; i < lines.size(); i++)
		{
			lines[i].setPosition(backing.getPosition().x, backing.getPosition().y + (i*10));

		}

	}

}
