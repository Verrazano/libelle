#ifndef LIBELLE_H_
#define LIBELLE_H_

/* TODO: Should this be defined here, or somewhere else? */
/* #define NOLAUNCHER_AVAILABLE */

int libelle(int argc, char* argv[]);

/*

When you comment your code you save children from being eaten by platypi.
Last year 65,535 children were saved how many children will you save?

      .--..-'''-''''-._
  ___/%   ) )      \ i-;;,_
((:___/--/ /--------\ ) `'-'
         ""          ""

 */

#endif /* LIBELLE_H_ */
