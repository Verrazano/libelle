//timer.h
#ifndef TIMER_H
#define TIMER_H

#include <ctime>

class timer
{
public:
	timer();
	void reset();
	clock_t elapsed();

private:
	clock_t m_start;

};

#endif /*TIMER_H*/