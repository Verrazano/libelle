//timer.cpp

#include "timer.h"

timer::timer()
{
	reset();

}

void timer::reset()
{
	m_start = clock();

}

clock_t timer::elapsed()
{
	return clock() -  m_start;

}