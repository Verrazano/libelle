#ifndef CONFIGITEM_H_
#define CONFIGITEM_H_

#include <string>
#include <vector>

class Config;

class ConfigItem
{
public:
	ConfigItem();
	ConfigItem(std::string key, std::string type, std::string line,
			std::string value, std::vector<std::string> params);

	std::string getKey();
	std::string getType();
	std::string getLine();
	std::string getValue();
	std::vector<std::string> getParams();
	unsigned int getNumOfParams();
	std::string getParam(unsigned int index);

	int valueAsInt();
	unsigned int valueAsUInt();
	float valueAsFloat();
	bool valueAsBool();

	int paramAsInt(unsigned int index);
	unsigned int paramAsUInt(unsigned int index);
	float paramAsFloat(unsigned int index);
	bool paramAsBool(unsigned int index);

private:
	std::string m_key;
	std::string m_type;
	std::string m_line;
	std::string m_value;
	std::vector<std::string> m_params;

	friend Config;

};

#endif /* CONFIGITEM_H_ */
