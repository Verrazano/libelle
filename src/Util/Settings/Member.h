#ifndef MEMBER_H_
#define MEMBER_H_

#include <string>
#include <sstream>

#include "MemberBase.h"

template <typename T>
class Member : public MemberBase
{
public:
	Member(std::string name, size_t offset, T value, std::string type);
	virtual ~Member();

	virtual std::string getType();
	virtual std::string getValue(char* base);

	void set(char* base, void* value);

protected:
	T m_value;
	std::string m_type;

};


class BoolMember : public Member<bool>
{
public:
	BoolMember(std::string name, size_t offset, bool value, std::string type);

	virtual ~BoolMember();

	virtual std::string getValue(char* base);

};

#include "Member.inl"


#endif /* MEMBER_H_ */
