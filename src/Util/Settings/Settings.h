#ifndef SETTINGS_H_
#define SETTINGS_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include "../Config/Config.h"

#include <list>
#include <string>
#include <stddef.h>
#include <stdint.h>

#include "MemberBase.h"

/**Settings class.
 * Uses the Config class to read and write a config using the
 * members of a class.
 */
class Settings
{
public:
	/**Settings constructor.
	 */
	Settings();

	/**Settings deconstructor.
	 */
	virtual ~Settings();

	/**Reads the file as a config and sets the value of all registered class members.
	 * @param file the file you want parsed as a string. Use Config::load.
	 * @return true if the read was successful, otherwise false.
	 */
	bool read(std::string file);

	bool read(Config& settings);

	/**Writes the file to the specified path.
	 * @param the path to the file you want to write a config too.
	 * @return true if the write was successful, otherwise false.
	 */
	bool write(std::string path);

	/**Register a int member of the class.
	 * @param name the name of the int member as a string.
	 * @param offset the byte offset of the int member. Use offsetof.
	 * @param value the default value of the int.
	 */
	void add(std::string name, size_t offset, int32_t value);

	/**Register a uint member of the class.
	 * @param name the name of the uint member as a string.
	 * @param offset the byte offset of the uint member. Use offsetof.
	 * @param value the default value of the uint.
	 */
	void add(std::string name, size_t offset, uint32_t value);

	/**Register a float member of the class.
	 * @param name the name of the float member as a string.
	 * @param offset the byte offset of the float member. Use offsetof.
	 * @param value the default value of the float.
	 */
	void add(std::string name, size_t offset, float value);

	/**Register a bool member of the class.
	 * @param name the name of the bool member as a string.
	 * @param offset the byte offset of the bool member. Use offsetof.
	 * @param value the default value of the bool.
	 */
	void add(std::string name, size_t offset, bool value);

	/**Register a string member of the class.
	 * @param name the name of the string member as a string.
	 * @param offset the byte offset of the string member. Use offsetof.
	 * @param value the default value of the string.
	 */
	void add(std::string name, size_t offset, std::string value);

	/**Register a string vector member of the class.
	 * @param name the name of the string vector member as a string.
	 * @param offset the byte offset of the string vector member. Use offsetof.
	 * @param value the default value of the string.
	 */
	void add(std::string name, size_t offset, std::vector<std::string> value);

private:
	std::list<MemberBase*> m_members; ///< A list of the registered members of the class.

};

#endif /* SETTINGS_H_ */
