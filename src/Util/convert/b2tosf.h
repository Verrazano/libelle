#ifndef B2TOSF_H_
#define B2TOSF_H_

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

const float pixelsInAMeter = 32.f;

float toMeters(float pixels);
float toPixels(float meters);

b2Vec2 toWorld(sf::Vector2f pos);
sf::Vector2f toScreen(b2Vec2 pos);

#endif /* B2TOSF_H_ */
