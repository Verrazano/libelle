#include "units.h"

void registerConvertUnits(asIScriptEngine* engine)
{
	assert(engine->RegisterGlobalFunction("float toRadians(float)", asFUNCTION(toRadians), asCALL_CDECL) >= 0);
	assert(engine->RegisterGlobalFunction("float toDegrees(float)", asFUNCTION(toDegrees), asCALL_CDECL) >= 0);

}


float toRadians(float a)
{
	return (a*M_PI/180);

}

float toDegrees(float a)
{
	return (a*180/M_PI);

}
