#include "b2tosf.h"

float toMeters(float pixels){ return pixels/pixelsInAMeter; }
float toPixels(float meters){ return meters*pixelsInAMeter; }

b2Vec2 toWorld(sf::Vector2f pos){ return b2Vec2(toMeters(pos.x), -toMeters(pos.y)); }
sf::Vector2f toScreen(b2Vec2 pos){ return sf::Vector2f(toPixels(pos.x), -toPixels(pos.y)); }
