#ifndef UNITS_H_
#define UNITS_H_

#include <angelscript.h>
//#include <math.h>
#include <assert.h>
#include <iostream>

#define M_PI		3.14159265358979323846

void registerConvertUnits(asIScriptEngine* engine);

float toRadians(float a);
float toDegrees(float a);


#endif /* UNITS_H_ */
