#ifndef BSLIB_H
#define BSLIB_H

#include <string>

unsigned int findFunct(std::string functName, std::string line);
std::string getFunctStr(unsigned int pos, std::string line);
std::string getVal(unsigned int pos, std::string line);
bool isFunctOnLine(std::string functName, std::string line);

//uses ()
float findFloatVal(std::string functName, std::string line);
int findIntVal(std::string functName, std::string line);
std::string findStringVal(std::string functName, std::string line);
bool findBoolVal(std::string functName, std::string line);

//uses []
float findFloatParam(std::string paramName, std::string line);
int findIntParam(std::string functName, std::string line);
std::string findStringParam(std::string functName, std::string line);
bool findBoolParam(std::string functName, std::string line);

#endif /*BSLIB_H*/
