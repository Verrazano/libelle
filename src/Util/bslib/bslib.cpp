#include "bslib.h"
#include "../convert/convertstring.h"

unsigned int findFunct(std::string functName, std::string line){
	if(line.find(functName, 0) < line.size()){
		return line.find(functName, 0)+functName.length();
	}

	return line.length() + 10;
}

std::string getFunctStr(unsigned int pos, std::string line){
	return line.substr(line.find("(", pos)+1, line.find(")", pos)-(line.find("(", pos)+1));
}

std::string getVal(unsigned int pos, std::string line){
	return line.substr(line.find("[", pos)+1, line.find("]", pos)-(line.find("[", pos)+1));
}

bool isFunctOnLine(std::string functName, std::string line)
{
	return findFunct(functName, line) < line.length()+1;
	
}

//uses ()
float findFloatVal(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return 0;

	return toFloat(getFunctStr(findFunct(functName, line), line));

}

int findIntVal(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return 0;

	return toInt(getFunctStr(findFunct(functName, line), line));

}

std::string findStringVal(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return "";

	return getFunctStr(findFunct(functName, line), line);

}

bool findBoolVal(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return false;

	std::string boolStr = findStringVal(functName, line);

	if(boolStr == "true" || boolStr == "True" || boolStr == "TRUE")
		return true;

	return false;

}


//uses []
float findFloatParam(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return 0;

	return toFloat(getVal(findFunct(functName, line), line));

}

int findIntParam(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return 0;

	return toInt(getVal(findFunct(functName, line), line));

}

std::string findStringParam(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return "";

	return getVal(findFunct(functName, line), line);

}

bool findBoolParam(std::string functName, std::string line)
{
	if(!isFunctOnLine(functName, line))
		return false;

	std::string boolStr = findStringParam(functName, line);

	if(boolStr == "true" || boolStr == "True" || boolStr == "TRUE")
		return true;

	return false;

}
