#include "Player.h"
#include <assert.h>
#include "Entity/Entity.h"
#include "Engine/Engine.h"

void Player::registerPlayer(asIScriptEngine* engine)
{
	assert(engine->RegisterObjectType("Player", sizeof(Player), asOBJ_REF | asOBJ_NOCOUNT) >= 0);

	assert(engine->RegisterObjectMethod("Player", "bool isKeyPressed(uint)", asMETHOD(Player, isKeyPressed), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "bool fireKeyPressed()", asMETHOD(Player, fireKeyPressed), asCALL_THISCALL) >= 0);
	//Joystick button input
	assert(engine->RegisterObjectMethod("Player", "bool isJoystickButtonPressed(uint)", asMETHOD(Player, isJoystickButtonPressed), asCALL_THISCALL) >= 0);
	//Joystick connection
	assert(engine->RegisterObjectMethod("Player", "bool isJoystickConnected(uint)", asMETHOD(Player, isJoystickConnected), asCALL_THISCALL) >= 0);
	//Set joystic controls
	assert(engine->RegisterObjectMethod("Player", "void setJoystickControlsEnabled(bool)", asMETHOD(Player, setJoystickDefault), asCALL_THISCALL) >= 0);


	assert(engine->RegisterObjectProperty("Player", "string name", asOFFSET(Player, name)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint up", asOFFSET(Player, up)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint down", asOFFSET(Player, down)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint left", asOFFSET(Player, left)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint right", asOFFSET(Player, right)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint fire", asOFFSET(Player, fire)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint reload", asOFFSET(Player, reload)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint nextweapon", asOFFSET(Player, nextweapon)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint prevweapon", asOFFSET(Player, prevweapon)) >= 0);
	assert(engine->RegisterObjectProperty("Player", "uint action", asOFFSET(Player, action)) >= 0);

	assert(engine->RegisterObjectMethod("Player", "Entity@ getEntity()", asMETHOD(Player, getEntity), asCALL_THISCALL) >= 0);


	//VarManager
	assert(engine->RegisterObjectMethod("Player", "void setInt(string, int)", asMETHOD(Player, setInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "int getInt(string)", asMETHOD(Player, getInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Player", "void setUInt(string, uint)", asMETHOD(Player, setUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "uint getUInt(string)", asMETHOD(Player, getUInt), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Player", "void setFloat(string, float)", asMETHOD(Player, setFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "float getFloat(string)", asMETHOD(Player, getFloat), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Player", "void setString(string, string)", asMETHOD(Player, setString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "string getString(string)", asMETHOD(Player, getString), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Player", "void setBool(string, bool)", asMETHOD(Player, setBool), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "bool getBool(string)", asMETHOD(Player, getBool), asCALL_THISCALL) >= 0);

	assert(engine->RegisterObjectMethod("Player", "void syncInt(string)", asMETHOD(Player, syncInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "void syncUInt(string)", asMETHOD(Player, syncUInt), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "void syncFloat(string)", asMETHOD(Player, syncFloat), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "void syncString(string)", asMETHOD(Player, syncString), asCALL_THISCALL) >= 0);
	assert(engine->RegisterObjectMethod("Player", "void syncBool(string)", asMETHOD(Player, syncBool), asCALL_THISCALL) >= 0);

}


bool Player::isKeyPressed(unsigned int key)
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Key(key)) && Engine::get()->hasFocus;

}

bool Player::isJoystickButtonPressed(unsigned int key)
{
	if(key < 100)
	{
		return	sf::Joystick::isButtonPressed(1, key);
	}

	switch(key)
	{
		case(101):
		if(controller.leftStickY() > .45) // Make that .45 configurable
		{
			return true;
		}
		break;

		case(102):
		if(controller.leftStickY() < -.45)
		{
			return true;
		}
		break;

		case(103):
		if(controller.leftStickX() < -.45)
		{
			return true;
		}
		break;

		case(104):
		if(controller.leftStickX() > .45)
		{
			return true;
		}
		break;

		case(105):
		return controller.rightTrigger();
		break;

		//This is bad
		//This is all just for the second joystick. Which is not set up in the player control stuff

		// Up on the right stick
		case(106):
			if(controller.rightStickY() > .45)
			{
				return true;
			}
		break;

		// Down on the right stick
		case(107):
			if(controller.rightStickY() < -.45)
			{
				return true;
			}
		break;

		// Right on the right stick
		case(108):
			if(controller.rightStickX() > .45)
			{
				return true;
			}
		break;

		// Left on the right stick
		case(109):
		if(controller.rightStickX() < -.45)
		{
			return true;
		}
		break;

	}

	return false;
}

bool Player::fireKeyPressed()
{
	return (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(fire)) || sf::Mouse::isButtonPressed(sf::Mouse::Left)) && Engine::get()->hasFocus;

}

bool Player::isJoystickConnected(unsigned int com)
{
	if(sf::Joystick::isConnected(com))
		return true;

	return false;
}

Player::Player(std::string name)
{
	this->name = name;

	setKeysDefault();
	//setJoystickDefault();
}

void Player::setKeys(unsigned int up, unsigned int down,
		unsigned int left, unsigned int right,
		unsigned int fire1, unsigned int reload,
		unsigned int nextweapon, unsigned int prevweapon,
		unsigned int action,
		bool useClickFire, bool useScrollSwitch)
{
	this->up = up;
	this->down = down;
	this->left = left;
	this->right = right;
	this->fire = fire1;
	this->reload = reload;
	this->nextweapon = nextweapon;
	this->prevweapon = prevweapon;
	this->action = action;
	this->useClickFire = useClickFire;
	this->useScrollSwitch = useScrollSwitch;

}

void Player::setKeysDefault()
{
	setKeys(sf::Keyboard::W, sf::Keyboard::S,
			sf::Keyboard::A, sf::Keyboard::D,
			sf::Keyboard::L, sf::Keyboard::R,
			sf::Keyboard::C, sf::Keyboard::Z,
			sf::Keyboard::E, true, true);

}

void Player::setJoystickDefault(bool controls)
{
	if(controls)
	{
		setKeys(102, 101,
			103, 104,
			105, 2,
			5, 4,
			0, false, true);
	}
	else
	{
		setKeysDefault();
	}
}

Entity* Player::getEntity()
{
	return e;

}
