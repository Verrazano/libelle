#ifndef SERVER_H_
#define SERVER_H_

#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <enet/enet.h>
#include <vector>

//WARNING YOU MUST CALL enet_initialize() before calling start()

/* A simple threaded enet server that can send and receive
 * using sfml packets. There are hooks that can be overwritten
 * to control the behavior of the server.
 */
class Server
{
private:
  bool running;

  ENetHost* enetServer;
  ENetAddress address;
  ENetEvent event;

protected:
  //number of clients that are allowed to be connected
  unsigned int max_clients;
  unsigned int channels;

  unsigned int port;

  bool basicOut;

  //a list of all the clients that are currently connected to the server
  std::vector<ENetPeer*> clients;

public:
  Server();

  /* the port to listen from, the number of clients allowed, and the number
   * of channels available on the server.
   */
  Server(unsigned int port, unsigned int max_clients,
      unsigned int channels = 1, bool basicOut = false);

  virtual ~Server();

  /*launches the server thread*/
  void start();
  void run();

private:
  /* sets up all the enet variables and triggers the hook
   * onInit()
   */
  void init();

  /* runs the server loop, accepting connections
   * disconnecting old clients and receiving packets
   * triggers all related hooks: onReceive, onDisconnect, onConnect, onOut
   */

public:
  /* stops the server thread and triggers onStop() */
  void stop();

  bool isRunning();

  //setters are disabled while the server is running
  unsigned int getPort();
  void setPort(unsigned int port);

  unsigned int getNumOfChannels();
  void setNumOfChannels(unsigned int channels);

  unsigned int getMaxClients();
  void setMaxClients(unsigned int max_clients);

  std::string getServerIP();

  bool isBasicOutOn();
  void setBasicOut(bool enable);

  unsigned int getClientsConnected();
  ENetPeer* getClientByIndex(unsigned int index);

  //returns -1 if the client didn't exist
  int getIndexOf(ENetPeer* client);

  //broadcasts to all clients
  void broadcast(sf::Packet pack, unsigned int channel = 0,
      bool reliable = false);

  //broadcast to all clients except ENetPeer& exclude
  void broadcastExcluding(sf::Packet pack, ENetPeer* exclude,
      unsigned int channel = 0, bool reliable = false);

  //send a packet to client
  void send(sf::Packet pack, ENetPeer* client,
      unsigned int channel = 0, bool reliable = false);

  void out(std::string s);

  //disconnect client from the server
  void disconnect(ENetPeer* client);
  void disconnectAll();

  //returns true if both peer1 and peer2 have the same data
  bool sameENetPeerData(ENetPeer* peer1, ENetPeer* peer2);

protected:
  void cleanUp();

  virtual void onInit() = 0;
  virtual void onConnect(ENetPeer* client) = 0;
  virtual void onDisconnect(ENetPeer* client) = 0;
  virtual void onReceive(sf::Packet pack, ENetPeer* client) = 0;
  virtual void onOut(std::string s) = 0;
  virtual void onStop() = 0;

};

#endif /* SERVER_H_ */
