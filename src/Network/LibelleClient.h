//TODO: replace with license

/*====================*/
// LibelleClient.h
// @author Harrison D. Miller
// @version 0.1 May 15, 2013
/*====================*/

#ifndef LIBELLECLIENT_H_
#define LIBELLECLIENT_H_

#include "../Network/Client.h"
#include "Common/RequestHandler.h"
#include "../File/File.h"
#include <functional>
#include <vector>

//TODO singleton class to initialize enet and deinitialize it

class LibelleClient : public Client
{
private:
  RequestHandler clientRequestHandler;

  std::function<void(ENetPeer*)> connectHandler;
  std::function<void(ENetPeer*)> disconnectHandler;

public:
  LibelleClient();
  LibelleClient(unsigned int port, std::string serverIP = "localhost");

  ~LibelleClient();

  void addPacker(std::string requestName, RequestPacker packer);
  void addUnpacker(std::string requestName, RequestUnpacker unpacker);

  void remPacker(std::string requestName);
  void remUnpacker(std::string requestName);

  void request(std::string requestName);

  ENetPeer* getServerPeer();

  void setConnectHandler(std::function<void(ENetPeer*)> handler);
  void setDisconnectedHandler(std::function<void(ENetPeer*)> handler);

private:
  void requestManager(sf::Packet pack);

protected:
  void onInit();
  void onConnect();
  void onConnectionFailed();
  void onDisconnect();
  void onReceive(sf::Packet pack);
  void onOut(std::string s);
  void onStop();

};

#endif /* LIBELLECLIENT_H_ */
