//TODO: replace with license

/*====================*/
// LibelleServer.cpp
// @author Harrison D. Miller
// @version 0.1 May 17, 2013
/*====================*/

#include "LibelleServer.h"
#include <iostream>
#include "../Util/convert/convertstring.h"

LibelleServer::LibelleServer() : Server()
{
  setName("LibelleServer");
  setDescription("default libelle server");

  registerHandlers();
  //setBasicOut(true);

}

LibelleServer::LibelleServer(unsigned int port, unsigned max_clients,
    std::string name, std::string description)
: Server(port, max_clients, 64, false)
{
  setName(name);
  setDescription(description);

  registerHandlers();
  //setBasicOut(true);

}

LibelleServer::~LibelleServer()
{

}

void LibelleServer::addPacker(std::string requestName, RequestPacker packer)
{
  serverRequestHandler.addPacker(requestName, packer);

}

void LibelleServer::addUnpacker(std::string requestName, RequestUnpacker unpacker)
{
  serverRequestHandler.addUnpacker(requestName, unpacker);

}

void LibelleServer::remPacker(std::string requestName)
{
  serverRequestHandler.remPacker(requestName);

}

void LibelleServer::remUnpacker(std::string requestName)
{
   serverRequestHandler.remUnacker(requestName);

}

std::string LibelleServer::getName(){ return name; }
void LibelleServer::setName(std::string name){ this->name = name; }

std::string LibelleServer::getDescription(){ return description; }
void LibelleServer::setDescription(std::string description){ this->description = description; }

void LibelleServer::broadcastRequest(std::string requestName, unsigned int channel,
    bool reliable)
{
  sf::Packet pack;
  pack << "request" << requestName;
  broadcast(pack, channel, reliable);

}

void LibelleServer::broadcastRequestExclude(std::string requestName, ENetPeer* exclude,
    unsigned int channel, bool reliable)
{
  for(unsigned int i = 0; i < getClientsConnected(); i++)
    if(!sameENetPeerData(exclude, getClientByIndex(i)))
      request(requestName, getClientByIndex(i), channel, reliable);

}

void LibelleServer::request(std::string requestName, ENetPeer* client,
    unsigned int channel, bool reliable)
{
  sf::Packet pack;
  pack << "request" << requestName;
  send(pack, client, channel, reliable);

}

void LibelleServer::update()
{

}

void LibelleServer::setConnectHandler(std::function<void(ENetPeer*)> handler)
{
  connectHandler = handler;

}

void LibelleServer::setDisconnectedHandler(std::function<void(ENetPeer*)> handler)
{
  disconnectHandler = handler;

}

void LibelleServer::registerHandlers()
{

}

void LibelleServer::requestManager(sf::Packet pack, ENetPeer* client)
{
  std::string cmd;
  pack >> cmd;
  if(cmd == "request")
  {
    std::string request;
    pack >> request;

    out("client requested: " + request);

    if(!serverRequestHandler.hasPacker(request))
      return;

    sf::Packet rPack = serverRequestHandler.pack(request, pack, client);
    out("answering request");
    send(rPack, client, 1, true);

  }
  else if(serverRequestHandler.hasUnpacker(cmd))
  {
    serverRequestHandler.unpack(cmd, pack, client);

  }

}

void LibelleServer::onInit()
{
	out("\n\nserver started on " + getServerIP() + ":" + toString(getPort()));

}

void LibelleServer::onConnect(ENetPeer* client)
{
  out("a client connected to the server");
  connectHandler(client);


}

void LibelleServer::onDisconnect(ENetPeer* client)
{
  out("a client disconnected from the server");
  disconnectHandler(client);

}

void LibelleServer::onReceive(sf::Packet pack, ENetPeer* client)
{
  requestManager(pack, client);

}

void LibelleServer::onOut(std::string s)
{
	std::cout << s << "\n";

}

void LibelleServer::onStop()
{

}
