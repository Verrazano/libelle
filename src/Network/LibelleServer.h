//TODO: replace with license

/*====================*/
// LibelleServer.h
// @author Harrison D. Miller
// @version 0.1 May 15, 2013
/*====================*/

#ifndef LIBELLESERVER_H_
#define LIBELLESERVER_H_

#include "Server.h"
#include "Common/RequestHandler.h"
#include <functional>

class LibelleServer : public Server
{
private:
  std::string name;
  std::string description;

  RequestHandler serverRequestHandler;

  std::function<void(ENetPeer*)> connectHandler;
  std::function<void(ENetPeer*)> disconnectHandler;

public:
  LibelleServer();
  LibelleServer(unsigned int port, unsigned max_clients,
      std::string name = "Libelle Server",
      std::string description = "default libelle server");

  ~LibelleServer();

  void addPacker(std::string requestName, RequestPacker packer);
  void addUnpacker(std::string requestName, RequestUnpacker unpacker);

  void remPacker(std::string requestName);
  void remUnpacker(std::string requestName);

  std::string getName();
  void setName(std::string name);

  std::string getDescription();
  void setDescription(std::string description);

  void broadcastRequest(std::string requestName, unsigned int channel = 0,
      bool reliable = false);

  void broadcastRequestExclude(std::string requestName, ENetPeer* exclude,
      unsigned int channel = 0, bool reliable = false);

  void request(std::string requestName, ENetPeer* client,
      unsigned int channel = 0, bool reliable = false);

  void update();

  void setConnectHandler(std::function<void(ENetPeer*)> handler);
  void setDisconnectedHandler(std::function<void(ENetPeer*)> handler);

private:
  void registerHandlers();

  void requestManager(sf::Packet pack, ENetPeer* client);

protected:
  void onInit();
  void onConnect(ENetPeer* client);
  void onDisconnect(ENetPeer* client);
  void onReceive(sf::Packet pack, ENetPeer* client);
  void onOut(std::string s);
  void onStop();

};

#endif /* LIBELLESERVER_H_ */
