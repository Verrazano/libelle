//TODO: replace with license

/*====================*/
// RequestHandler.cpp
// @author Harrison D. Miller
// @version 0.1 May 14, 2013
/*====================*/

#include "RequestHandler.h"
#include <iostream>

RequestHandler::RequestHandler( ){ }

void RequestHandler::addPacker(std::string requestName, RequestPacker packer)
{
  if(hasPacker(requestName))
    return;

  packers.push_back(std::pair<std::string, RequestPacker>(requestName, packer));

}

void RequestHandler::addUnpacker(std::string requestName, RequestUnpacker unpacker)
{
  if(hasUnpacker(requestName))
    return;

  unpackers.push_back(std::pair<std::string, RequestUnpacker>(requestName, unpacker));

}

sf::Packet RequestHandler::pack(std::string request, sf::Packet pack, ENetPeer* client)
{
  int i = indexOfPacker(request);
    if(i < 0)
    {
    	std::cout << "no packer for \"" << request << "\"\n";
      sf::Packet derp;
      derp << "NULL";
      return derp;

    }

    std::pair<std::string, RequestPacker> packer = packers[i];
    return packer.second(pack, client);

}

void RequestHandler::unpack(std::string request, sf::Packet pack, ENetPeer* client)
{
  int i = indexOfUnpacker(request);
  if(i < 0)
    return;

  //std::cout << "unpack request: " << request << "\n";
  std::pair<std::string, RequestUnpacker> unpacker = unpackers[i];
  return unpacker.second(pack, client);

}

bool RequestHandler::hasPacker(std::string requestName)
{
  if(indexOfPacker(requestName) < 0)
    return false;

  return true;

}

bool RequestHandler::hasUnpacker(std::string requestName)
{
  if(indexOfUnpacker(requestName) < 0)
    return false;

  return true;

}

int RequestHandler::indexOfPacker(std::string requestName)
{
  for(unsigned int i = 0; i < packers.size(); i++)
  {
    std::pair<std::string, RequestPacker> packer = packers[i];
    if(packer.first == requestName)
      return i;

  }

  return -1;

}

int RequestHandler::indexOfUnpacker(std::string requestName)
{
  for(unsigned int i = 0; i < unpackers.size(); i++)
  {
    std::pair<std::string, RequestUnpacker> unpacker = unpackers[i];
    if(unpacker.first == requestName)
      return i;

  }

  std::cout << "no unpacker for \"" << requestName << "\"\n";
  return -1;

}

void RequestHandler::remPacker(std::string requestName)
{
  if(!hasPacker(requestName))
    return;

  std::vector<std::pair<std::string, RequestPacker> >::iterator it;
  for(it = packers.begin(); it != packers.end(); ++it)
  {
    std::pair<std::string, RequestPacker> packer = *it;
    if(packer.first == requestName)
    {
      packers.erase(it);
      return;

    }

  }

}

void RequestHandler::remUnacker(std::string requestName)
{
  if(!hasUnpacker(requestName))
    return;

  std::vector<std::pair<std::string, RequestUnpacker> >::iterator it;
  for(it = unpackers.begin(); it != unpackers.end(); ++it)
  {
    std::pair<std::string, RequestUnpacker> unpacker = *it;
    if(unpacker.first == requestName)
    {
      unpackers.erase(it);
      return;

    }

  }

}

void RequestHandler::remAll( )
{
  while(!packers.empty())
    packers.erase(packers.begin());

  while(!unpackers.empty())
    unpackers.erase(unpackers.begin());

}

