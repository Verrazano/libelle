#ifndef PACKETCONVERTER_H_
#define PACKETCONVERTER_H_

//converts packets from sfml to enet and back

#include <SFML/Network.hpp>
#include <enet/enet.h>

//MUST DESTROY ENETPACKET AFTER YOU ARE DONE
ENetPacket* toEnet(sf::Packet sp, int flags);

sf::Packet toSFML(ENetPacket* ep);

#endif /* PACKETCONVERTER_H_ */
