//TODO: replace with license

/*====================*/
// RequestHandler.h
// @author Harrison D. Miller
// @version 0.1 May 14, 2013
/*====================*/

#ifndef REQUESTHANDLER_H_
#define REQUESTHANDLER_H_

#include <functional>
#include <SFML/Network.hpp>
#include <enet/enet.h>
#include <utility>

typedef std::function<sf::Packet(sf::Packet, ENetPeer*)> RequestPacker;
typedef std::function<void(sf::Packet, ENetPeer*)> RequestUnpacker;

class RequestHandler
{
private:
  std::vector<std::pair<std::string, RequestPacker > > packers;
  std::vector<std::pair<std::string, RequestUnpacker > > unpackers;

public:
  RequestHandler( );

  void addPacker(std::string requestName, RequestPacker packer);

  void addUnpacker(std::string requestName, RequestUnpacker unpacker);

  sf::Packet pack(std::string request, sf::Packet pack, ENetPeer* client);

  void unpack(std::string request, sf::Packet pack, ENetPeer* client);

  bool hasPacker(std::string requestName);

  bool hasUnpacker(std::string requestName);

  //returns -1 if the packer/unpacker doesn't exist
  int indexOfPacker(std::string requestName);

  int indexOfUnpacker(std::string requestName);

  void remPacker(std::string requestName);

  void remUnacker(std::string requestName);

  void remAll( );

};

#endif /* REQUESTHANDLER_H_ */
