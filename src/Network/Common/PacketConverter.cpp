#include "PacketConverter.h"
#include <enet/enet.h>

ENetPacket* toEnet(sf::Packet sp, int flags)
{
	  ENetPacket* ep = enet_packet_create(sp.getData(), sp.getDataSize(), flags);
	  return ep;

}

sf::Packet toSFML(ENetPacket* ep)
{
	sf::Packet sp;
	sp.clear();
	sp.append(ep->data, ep->dataLength);
	return sp;

}
