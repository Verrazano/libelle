#include "Server.h"
#include "Common/PacketConverter.h"
#include <enet/enet.h>
#include <iostream>

Server::Server()
{
  running = false;

  setMaxClients(32);
  setNumOfChannels(64);
  setPort(1234);
  setBasicOut(false);

}

Server::Server(unsigned int port, unsigned int max_clients,
    unsigned int channels, bool basicOut)
{
  running = false;

  setMaxClients(max_clients);
  setNumOfChannels(channels);
  setPort(port);
  setBasicOut(basicOut);

}

Server::~Server()
{
  cleanUp();

}

void Server::start()
{
  if(isRunning())
    return;

  init();
  running = true;

}

void Server::init()
{
  onInit();

  if(isBasicOutOn())
    out("Initializing server.");

  address.host = ENET_HOST_ANY;
  address.port = port;

  enetServer = enet_host_create(&address, max_clients, channels, 0, 0);

}

void Server::run()
{

    if(enet_host_service(enetServer, &event, 0) > 0)
    {
      if(event.type == ENET_EVENT_TYPE_CONNECT)
      {
        clients.push_back(event.peer);
        onConnect(event.peer);

        if(isBasicOutOn())
          out("A new client joined.");

      }
      else if(event.type == ENET_EVENT_TYPE_RECEIVE)
      {
        onReceive(toSFML(event.packet), event.peer);
        if(isBasicOutOn())
          out("Received a packet.");

      }
      else if(event.type == ENET_EVENT_TYPE_DISCONNECT)
      {
        disconnect(event.peer);

      }

    }

    sf::sleep(sf::milliseconds(1));

}

void Server::stop()
{
  onStop();
  running = false;
  out("dis");
  disconnectAll();

}

bool Server::isRunning(){ return running; }

unsigned int Server::getPort(){ return port; }
void Server::setPort(unsigned int port)
{
  if(isRunning())
    return;

  this->port = port;

}

unsigned int Server::getNumOfChannels(){ return channels; }
void Server::setNumOfChannels(unsigned int channels)
{
  if(isRunning())
    return;

  this->channels = channels;
}

unsigned int Server::getMaxClients(){ return max_clients; }
void Server::setMaxClients(unsigned int max_clients)
{
  if(isRunning())
    return;

  this->max_clients = max_clients;

}

std::string Server::getServerIP()
{
  return sf::IpAddress::getPublicAddress(sf::milliseconds(500)).toString();

}

bool Server::isBasicOutOn(){ return basicOut; }
void Server::setBasicOut(bool enable){ basicOut = enable; }

unsigned int Server::getClientsConnected(){ return clients.size(); }
ENetPeer* Server::getClientByIndex(unsigned int index)
{
  return clients[index];

}

int Server::getIndexOf(ENetPeer* client)
{
  for(unsigned int i = 0; i < getClientsConnected(); i++)
    if(sameENetPeerData(client, clients[i]))
    {
      return i;

    }

  return -1;

}

void Server::broadcast(sf::Packet pack, unsigned int channel,
    bool reliable)
{
	/*if(enetServer == NULL)
		return;

	if(!pack)
	{
		return;

	}*/

  ENetPacket* packet;
  packet =  toEnet(pack, reliable ? ENET_PACKET_FLAG_RELIABLE : 0);

  if(packet == NULL)
  {
	  onOut("NULL PACKET > SERVER : 181");
	  return;

  }

 // std::cout << "broadcast " << packet << "\n";

  enet_host_broadcast(enetServer, channel, packet);
  enet_host_flush(enetServer);

}

void Server::broadcastExcluding(sf::Packet pack, ENetPeer* exclude,
    unsigned int channel, bool reliable)
{
  for(unsigned int i = 0; i < getClientsConnected(); i++)
  {
    if(!sameENetPeerData(exclude, clients[i]))
    {
      send(pack, clients[i]);

    }

  }

}

void Server::send(sf::Packet pack, ENetPeer* client,
    unsigned int channel, bool reliable)
{
  ENetPacket* packet;
  packet = toEnet(pack, reliable ? ENET_PACKET_FLAG_RELIABLE : 0);

  if(packet == NULL)
  {
	  onOut("NULL PACKET > SERVER : 210");
	  return;

  }
  //std::cout << "send " << packet << "\n";

  enet_peer_send(client, channel, packet);
  enet_host_flush(enetServer);

}

void Server::out(std::string s)
{
  onOut(s);

}

void Server::disconnect(ENetPeer* client)
{
  enet_peer_disconnect_now(client, 0);

  onDisconnect(client);

  if(isBasicOutOn())
    out("Disconnected a client from the server.");

  enet_peer_reset(client);

  std::vector<ENetPeer*>::iterator it;
  for(it = clients.begin(); it != clients.end(); it++)
  {
    if(sameENetPeerData(client, *it))
    {
      clients.erase(it);
      return;

    }

  }

}

void Server::disconnectAll()
{
  for(unsigned int i = 0; i < getClientsConnected(); i++)
    disconnect(clients[i]);

}

bool Server::sameENetPeerData(ENetPeer* peer1, ENetPeer* peer2)
{
  if(peer1->address.host == peer2->address.host &&
      peer1->address.port == peer2->address.port)
  {
    return true;

  }

  return false;

}

void Server::cleanUp()
{
  stop();
  clients.clear();
  enet_host_destroy(enetServer);

}
