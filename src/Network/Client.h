#ifndef CLIENT_H_
#define CLIENT_H_

#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <enet/enet.h>

//WARNING YOU MUST CALL enet_initialize() before calling start()

/* A simple threaded enet client that can send and receive data
 * using sfml packets. There are hooks to control the behavior of the
 * server that must be overwritten. Meant to be easily extended.
 */
class Client
{
private:
  bool running;
  bool connected;

  ENetHost* enetClient;
  ENetAddress serverAddress;
  std::string serverIP;
	ENetEvent event;

protected:

  unsigned int port;
  unsigned int channels;
  ENetPeer* serverPeer;

  //the amount of time in milliseconds before a connection is deemed as failed.
  unsigned int connecting_timeout;
  bool basicOut;

public:
  //defaults port to 1234 and sets ip to localhost
  Client();

  /* The port at which to contact the server
   * and the server ip. Defaulted to localhost . . this computer.
   */
  Client(unsigned int port, unsigned int connecting_timeout = 5000,
      unsigned int channels = 1, std::string serverIP = "localhost",
      bool basicOut = false);

  //make sure to call this in overloaded destructor
  virtual ~Client();

  /* launches the client thread. */
  void start();

  unsigned int getPing()
  {
	  return serverPeer->roundTripTime;

  }

  void run();

private:
  /* used to setup everything before running
   * called from run it's self.
   * intializes server address and tries initial connection to server.
   */
  void init();

  /* Attempts to connect to the specified server if the specified
   * time is reached the hook onConnectionFailed will be triggered.
   */
  void attemptConnect();

  /* runs the basic server loop, attempts to reconnect if
   * disconnected. Calls necessary hooks.
   */

public:
  /* stops the client thread and triggers onStop(). */
  void stop();

  bool isRunning();
  bool isConnected();

  /*setters disabled while client is running*/
  unsigned int getPort();
  void setPort(unsigned int port);

  unsigned int getNumOfChannels();
  void setNumOfChannels(unsigned int channels);

  unsigned int getConnectingTimeout();
  void setConnectingTimeout(unsigned int connecting_timeout);

  std::string getServerIP();
  void setServerIP(std::string serverIP);

  bool isBasicOutOn();
  void setBasicOut(bool enable);

  //triggers onOut hook which can be customized
  void out(std::string s);

  //sends a packet to the server
  void send(sf::Packet pack, unsigned int channel = 0,
      bool reliable = false);

  //disconnect from the server
  void disconnect();

protected:
  virtual void onInit() = 0;
  virtual void onConnectionFailed() = 0;
  virtual void onConnect() = 0;
  virtual void onDisconnect() = 0;
  virtual void onReceive(sf::Packet pack) = 0;
  virtual void onOut(std::string s) = 0;
  virtual void onStop() = 0;

  //call this on deconstruct
  void cleanUp();

};

#endif /* CLIENT_H_ */
