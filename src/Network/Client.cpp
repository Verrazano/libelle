#include "Client.h"
#include "Common/PacketConverter.h"

Client::Client() {
	running = false;
	connected = false;

	setServerIP("localhost");
	setPort(1234);
	setNumOfChannels(64);
	setConnectingTimeout(5000);
	setBasicOut(false);

}

Client::Client(unsigned int port, unsigned int connecting_timeout,
		unsigned int channels, std::string serverIP, bool basicOut) {
	running = false;
	connected = false;

	setServerIP(serverIP);
	setPort(port);
	setNumOfChannels(channels);
	setConnectingTimeout(connecting_timeout);
	setBasicOut(basicOut);

}

Client::~Client() {
	cleanUp();

}

void Client::start() {
	if (isRunning())
		return;

	init();
	running = true;

}

void Client::init() {
	onInit();

	if (isBasicOutOn())
		out("Initializing client.");

	enetClient = enet_host_create(NULL, 1, channels, 0, 0);
	enet_address_set_host(&serverAddress, (serverIP).c_str());
	serverAddress.port = port;

	attemptConnect();

}

void Client::attemptConnect() {
	serverPeer = enet_host_connect(enetClient, &serverAddress, channels, 0);

	if (isBasicOutOn())
		out("Attempting to connect to the server.");

	ENetEvent event;
	if (enet_host_service(enetClient, &event, connecting_timeout) > 0
			&& event.type == ENET_EVENT_TYPE_CONNECT) {
		onConnect();
		if (isBasicOutOn())
			out("Connected to the server.");

		connected = true;

	} else {
		onConnectionFailed();
		if (isBasicOutOn())
			out("Connection to the server failed.");

		connected = false;

	}

}

void Client::run() {

		if (connected && enet_host_service(enetClient, &event, 1000) > 0) {
			if (event.type == ENET_EVENT_TYPE_RECEIVE) {
				onReceive(toSFML(event.packet));
				if (isBasicOutOn())
					out("Received a packet.");

			} else if (event.type == ENET_EVENT_TYPE_DISCONNECT) {
				disconnect();

			}

		} else if (!connected) {
			disconnect();
			attemptConnect();

		}

}

void Client::stop() {
	onStop();
	running = false;
	disconnect();

}

bool Client::isRunning() {
	return running;
}
bool Client::isConnected() {
	return connected;
}

unsigned int Client::getPort() {
	return port;
}
void Client::setPort(unsigned int port) {
	if (isRunning())
		return;

	this->port = port;

}

unsigned int Client::getNumOfChannels() {
	return channels;
}
void Client::setNumOfChannels(unsigned int channels) {
	if (isRunning())
		return;

	this->channels = channels;

}

unsigned int Client::getConnectingTimeout() {
	return connecting_timeout;
}
void Client::setConnectingTimeout(unsigned int connecting_timeout) {
	if (isRunning())
		return;

	this->connecting_timeout = connecting_timeout;

}

std::string Client::getServerIP() {
	return serverIP;
}
void Client::setServerIP(std::string serverIP) {
	if (isRunning())
		return;

	this->serverIP = serverIP;

}

bool Client::isBasicOutOn() {
	return basicOut;
}
void Client::setBasicOut(bool enable) {
	basicOut = enable;
}

void Client::out(std::string s) {
	onOut(s);

}

void Client::send(sf::Packet pack, unsigned int channel, bool reliable) {
	ENetPacket* packet;
	packet = toEnet(pack, reliable ? ENET_PACKET_FLAG_RELIABLE : 0);

	if (packet == NULL) {
		onOut("NULL PACKET > SERVER : 203");
		return;

	}

	enet_peer_send(serverPeer, channel, packet);
	enet_host_flush(enetClient);

}

void Client::disconnect() {
	enet_peer_disconnect_now(serverPeer, 0);

	connected = false;
	onDisconnect();

	if (isBasicOutOn())
		out("Disconnected from the server.");

	enet_peer_reset(serverPeer);

}

void Client::cleanUp() {
	stop();
	enet_host_destroy(enetClient);

}
