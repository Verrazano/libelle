//TODO: replace with license

/*====================*/
// LibelleClient.cpp
// @author Harrison D. Miller
// @version 0.1 May 17, 2013
/*====================*/

#include "LibelleClient.h"
#include <iostream>

LibelleClient::LibelleClient()
: Client()
{
	//setBasicOut(true);

}

LibelleClient::LibelleClient(unsigned int port,  std::string serverIP)
: Client(port, 5000, 64, serverIP, false)
{
	//setBasicOut(true);

}

LibelleClient::~LibelleClient()
{

}

void LibelleClient::addPacker(std::string requestName, RequestPacker packer)
{
  clientRequestHandler.addPacker(requestName, packer);

}

void LibelleClient::addUnpacker(std::string requestName, RequestUnpacker unpacker)
{
  clientRequestHandler.addUnpacker(requestName, unpacker);

}

void LibelleClient::remPacker(std::string requestName)
{
  clientRequestHandler.remPacker(requestName);

}

void LibelleClient::remUnpacker(std::string requestName)
{
  clientRequestHandler.remUnacker(requestName);

}

void LibelleClient::request(std::string requestName)
{
  sf::Packet pack;
  pack << "request" << requestName;
  send(pack, 1, true);

}

ENetPeer* LibelleClient::getServerPeer()
{
	return serverPeer;

}

void LibelleClient::setConnectHandler(std::function<void(ENetPeer*)> handler)
{
  connectHandler = handler;

}

void LibelleClient::setDisconnectedHandler(std::function<void(ENetPeer*)> handler)
{
  disconnectHandler = handler;

}

void LibelleClient::requestManager(sf::Packet pack)
{
  std::string cmd;
  pack >> cmd;
  if(cmd == "request")
  {
    std::string request;
    pack >> request;

    if(!clientRequestHandler.hasPacker(request))
      return;

    sf::Packet rPack = clientRequestHandler.pack(request, pack, serverPeer);
    out("answering request");
    send(rPack, 1, true);

  }
  else if(clientRequestHandler.hasUnpacker(cmd))
  {
    clientRequestHandler.unpack(cmd, pack, serverPeer);

  }

}

void LibelleClient::onInit()
{

}

void LibelleClient::onConnect()
{
  out("connected to the server.");
  connectHandler(getServerPeer());

}

void LibelleClient::onConnectionFailed()
{

}

void LibelleClient::onDisconnect()
{
  out("disconnected from the server.");
  disconnectHandler(getServerPeer());

}

void LibelleClient::onReceive(sf::Packet pack)
{
  requestManager(pack);

}

void LibelleClient::onOut(std::string s)
{
	std::cout << s << "\n";

}

void LibelleClient::onStop()
{

}


